package br.ufsm.aopjungle.log;

import org.aspectj.asm.IProgramElement;

import br.ufsm.aopjungle.binding.AOJBinding;
import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.aspectj.AOJBuildListener;

public aspect DebugBinding {
	pointcut assignInOut (AOJMember source, AOJBinding sourceBinding, IProgramElement sourceElement,
			AOJMember target, AOJBinding targetBinding, IProgramElement targetElement) : 
		execution (* AOJBuildListener.loadInOutRelationShip 
			(AOJMember, AOJBinding , IProgramElement, 
			 AOJMember, AOJBinding, IProgramElement))
		&& args (source, sourceBinding, sourceElement,
	    target, targetBinding, targetElement);
	
	before (AOJMember source, AOJBinding sourceBinding, IProgramElement sourceElement,
			AOJMember target, AOJBinding targetBinding, IProgramElement targetElement) : 
			assignInOut (source, sourceBinding, sourceElement,
		    target, targetBinding, targetElement) {
		System.out.println("Out :: " + targetBinding.getBindingHandler() + " kind " + targetBinding.getKind() + " " + targetBinding.getName());
		System.out.println("In :: " + sourceBinding.getBindingHandler() + " kind " + sourceBinding.getKind() + " " + sourceBinding.getName());
	}
}