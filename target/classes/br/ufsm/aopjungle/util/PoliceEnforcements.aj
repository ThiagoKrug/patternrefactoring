package br.ufsm.aopjungle.util;

import java.util.List;

import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeble;

public aspect PoliceEnforcements {
	pointcut setTypeNotWithinCompilationUnits() : 
		call (* List+.add(AOJTypeble)) && !within(AOJCompilationUnit);

	declare error : setTypeNotWithinCompilationUnits() :
		"Use addType method of AOJCompilationUnit class instead";
		//		pointcut callType(AOJTypeble type) : 
//		call (* List+.add(..)) && args(type) && within(AOJCompilationUnit);

}
