package br.ufsm.aopjungle.hql;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.util.HibernateUtil;

public class AOJProjectDAO {
	private AOJWorkspace workspace;
	
	public AOJProjectDAO(AOJWorkspace workspace) {
		this.workspace = workspace;					
	}
	
	public void saveAll() {
		System.out.println("Saving project...");
	
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		transaction = session.beginTransaction();

		try {
			for (AOJProject project : workspace.getProjects()) {
				System.out.println("Saving project.." + project.getName());
				
//				for (AOJClassDeclaration clazz : project.getClasses()) {
//					for (AOJConstructorDeclaration c : clazz.getMembers().getConstructors()) {
//						for (AOJAnnotationDeclaration a : c.getAnnotations()) {
//							System.out.println(a.getName());
//						}
//					}
//				}
				
				Long id = (Long) session.save(project);
				System.out.println("Project Id saved = " + id);				
			}

		transaction.commit();

		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			//session.close();
		}
	}
}
