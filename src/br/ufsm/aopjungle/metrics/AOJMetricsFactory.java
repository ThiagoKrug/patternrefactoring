package br.ufsm.aopjungle.metrics;

public interface AOJMetricsFactory {
	AOJMetricsFactory eINSTANCE = AOJMetricsFactoryImpl.init();
	AOJBehaviorMetrics createBehaviorMetrics();
	AOJTypeMetrics createTypeMetrics();
	AOJAspectMetrics createAspectMetrics();
	AOJContainerMetrics createPackageMetrics();
	AOJContainerMetrics createProjectMetrics();
	AOJContainerMetrics createGlobalMetrics();
	AOJCommonMetrics createSimpleMetrics();
}
