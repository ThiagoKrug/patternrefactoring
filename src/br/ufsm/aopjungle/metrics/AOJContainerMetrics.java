package br.ufsm.aopjungle.metrics;

import javax.persistence.Entity;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */

@Entity
public class AOJContainerMetrics extends AOJCommonMetrics {
	private long numberOfSubContainers;	
	private long numberOfClasses;
	private long numberOfInterfaces;
	private long numberOfAspects;
	private long numberOfEnums;
	private long numberOfAnnotationDecls;

	public AOJContainerMetrics() {
	
	}
	
	public long getNumberOfClasses() {
		return numberOfClasses;
	}
	public void setNumberOfClasses(long numberOfClasses) {
		this.numberOfClasses = numberOfClasses;
	}
	public long getNumberOfInterfaces() {
		return numberOfInterfaces;
	}
	public void setNumberOfInterfaces(long numberOfInterfaces) {
		this.numberOfInterfaces = numberOfInterfaces;
	}
	public long getNumberOfAspects() {
		return numberOfAspects;
	}
	public void setNumberOfAspects(long numberOfAspects) {
		this.numberOfAspects = numberOfAspects;
	}
	public long getNumberOfEnums() {
		return numberOfEnums;
	}
	public void setNumberOfEnums(long numberOfEnums) {
		this.numberOfEnums = numberOfEnums;
	}
	public long getNumberOfAnnotationDecls() {
		return numberOfAnnotationDecls;
	}
	public void setNumberOfAnnotationDecls(long numberOfAnnotationDecls) {
		this.numberOfAnnotationDecls = numberOfAnnotationDecls;
	}
	public long getNumberOfSubContainers() {
		return numberOfSubContainers;
	}
	public void setNumberOfSubContainers(long numberOfSubContainers) {
		this.numberOfSubContainers = numberOfSubContainers;
	}	
}
