package br.ufsm.aopjungle.metrics;

import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;

public interface AOJMetrics {
	public AOJProgramElement getOwner();
	public void loadMetrics();
	public AOJCommonMetrics getMetrics();
}
