package br.ufsm.aopjungle.metrics;

public class AOJMetricsFactoryImpl implements AOJMetricsFactory {
	  private static AOJMetricsFactory factory;

	  public static AOJMetricsFactory init() {
		  if (factory == null)
			  factory = new AOJMetricsFactoryImpl(); 
		  return factory;
	  }
	  
	@Override
	public AOJBehaviorMetrics createBehaviorMetrics() {
		AOJBehaviorMetrics metrics = new AOJBehaviorMetrics();
		return metrics;
	}

	@Override
	public AOJTypeMetrics createTypeMetrics() {
		AOJTypeMetrics metrics = new AOJTypeMetrics();
		return metrics;
	}

	@Override
	public AOJAspectMetrics createAspectMetrics() {
		AOJAspectMetrics metrics = new AOJAspectMetrics();
		return metrics;
	}
	
	@Override
	public AOJContainerMetrics createPackageMetrics() {
		return createContainerMetrics();
	}

	private AOJContainerMetrics createContainerMetrics() {
		AOJContainerMetrics metrics = new AOJContainerMetrics();
		return metrics;
	}
	
	@Override
	public AOJContainerMetrics createProjectMetrics() {
		return createContainerMetrics();
	}

	@Override
	public AOJContainerMetrics createGlobalMetrics() {
		return createContainerMetrics();
	}

	@Override
	public AOJCommonMetrics createSimpleMetrics() {
		AOJCommonMetrics metrics = new AOJCommonMetrics();
		return metrics;
	}
}
