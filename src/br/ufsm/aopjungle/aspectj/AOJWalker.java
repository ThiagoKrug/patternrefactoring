package br.ufsm.aopjungle.aspectj;

import org.aspectj.asm.HierarchyWalker;
import org.aspectj.asm.IProgramElement;

public class AOJWalker extends HierarchyWalker {
	@Override
	protected void preProcess(IProgramElement node) {
		System.out.println(node.getPackageName() + "::" + node.getKind());
		super.preProcess(node);
	}
}
