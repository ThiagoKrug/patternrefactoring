package br.ufsm.aopjungle.aspectj;

import org.aspectj.asm.IProgramElement;

import br.ufsm.aopjungle.binding.AOJBinding;
import br.ufsm.aopjungle.log.AOJLogger;
import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.aspectj.AOJBuildListener;

public aspect AOJReferenceMapCheck {
	pointcut inoutMapReference 
		(AOJMember source, AOJBinding sourceBinding, IProgramElement sourceElement,
			AOJMember target, AOJBinding targetBinding, IProgramElement targetElement) : 
			execution (* AOJBuildListener.loadInOutRelationShip(AOJMember, AOJBinding, IProgramElement,
					AOJMember, AOJBinding, IProgramElement)) 
			&& args (source, sourceBinding, sourceElement, target, targetBinding, 
					targetElement);

	before (AOJMember source, AOJBinding sourceBinding, IProgramElement sourceElement,
			AOJMember target, AOJBinding targetBinding, IProgramElement targetElement) : 
				inoutMapReference (source, sourceBinding, sourceElement, target, targetBinding, 
						targetElement) {
		if ( (null == source) ) 
			AOJLogger.getLogger(thisJoinPoint.getClass()).warn("Source not found at reference map. Ignored. Element : " + sourceElement.getName() + " :: " +  sourceElement.getBytecodeName());
		if ( (null == target) )
			AOJLogger.getLogger(thisJoinPoint.getClass()).warn("Target not found at reference map. Ignored. Element : " + targetElement.getName() + " :: " +  targetElement.getBytecodeName());
	}
}
