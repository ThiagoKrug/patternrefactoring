package br.ufsm.aopjungle.aspectj;

import org.aspectj.ajdt.internal.compiler.ast.AjMethodDeclaration;
import org.aspectj.ajdt.internal.core.builder.AsmHierarchyBuilder;
import org.aspectj.org.eclipse.jdt.internal.compiler.ast.MethodDeclaration;
import org.aspectj.org.eclipse.jdt.internal.compiler.ast.TypeDeclaration;
import org.aspectj.org.eclipse.jdt.internal.compiler.lookup.ClassScope;
import org.aspectj.org.eclipse.jdt.internal.compiler.lookup.CompilationUnitScope;

import br.ufsm.aopjungle.metamodel.commons.AOJPackageDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.util.AOJAsmUtils;

public class AOJAsmBuilder extends AsmHierarchyBuilder {
	private AOJProject project;
	
	public AOJAsmBuilder(AOJProject project) {
		this.project = project;
	}
	
	@Override
	public boolean visit(TypeDeclaration arg0, CompilationUnitScope arg1) {
		return super.visit(arg0, arg1);
	}

	@Override
	public boolean visit(MethodDeclaration arg0, ClassScope arg1) {
		if ( !(arg0 instanceof AjMethodDeclaration) )
			System.out.println("Method Signature : " + AOJAsmUtils.genSourceSignature(arg0));
		return super.visit(arg0, arg1);
	}
	
	private AOJPackageDeclaration getPackage(TypeDeclaration type) {
		String packageName = new String(type.scope.getCurrentPackage().readableName());
		AOJPackageDeclaration pack = project.getPackage(packageName);
		return pack;
	}
}
