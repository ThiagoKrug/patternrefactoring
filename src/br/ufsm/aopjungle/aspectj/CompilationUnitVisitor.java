package br.ufsm.aopjungle.aspectj;

import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class CompilationUnitVisitor {
	@XStreamOmitField
	private AOJProgramElement cUnit;
	
	public CompilationUnitVisitor(AOJProgramElement compilationUnit) {
		setcUnit(compilationUnit);
	}

	private void setcUnit(AOJProgramElement cUnit) {
		this.cUnit = cUnit;
	}

	public AOJProgramElement getcUnit() {
		return cUnit;
	}
	
	
	
	
}
