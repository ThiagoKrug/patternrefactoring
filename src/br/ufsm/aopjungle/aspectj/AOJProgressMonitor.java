package br.ufsm.aopjungle.aspectj;

import org.aspectj.ajdt.internal.core.builder.AjBuildManager;
import org.aspectj.ajdt.internal.core.builder.AsmHierarchyBuilder;
import org.eclipse.ajdt.core.builder.AJBuilder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;

public class AOJProgressMonitor extends NullProgressMonitor {
	private AsmHierarchyBuilder hb;
	private IProject project;

	public AOJProgressMonitor(AsmHierarchyBuilder hb, IProject project) {
		this.hb = hb;
		this.project = project;
	}	

	@Override
    public void done() {
		AJBuilder.removeAJBuildListener(AOJBuildListener.getInstance());
		AjBuildManager.setAsmHierarchyBuilder(hb);
		try {
			project.refreshLocal(IResource.DEPTH_ONE, null);
		} catch (CoreException e) {
			
		}
     }
}
