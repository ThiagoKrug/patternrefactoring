package br.ufsm.aopjungle.aspectj;

//import org.aspectj.ajde.Ajde;
import org.aspectj.ajdt.internal.core.builder.AjBuildManager;
import org.aspectj.ajdt.internal.core.builder.AsmHierarchyBuilder;
import org.eclipse.ajdt.core.builder.AJBuilder;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;

import br.ufsm.aopjungle.exception.AOJCompilerException;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.views.AOPJungleMonitor;

public class AOJCompiler {
	private String configfile = "/Users/defaveri/Documents/workspace/runtime-AOPJungle-AOPTetris/AOPTetris/start.lst";
	
	public AOJCompiler() {
//		Ajde.getDefault().getBuildConfigManager().addListener(buildListener);
	}
	
	public AOJAsmBuilder build(AOJProject project) throws AOJCompilerException {
		AOJAsmBuilder asmBuilder = new AOJAsmBuilder(project);
		AsmHierarchyBuilder hb = AjBuildManager.getAsmHierarchyBuilder();
	    AJBuilder.addAJBuildListener(AOJBuildListener.getInstance());
	    AjBuildManager.setAsmHierarchyBuilder(asmBuilder);
	    try {
	    	//project.getResourceProject().open(new NullProgressMonitor());
	    	AOPJungleMonitor.projectMonitor.setTaskName("Opening project " + project.getName());
	    	project.getResourceProject().open(AOPJungleMonitor.projectMonitor.split(1));
	    	
			//project.getResourceProject().build(IncrementalProjectBuilder.FULL_BUILD, new AOJProgressMonitor(hb, project.getResourceProject()));
	    	AOPJungleMonitor.projectMonitor.setTaskName("Building project " + project.getName());
			project.getResourceProject().build(IncrementalProjectBuilder.FULL_BUILD, AOPJungleMonitor.projectMonitor.split(49));
		} catch (CoreException e) {
			e.printStackTrace();
		}
		
//		AjBuildManager.setAsmHierarchyBuilder(asmBuilder);
//		doBuild(this.configfile);
//		if (!doBuild(this.configfile))
//			throw new AOJCompilerException("Build failed");

		return asmBuilder;
	}

	/*protected boolean doBuild(String config) {
		System.out.println("building " + config);
		Ajde.getDefault().getBuildConfigManager().buildModel(config);
//		while(!buildListener.getBuildFinished()) {
//			try {
//				Thread.sleep(300);
//			} catch (InterruptedException ie) {
//				
//			}
//		}	
		return true;
	}*/
	
}
