package br.ufsm.aopjungle.bindings.nold;

import org.aspectj.org.eclipse.jdt.core.dom.ITypeBinding;
import org.aspectj.org.eclipse.jdt.core.dom.IVariableBinding;
import org.aspectj.org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.aspectj.org.eclipse.jdt.core.dom.VariableDeclarationStatement;


import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class AOJVariableDeclarationBinding implements AOJTypeBinding, AOJVariableBinding  {
	@XStreamOmitField
	private VariableDeclarationStatement varBinding;
	private String expression;
	private String type;
	private String name;
	private AOJBindingKind kind;
	private int modifiers;
	private boolean isDeprecated;
	private boolean isSynthetic;
	private boolean isAnnotation;
	private boolean isClass;
	private boolean isInterface;
	private boolean isEnum;
	private boolean isArray;
	private boolean isGenericType;
	private boolean isMember;
	private boolean isPrimitive;
	private boolean isNullType;
	private boolean isAspect;
	private boolean isRawType;
	private boolean isFromSource;
	private boolean isNested;
	private boolean isAnonymous;
	private boolean isWildcardType;
	private boolean isParameterizedType;
	private boolean isLocal;
	
	public AOJVariableDeclarationBinding (VariableDeclarationStatement varBinding) {
		this.varBinding = varBinding;
		setBindings();
	}
	
	private void setBindings() {
		expression = varBinding.toString();
		modifiers = varBinding.getModifiers();
		type = varBinding.getType().toString();
		
		ITypeBinding varType = varBinding.getType().resolveBinding();
		if (varType != null) {
			kind = AOJBindingKind.get(varType.getKind());
			isDeprecated = varType.isDeprecated();
			isSynthetic = varType.isSynthetic();
			isAnnotation = varType.isAnnotation();
			isClass = varType.isClass();
			isInterface = varType.isInterface();
			isEnum = varType.isEnum();
			isArray = varType.isArray();
			isGenericType = varType.isGenericType();
			isMember = varType.isMember();
			isPrimitive = varType.isPrimitive();
			isNullType = varType.isNullType();
			isRawType = varType.isRawType();
			isFromSource = varType.isFromSource();
			isNested = varType.isNested();
			isAnonymous = varType.isAnonymous();
			isWildcardType = varType.isWildcardType();
			isParameterizedType = varType.isParameterizedType();
			isLocal = varType.isLocal();
		}
		
		for (Object fragment : varBinding.fragments()) {
			name = ((VariableDeclarationFragment)fragment).getName().toString();
			IVariableBinding var = ((VariableDeclarationFragment)fragment).resolveBinding();
			if (var != null)
				name = var.getName();
		}
	}

	@Override
	public AOJBindingKind getKind() {
		return kind;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getModifiers() {
		return modifiers;
	}

	@Override
	public boolean isDeprecated() {
		return isDeprecated;
	}

	@Override
	public boolean isSynthetic() {
		return isSynthetic;
	}

	@Override
	public boolean isAnnotation() {
		return isAnnotation;
	}

	@Override
	public boolean isAnonymous() {
		return isAnonymous;
	}

	@Override
	public boolean isArray() {
		return isArray;
	}

	@Override
	public boolean isClass() {
		return isClass;
	}

	@Override
	public boolean isEnum() {
		return isEnum;
	}

	@Override
	public boolean isGenericType() {
		return isGenericType;
	}

	@Override
	public boolean isInterface() {
		return isInterface;
	}

	@Override
	public boolean isMember() {
		return isMember;
	}

	@Override
	public boolean isNullType() {
		return isNullType;
	}

	@Override
	public boolean isPrimitive() {
		return isPrimitive;
	}

	@Override
	public boolean isRawType() {
		return isRawType;
	}

	@Override
	public boolean isAspect() {
		return isAspect;
	}

	@Override
	public boolean isParameterizedType() {
		return isParameterizedType;
	}

	@Override
	public boolean isFromSource() {
		return isFromSource;
	}

	@Override
	public boolean isLocal() {
		return isLocal;
	}

	@Override
	public boolean isNested() {
		return isNested;
	}

	@Override
	public boolean isWildcardType() {
		return isWildcardType;
	}

	public String getType() {
		return type;
	}

	public String getExpression() {
		return expression;
	}
	
}