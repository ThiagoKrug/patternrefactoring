	package br.ufsm.aopjungle.bindings.nold;

public interface AOJTypeBinding extends AOJBinding {
	public boolean isAnnotation();
	public boolean isAnonymous();	
	public boolean isArray();
	public boolean isClass();
	public boolean isEnum();
	public boolean isGenericType();
	public boolean isInterface();
	public boolean isMember();
	public boolean isNullType();
	public boolean isPrimitive();
	public boolean isRawType();
	public boolean isAspect();
	public boolean isParameterizedType();
	public boolean isFromSource();
	public boolean isLocal();
	public boolean isNested();
	public boolean isWildcardType();
}
