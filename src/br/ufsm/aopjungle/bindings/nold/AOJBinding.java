package br.ufsm.aopjungle.bindings.nold;


public interface AOJBinding {
	public AOJBindingKind getKind();
	public String getName();
	public int getModifiers();
	public boolean isDeprecated();
	public boolean isSynthetic(); // Instructions built by the compiler
	//public IJavaElement getJavaElement();
	
	
}
