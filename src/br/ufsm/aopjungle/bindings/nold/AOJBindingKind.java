package br.ufsm.aopjungle.bindings.nold;

public enum AOJBindingKind {
	PACKAGE (1), 
	TYPE (2), 
	VARIABLE (3),
	METHOD (4), 
	ANNOTATION (5),
	MEMBER_VALUE_PAIR (6), 
	ADVICE(7);

	private int code;
	
	public int getCode() {
		return code;
	}
	
	AOJBindingKind (int code) {
		this.code = code;
	}
	
	public static AOJBindingKind get(int code) {
		for (AOJBindingKind kind : AOJBindingKind.values()) {
			if (kind.getCode() == code) 
				return kind;
		}
		
		return null;
	}
}	
