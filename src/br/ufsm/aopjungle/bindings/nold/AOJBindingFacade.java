package br.ufsm.aopjungle.bindings.nold;


public class AOJBindingFacade {
	private AOJVariableDeclarationBinding varBinding;

	public static AOJBindingFacade getInstance() {
		return new AOJBindingFacade();
	}
	
	public AOJVariableDeclarationBinding getVarBinding() {
		return varBinding;
	}

	public void setVarBinding(AOJVariableDeclarationBinding varBinding) {
		this.varBinding = varBinding;
	}
	
	public boolean isEmpty() {
		return varBinding == null ? true : false;
	}
	
}
