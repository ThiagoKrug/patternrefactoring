package br.ufsm.aopjungle.log;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AOJLogger {
	static final Logger logger = Logger.getLogger(AOJLogger.class);
			
	public static Logger getLogger() {
		return logger;
	}

	public static Logger getLogger(Class<?> clazz) {
		return Logger.getLogger(clazz);
	}
	
	public static void setLevel(Level level) {
		logger.setLevel(level);
	}
	
	static {
		System.out.println("Invoking basic onfiguration for Log4J...");
//		BasicConfigurator.configure();
		setLevel(Level.DEBUG);
	}
}
