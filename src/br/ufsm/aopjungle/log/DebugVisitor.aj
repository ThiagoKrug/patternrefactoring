package br.ufsm.aopjungle.log;

import org.aspectj.org.eclipse.jdt.core.dom.AjASTVisitor;
import org.aspectj.org.eclipse.jdt.core.dom.BeforeAdviceDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.Statement;

public privileged aspect DebugVisitor {
	pointcut traceVisitorAdviceBefore() : (execution (* AjASTVisitor+.visit(..)) || 
			                               execution (* AjASTVisitor+.endVisit(..))) 
	                                    && (args(Statement+) || args(BeforeAdviceDeclaration+));
	
//	before() : traceVisitorAdviceBefore() {
//		System.out.println("[Tracing adviceBefore] - " + thisJoinPointStaticPart.getSignature().getName()
//				+ " " + thisJoinPoint.toLongString()); 
//	}
	
//	pointcut visitorsExecution (AS	TNode node): execution (* AOJungleVisitor.visit(ASTNode+)) && args(node) && ! within(DebugVisitor);
//	pointcut visitorTest (DeclareParentsDeclaration node): 
//		execution (public boolean visit(DeclareParentsDeclaration+)) &&
//		args(node) && 
//		! within(DebugVisitor);
/*
	before (DeclareParentsDeclaration node) : visitorTest (node) {
		AOJLogger.getLogger().debug(">>"+thisJoinPointStaticPart.getSignature()+"\n"+node.getClass().getName() + "." + node.toString());
		AOJLogger.getLogger().debug("Child ==> " + node.getChildTypePattern().getTypePatternExpression().toString());
		for (Object t : node.parentTypePatterns()) {
			AOJLogger.getLogger().debug("Parents ==> " + ((TypePattern)t).getTypePatternExpression().toString());
		}
	}
	
*/	
	
/*	after (Object node) : call (* java.util.Stack+.push(Object)) && args (node) {
		if (node instanceof AOJTypeble)
			System.out.println("Push Stack = " + ((AOJTypeble)node).getName() + "[" + node.getClass().getName() + "]");
	}

	after () : call (Object java.util.Stack+.pop()) {
		if ( (! AOJungleVisitor.getElementStack().isEmpty() && (AOJungleVisitor.getElementStack().peek() instanceof AOJTypeble) ) ){
			AOJTypeble t = (AOJTypeble)AOJungleVisitor.getElementStack().peek();
			System.out.println("Pop Stack = " + t.getName() + "[" + t.getClass().getName() + "]");
		}
	}
*/	
/*	
	after (AOJungleVisitor node) : execution (* AOJungleVisitor.visit(TypeDeclaration)) && this (node) {
		if (! AOJungleVisitor.getElementStack().isEmpty() )
			System.out.println("After visit(TypeDeclaration) : " + ((AOJAbstractTypeDeclaration)node.getLastMember()).getName());
	}

	before (AOJungleVisitor node) : execution (* AOJungleVisitor.endVisit(TypeDeclaration)) && this (node) {
		if (! AOJungleVisitor.getElementStack().isEmpty() )
			System.out.println("Before endVisitor(TypeDeclaration) : " + ((AOJAbstractTypeDeclaration)node.getLastMember()).getName());
	}
	
	after (AOJungleVisitor node) : execution (* AOJungleVisitor.endVisit(TypeDeclaration)) && this (node) {
		if (! AOJungleVisitor.getElementStack().isEmpty() )	
			System.out.println("After endVisitor(TypeDeclaration) : " + ((AOJAbstractTypeDeclaration)node.getLastMember()).getName());
	}
*/	
//	before (ASTNode node) : visitorsExecution (node) {
//		AOJLogger.getLogger().debug(">>"+thisJoinPointStaticPart.getSignature()+"\n"+node.getClass().getName() + "." + node.toString());
//	}
}
