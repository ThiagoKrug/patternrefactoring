package br.ufsm.aopjungle.binding;

import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJPackageDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

public class AOJInheritanceResolver {
	private AOJProject project;
	
	public AOJInheritanceResolver (AOJProject project) {
		this.project = project;
	}
	
	/**
	 * Resolves parent attribute (from AST) to AOJ Model object
	 */
	public void resolve() {
		for (AOJPackageDeclaration pack : project.getPackages()) {
			for (AOJCompilationUnit cunit : pack.getCompilationUnits()) {
				for (AOJContainer type : cunit.getDeclaredTypes()) {
					if (type.getSuperType() != null) {
					//if (type.getParent() != null) {
						
						AOJContainer parent = project.getBindingMapping().get(type.getSuperType().getFullQualifiedName());
						//AOJContainer parent = project.getBindingMapping().get(type.getParent().getFullQualifiedName());
						if (parent != null) {
							type.setSuperType(parent);
							parent.addSubType(type);
						}
					}
				}	
			}	
		}
	}	
}
