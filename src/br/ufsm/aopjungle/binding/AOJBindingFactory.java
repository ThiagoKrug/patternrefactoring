package br.ufsm.aopjungle.binding;

/**
 * The interface of Binding Model Factory
 * 
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
public interface AOJBindingFactory {
	AOJBindingFactory eINSTANCE = AOJBindingFactoryImpl.init();
	AOJBinding createBinding();
}
