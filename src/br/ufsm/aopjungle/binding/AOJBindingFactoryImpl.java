package br.ufsm.aopjungle.binding;

/**
 * Implementation of Binding Model Factory
 * 
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
public class AOJBindingFactoryImpl implements AOJBindingFactory {
	  private static AOJBindingFactory factory;

	  public static AOJBindingFactory init() {
		  if (factory == null)
			  factory = new AOJBindingFactoryImpl(); 
		  return factory;
	  } 
	  
	@Override
	public AOJBinding createBinding() {
		AOJBinding binding = new AOJBinding();
		return binding;
	}
}
