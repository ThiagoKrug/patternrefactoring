package br.ufsm.aopjungle.views;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;

import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.util.IconProvider;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeAttribute;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeCodeSmell;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeConstructorMethod;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeElement;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeEnum;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeMethod;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeObject;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreePackage;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeProject;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeType;

public class ViewLabelProvider extends LabelProvider implements IStyledLabelProvider {

	private static final String FOREGROUND_COLOR_STYLER = "custom_foreground_color";
	private Map<ImageDescriptor, Image> imageCache = new HashMap<ImageDescriptor, Image>();

	@Override
	public String getText(Object obj) {
		return ((TreeObject) obj).getName();
	}

	@Override
	public Image getImage(Object obj) {
		ImageDescriptor descriptor = null;
		if (obj instanceof TreeCodeSmell)
			if (((TreeCodeSmell) obj).hasChildren())
				descriptor = IconProvider.getImageDescriptor("redicon.jpeg");
			else
				descriptor = IconProvider.getImageDescriptor("greenicon.jpeg");
		else if (obj instanceof TreeProject)
			descriptor = IconProvider.getImageDescriptor("ajproject.gif");
		else if (obj instanceof TreePackage)
			descriptor = IconProvider.getImageDescriptor("package.gif");
		else if (obj instanceof TreeEnum)
			descriptor = IconProvider.getImageDescriptor("enum_obj.png");
		else if (obj instanceof TreeType)
			descriptor = IconProvider.getImageDescriptor("class.gif");
		else if (obj instanceof TreeElement)
			descriptor = IconProvider.getImageDescriptor("report.gif");
		else if (obj instanceof TreeMethod)
			descriptor = IconProvider.getImageDescriptor("methpub_obj.png");
		else if (obj instanceof TreeConstructorMethod)
			descriptor = IconProvider.getImageDescriptor("constructor_method.png");
		else if (obj instanceof TreeAttribute)
			descriptor = IconProvider.getImageDescriptor("attribute_obj.png");
		else if (obj instanceof TreeObject)
			descriptor = IconProvider.getImageDescriptor("report.gif");
		else
			descriptor = IconProvider.getImageDescriptor("folders.gif");

		// obtain the cached image corresponding to the descriptor
		Image image = (Image) imageCache.get(descriptor);
		if (image == null) {
			image = descriptor.createImage();
			imageCache.put(descriptor, image);
		}
		return image;
	}

	@Override
	public StyledString getStyledText(Object element) {
		StyledString ss = new StyledString(((TreeObject) element).getName());

		ColorRegistry colorRegistry = JFaceResources.getColorRegistry();
		colorRegistry.put(FOREGROUND_COLOR_STYLER, new RGB(128, 128, 128));
		Styler style = StyledString.createColorRegistryStyler(FOREGROUND_COLOR_STYLER, null);

		if (element instanceof TreePackage)
			ss.append(" - " + ((TreeElement) (((TreePackage) element).getChild(0).getChild(0))).getRefactoringOpportunity().getClazz().getProject().getName(), style);

		else if (element instanceof TreeType) {
			AOJTypeDeclaration clazz = ((TreeElement) ((TreeType) element).getChild(0)).getRefactoringOpportunity().getClazz();
			ss.append(" - " + clazz.getPackage().getName() + " - " + clazz.getProject().getName(), style);
		}

		return ss;
	}

	@Override
	public void dispose() {
		for (Iterator<Image> i = imageCache.values().iterator(); i.hasNext();) {
			i.next().dispose();
		}
		imageCache.clear();
	}

	/*
	 * @Override public String getText(Object obj) { return obj.toString(); }
	 * 
	 * @Override public Image getImage(Object obj) { String imageKey =
	 * ISharedImages.IMG_OBJ_ELEMENT; if (obj instanceof TreeParent) imageKey =
	 * ISharedImages.IMG_OBJ_FOLDER; return
	 * PlatformUI.getWorkbench().getSharedImages().getImage(imageKey); }
	 */
}