package br.ufsm.aopjungle.views.patternrefactoring;

import javax.swing.JOptionPane;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

public class SelectProjectDialog extends TitleAreaDialog {

	private List projectsList;
	private String[] selectedProjects;

	public SelectProjectDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	public void create() {
		super.create();
		setTitle("AOPJungle");
		setMessage("Selecione os projetos: ", IMessageProvider.INFORMATION);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);

		createProjectsList(area);
		createButtons(area);

		Button okButton = getButton(IDialogConstants.OK_ID);
		if (okButton != null) {
			okButton.addListener(SWT.Selection, new Listener() {
				@Override
				public void handleEvent(Event event) {
					saveSelectedProjects();
					if (selectedProjects.length == 0) {
						JOptionPane.showMessageDialog(null, "Selecione pelo menos um projeto.", "", JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}

		return area;
	}

	private void createProjectsList(Composite area) {
		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);

		projectsList = new List(container, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		projectsList.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
	}

	private void createButtons(Composite area) {
		Composite container = new Composite(area, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
		GridLayout layout = new GridLayout(2, false);
		container.setLayout(layout);

		Button selectAll = new Button(container, SWT.NONE);
		selectAll.setText("Selecionar tudo");
		selectAll.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				projectsList.selectAll();
			}
		});

		Button deselectAll = new Button(container, SWT.NONE);
		deselectAll.setText("Deselecionar tudo");
		deselectAll.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				projectsList.deselectAll();
			}
		});

		GridData data = new GridData();
		data.grabExcessHorizontalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		selectAll.setLayoutData(data);
		deselectAll.setLayoutData(data);
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	private void saveSelectedProjects() {
		selectedProjects = projectsList.getSelection();
	}

	@Override
	protected void okPressed() {
		saveSelectedProjects();
		if (selectedProjects.length > 0) {
			super.okPressed();
		} else {
			JOptionPane.showMessageDialog(null, "Selecione pelo menos um projeto.", "AOPJungle", JOptionPane.ERROR_MESSAGE);
		}
	}

	public String[] getSelectedProjects() {
		return selectedProjects;
	}

	public void setSelectedProjects(String[] selectedProjects) {
		this.selectedProjects = selectedProjects;
	}

	public List getProjectsList() {
		return projectsList;
	}

	public void setProjectsList(List projectsList) {
		this.projectsList = projectsList;
	}
}
