package br.ufsm.aopjungle.views.patternrefactoring;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import br.ufsm.aopjungle.patternrefactoring.util.PRProp;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.CBanner;
import org.eclipse.swt.custom.ViewForm;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.swt.widgets.Table;
import org.eclipse.jface.viewers.CheckboxTableViewer;

public class teste extends Composite {
	private Text txtWeight1;
	private Text txtWeight2;
	private Text txtWeight3;
	private Text txtWeight4;
	private Text txtWeight5;
	private Text txtSubtipos;
	private Text txtClasseMesmoPacote;
	private Text txtModificadoresPublicos;
	private Text txtVisitor;
	private Text txtSubtypeCalls;
	private Table table;

	public teste(Composite parent, int style) {
		super(parent, style);

		Label lblWeight4 = new Label(this, SWT.NONE);
		lblWeight4.setText("Weight 4");
		lblWeight4.setBounds(273, 248, 47, 15);
		
		Label lblWeight5 = new Label(this, SWT.NONE);
		lblWeight5.setText("Weight 5");
		lblWeight5.setBounds(273, 272, 47, 15);

		Label lblSubtipos = new Label(this, SWT.NONE);
		lblSubtipos.setText("Subtipos");
		lblSubtipos.setBounds(10, 176, 143, 15);

		Label lblClassesNoMesmoPacote = new Label(this, SWT.NONE);
		lblClassesNoMesmoPacote.setText("Classes no mesmo pacote");
		lblClassesNoMesmoPacote.setBounds(10, 200, 143, 15);

		Label lblModificadoresPublicos = new Label(this, SWT.NONE);
		lblModificadoresPublicos.setText("Modificadores p�blicos");
		lblModificadoresPublicos.setBounds(10, 224, 143, 15);

		Label lblVisitor = new Label(this, SWT.NONE);
		lblVisitor.setText("Visitor");
		lblVisitor.setBounds(10, 248, 143, 15);

		Label lblValoresDeNormalizao = new Label(this, SWT.NONE);
		lblValoresDeNormalizao.setBounds(10, 154, 143, 15);
		lblValoresDeNormalizao.setText("Valores dos Par�metros");
		
		ControlDecoration controlDecoration_1 = new ControlDecoration(lblValoresDeNormalizao, SWT.LEFT | SWT.TOP);
		controlDecoration_1.setDescriptionText("Some description");

		Label lblSubtypeCalls = new Label(this, SWT.NONE);
		lblSubtypeCalls.setText("Subtype Calls");
		lblSubtypeCalls.setBounds(10, 272, 143, 15);

		txtVisitor = new Text(this, SWT.BORDER);
		txtVisitor.setBounds(159, 245, 76, 21);
		txtVisitor.setText(String.valueOf(PRProp.getECWFVisitor()));
		txtVisitor.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		
		txtSubtypeCalls = new Text(this, SWT.BORDER);
		txtSubtypeCalls.setBounds(159, 269, 76, 21);
		txtSubtypeCalls.setText(String.valueOf(PRProp.getECWFSubtypesCalls()));
		txtSubtypeCalls.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		
		txtWeight5 = new Text(this, SWT.BORDER);
		txtWeight5.setBounds(325, 269, 76, 21);
		txtWeight5.setText(String.valueOf(PRProp.getECWFWeight5()));
		txtWeight5.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		
		Button btnRestaurarValoresPadrao = new Button(this, SWT.NONE);
		btnRestaurarValoresPadrao.setBounds(10, 296, 157, 25);
		btnRestaurarValoresPadrao.setText("Restaurar valores padr�o");
		
		btnRestaurarValoresPadrao.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				PRProp.loadDefaultProperties();
				
				txtSubtipos.setText(String.valueOf(PRProp.getECWFSubtypes()));
				txtWeight1.setText(String.valueOf(PRProp.getECWFWeight1()));
				txtClasseMesmoPacote.setText(String.valueOf(PRProp.getECWFClassesInTheSamePackage()));
				txtWeight2.setText(String.valueOf(PRProp.getECWFWeight2()));
				txtModificadoresPublicos.setText(String.valueOf(PRProp.getECWFPublicModificators()));
				txtWeight3.setText(String.valueOf(PRProp.getECWFWeight3()));
				txtVisitor.setText(String.valueOf(PRProp.getECWFVisitor()));
				txtWeight4.setText(String.valueOf(PRProp.getECWFWeight4()));
				txtSubtypeCalls.setText(String.valueOf(PRProp.getECWFSubtypesCalls()));
				txtWeight5.setText(String.valueOf(PRProp.getECWFWeight5()));
			}
		});
		this.setText();
		
		SashForm sashForm = new SashForm(this, SWT.NONE);
		sashForm.setBounds(10, 31, 132, 103);
		
		txtSubtipos = new Text(sashForm, SWT.BORDER);
		txtSubtipos.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		txtSubtipos.setText(String.valueOf(PRProp.getECWFSubtypes()));
		
				txtClasseMesmoPacote = new Text(sashForm, SWT.BORDER);
				txtClasseMesmoPacote.setText(String.valueOf(PRProp.getECWFClassesInTheSamePackage()));
				
				ControlDecoration controlDecoration = new ControlDecoration(txtClasseMesmoPacote, SWT.LEFT | SWT.TOP);
				controlDecoration.setDescriptionText("Some description");
				
						txtModificadoresPublicos = new Text(sashForm, SWT.BORDER);
						txtModificadoresPublicos.setText(String.valueOf(PRProp.getECWFPublicModificators()));
						txtModificadoresPublicos.addFocusListener(new FocusAdapter() {
							@Override
							public void focusLost(FocusEvent e) {
								setText();
							}
						});
				txtClasseMesmoPacote.addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent e) {
						setText();
					}
				});
		sashForm.setWeights(new int[] {1, 1, 1});
		
		CBanner banner = new CBanner(this, SWT.NONE);
		banner.setBounds(152, 40, 109, 70);
		
				Label lblPesos = new Label(banner, SWT.NONE);
				banner.setLeft(lblPesos);
				lblPesos.setText("Pesos");
				
				Label lblWeight1 = new Label(banner, SWT.NONE);
				banner.setRight(lblWeight1);
				lblWeight1.setText("Weight 1");
				
				ViewForm viewForm = new ViewForm(this, SWT.NONE);
				viewForm.setBounds(177, 154, 88, 61);
				
						txtWeight2 = new Text(viewForm, SWT.BORDER);
						viewForm.setTopLeft(txtWeight2);
						txtWeight2.setText(String.valueOf(PRProp.getECWFWeight2()));
						txtWeight2.addFocusListener(new FocusAdapter() {
							@Override
							public void focusLost(FocusEvent e) {
								setText();
							}
						});
				
						txtWeight3 = new Text(viewForm, SWT.BORDER);
						viewForm.setTopCenter(txtWeight3);
						txtWeight3.setText(String.valueOf(PRProp.getECWFWeight3()));
						txtWeight3.addFocusListener(new FocusAdapter() {
							@Override
							public void focusLost(FocusEvent e) {
								setText();
							}
						});
				
						txtWeight4 = new Text(viewForm, SWT.BORDER);
						viewForm.setTopRight(txtWeight4);
						txtWeight4.setText(String.valueOf(PRProp.getECWFWeight4()));
						
								Label lblWeight3 = new Label(viewForm, SWT.NONE);
								viewForm.setContent(lblWeight3);
								lblWeight3.setText("Weight 3");
						txtWeight4.addFocusListener(new FocusAdapter() {
							@Override
							public void focusLost(FocusEvent e) {
								setText();
							}
						});
				
				txtWeight1 = new Text(this, SWT.BORDER);
				txtWeight1.setBounds(313, 173, 88, 38);
				txtWeight1.setText(String.valueOf(PRProp.getECWFWeight1()));
				
				ToolBar toolBar = new ToolBar(this, SWT.FLAT | SWT.RIGHT);
				toolBar.setBounds(152, 125, 109, 23);
				
				ToolItem tltmNewItem = new ToolItem(toolBar, SWT.NONE);
				tltmNewItem.setText("New Item");
				
				CheckboxTableViewer checkboxTableViewer = CheckboxTableViewer.newCheckList(this, SWT.BORDER | SWT.FULL_SELECTION);
				table = checkboxTableViewer.getTable();
				table.setBounds(274, 57, 157, 85);
				txtWeight1.addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent e) {
						setText();
					}
				});
	}
	
	private void setText() {
	}

	public void saveInformation() {
		PRProp.getProperties().setProperty(PRProp.ECWF_ClassesInTheSamePackage, txtClasseMesmoPacote.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_PublicModificators, txtModificadoresPublicos.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Subtypes, txtSubtipos.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Visitor, txtVisitor.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_SubtypesCalls, txtSubtypeCalls.getText());
		
		PRProp.getProperties().setProperty(PRProp.ECWF_Weight1, txtWeight1.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Weight2, txtWeight2.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Weight3, txtWeight3.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Weight4, txtWeight4.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Weight5, txtWeight5.getText());
	}
}
