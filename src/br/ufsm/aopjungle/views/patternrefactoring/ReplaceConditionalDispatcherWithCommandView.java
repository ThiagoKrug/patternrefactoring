package br.ufsm.aopjungle.views.patternrefactoring;

import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IViewSite;

import br.ufsm.aopjungle.patternrefactoring.ReplaceConditionalDispatcherWithCommand;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.AverageLOCPerConditionalDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.AverageLOCPerConditionalUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.QuantityOfConditionalsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.QuantityOfConditionalsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.QuantityOfLOCDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.QuantityOfLOCUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.QuantityOfSimpleExpressionsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.QuantityOfSimpleExpressionsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultAverageLOCPerConditionalDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultAverageLOCPerConditionalUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultQuantityOfConditionalsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultQuantityOfConditionalsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultQuantityOfLOCDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultQuantityOfLOCUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultQuantityOfSimpleExpressionsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultQuantityOfSimpleExpressionsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultSimpleConditionsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultSimpleConditionsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.SimpleConditionsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.SimpleConditionsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.providers.AverageLOCPerConditionalLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.QuantityOfConditionalsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.QuantityOfLOCLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.QuantityOfSimpleExpressionsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultAverageLOCPerConditionalLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultQuantityOfConditionalsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultQuantityOfLOCLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultQuantityOfSimpleExpressionsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultSimpleConditionsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.SimpleConditionsLabelProvider;

public class ReplaceConditionalDispatcherWithCommandView extends MechanicCompositeView {

	public void createView(CTabFolder tabFolder, IViewSite viewSite, Composite parent) {
		super.createView(tabFolder, viewSite, parent, new ReplaceConditionalDispatcherWithCommand().getLabel());
	}

	protected void fillContent() {
		treeViewerColumnBuilder(viewer, "Result of conditionals", 100, new ResultQuantityOfConditionalsLabelProvider(), new ResultQuantityOfConditionalsUpComparator(),
				new ResultQuantityOfConditionalsDownComparator());
		treeViewerColumnBuilder(viewer, "Result of LOC", 100, new ResultQuantityOfLOCLabelProvider(), new ResultQuantityOfLOCUpComparator(), new ResultQuantityOfLOCDownComparator());
		treeViewerColumnBuilder(viewer, "Result of Simple Expressions", 100, new ResultQuantityOfSimpleExpressionsLabelProvider(), new ResultQuantityOfSimpleExpressionsUpComparator(),
				new ResultQuantityOfSimpleExpressionsDownComparator());
		treeViewerColumnBuilder(viewer, "Result of Average LOC per Conditional", 100, new ResultAverageLOCPerConditionalLabelProvider(), new ResultAverageLOCPerConditionalUpComparator(),
				new ResultAverageLOCPerConditionalDownComparator());
		treeViewerColumnBuilder(viewer, "Result of Simple Conditions", 100, new ResultSimpleConditionsLabelProvider(), new ResultSimpleConditionsUpComparator(),
				new ResultSimpleConditionsDownComparator());
		treeViewerColumnBuilder(viewer, "N� of conditionals", 100, new QuantityOfConditionalsLabelProvider(), new QuantityOfConditionalsUpComparator(), new QuantityOfConditionalsDownComparator());
		treeViewerColumnBuilder(viewer, "N� of LOC", 100, new QuantityOfLOCLabelProvider(), new QuantityOfLOCUpComparator(), new QuantityOfLOCDownComparator());
		treeViewerColumnBuilder(viewer, "N� of Simple Expressions", 100, new QuantityOfSimpleExpressionsLabelProvider(), new QuantityOfSimpleExpressionsUpComparator(),
				new QuantityOfSimpleExpressionsDownComparator());
		treeViewerColumnBuilder(viewer, "Average LOC per Conditional", 100, new AverageLOCPerConditionalLabelProvider(), new AverageLOCPerConditionalUpComparator(),
				new AverageLOCPerConditionalDownComparator());
		treeViewerColumnBuilder(viewer, "Average of Simple Conditions", 100, new SimpleConditionsLabelProvider(), new SimpleConditionsUpComparator(), new SimpleConditionsDownComparator());
	}

}
