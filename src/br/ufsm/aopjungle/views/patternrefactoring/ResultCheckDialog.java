package br.ufsm.aopjungle.views.patternrefactoring;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ResultCheckDialog extends TitleAreaDialog {
	private Text text;
	private String message;

	public ResultCheckDialog(Shell parentShell, String message) {
		super(parentShell);
		this.message = message;
	}

	@Override
	public void create() {
		super.create();
		setTitle("Pattern Refactoring Result Check");
		setMessage("Resultado da checagem", IMessageProvider.NONE);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		area.setLayout(null);
		
		text = new Text(area, SWT.BORDER);
		text.setBounds(10, 10, 414, 207);
		text.setText(this.message);

		return area;
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	@Override
	protected void okPressed() {
		super.okPressed();
	}
}
