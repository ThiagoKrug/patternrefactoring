package br.ufsm.aopjungle.views.patternrefactoring;

import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IViewSite;

import br.ufsm.aopjungle.patternrefactoring.EncapsulateClassWithFactory;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.NumberOfClassesInSamePackageDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.NumberOfClassesInSamePackageUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.NumberOfPublicModificatorsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.NumberOfPublicModificatorsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.NumberOfSubtypesCallsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.NumberOfSubtypesCallsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.NumberOfSubtypesDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.NumberOfSubtypesUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultNumberOfClassesInSamePackageDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultNumberOfClassesInSamePackageUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultNumberOfPublicModificatorsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultNumberOfPublicModificatorsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultNumberOfSubtypesCallsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultNumberOfSubtypesCallsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultNumberOfSubtypesDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultNumberOfSubtypesUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultVisitorDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultVisitorUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.VisitorDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.VisitorUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.providers.NumberOfClassesInSamePackageLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.NumberOfPublicModificatorsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.NumberOfSubtypesCallsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.NumberOfSubtypesLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultNumberOfClassesInSamePackageLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultNumberOfPublicModificatorsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultNumberOfSubtypesCallsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultNumberOfSubtypesLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultVisitorLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.VisitorLabelProvider;

public class EncapsulateClassesWithFactoryView extends MechanicCompositeView {

	public void createView(CTabFolder tabFolder, IViewSite viewSite, Composite parent) {
		super.createView(tabFolder, viewSite, parent, new EncapsulateClassWithFactory().getLabel());
	}

	protected void fillContent() {
		treeViewerColumnBuilder(viewer, "Result of subtypes", 100, new ResultNumberOfSubtypesLabelProvider(), new ResultNumberOfSubtypesUpComparator(), new ResultNumberOfSubtypesDownComparator());
		treeViewerColumnBuilder(viewer, "Result of classes in same package", 100, new ResultNumberOfClassesInSamePackageLabelProvider(), new ResultNumberOfClassesInSamePackageUpComparator(),
				new ResultNumberOfClassesInSamePackageDownComparator());
		treeViewerColumnBuilder(viewer, "Result of public modificators", 100, new ResultNumberOfPublicModificatorsLabelProvider(), new ResultNumberOfPublicModificatorsUpComparator(),
				new ResultNumberOfPublicModificatorsDownComparator());
		treeViewerColumnBuilder(viewer, "Result visitor", 100, new ResultVisitorLabelProvider(), new ResultVisitorUpComparator(), new ResultVisitorDownComparator());
		treeViewerColumnBuilder(viewer, "Result of subtypes calls", 100, new ResultNumberOfSubtypesCallsLabelProvider(), new ResultNumberOfSubtypesCallsUpComparator(),
				new ResultNumberOfSubtypesCallsDownComparator());
		treeViewerColumnBuilder(viewer, "N� of subtypes", 100, new NumberOfSubtypesLabelProvider(), new NumberOfSubtypesUpComparator(), new NumberOfSubtypesDownComparator());
		treeViewerColumnBuilder(viewer, "N� of classes in same package", 100, new NumberOfClassesInSamePackageLabelProvider(), new NumberOfClassesInSamePackageUpComparator(),
				new NumberOfClassesInSamePackageDownComparator());
		treeViewerColumnBuilder(viewer, "N� of public modificators", 100, new NumberOfPublicModificatorsLabelProvider(), new NumberOfPublicModificatorsUpComparator(),
				new NumberOfPublicModificatorsDownComparator());
		treeViewerColumnBuilder(viewer, "Visitor", 100, new VisitorLabelProvider(), new VisitorUpComparator(), new VisitorDownComparator());
		treeViewerColumnBuilder(viewer, "N� of subtypes calls", 100, new NumberOfSubtypesCallsLabelProvider(), new NumberOfSubtypesCallsUpComparator(), new NumberOfSubtypesCallsDownComparator());
	}
}
