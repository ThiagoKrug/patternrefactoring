package br.ufsm.aopjungle.views.patternrefactoring.parameters;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import br.ufsm.aopjungle.patternrefactoring.util.PRProp;

public class ParametersAndWeightsDialog extends TitleAreaDialog {
	
	private RefactoringsSelectComposite rsc;
	private ECWFComposite ecwfTabItem;
	private RCDWCComposite rcdwcTabItem;
	private RITWCComposite ritwcTabItem;

	public ParametersAndWeightsDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	public void create() {
		super.create();
		setTitle("Pattern Refactoring");
		setMessage("Informe as configura��es", IMessageProvider.NONE);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		area.setLayout(null);
		
		CTabFolder tabFolder = new CTabFolder(area, SWT.BORDER);
		tabFolder.setBounds(0, 0, 600, 341);
		tabFolder.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
		
		CTabItem tabItem = new CTabItem(tabFolder, SWT.NONE);
		tabItem.setText("Mec�nicas de Busca");
		rsc = new RefactoringsSelectComposite(tabFolder, SWT.NONE);
		tabItem.setControl(rsc);
		
		CTabItem tabItem2 = new CTabItem(tabFolder, SWT.NONE);
		tabItem2.setText("Encapsular Classes com Factory");
		ecwfTabItem = new ECWFComposite(tabFolder, SWT.NONE);		
		tabItem2.setControl(ecwfTabItem);
		
		CTabItem tabItem3 = new CTabItem(tabFolder, SWT.NONE);
		tabItem3.setText("Substituir Envio Condicional por Command");
		rcdwcTabItem = new RCDWCComposite(tabFolder, SWT.NONE);
		tabItem3.setControl(rcdwcTabItem);
		
		CTabItem tabItem4 = new CTabItem(tabFolder, SWT.NONE);
		tabItem4.setText("Substituir �rvore Impl�cita por Composite");
		ritwcTabItem = new RITWCComposite(tabFolder, SWT.NONE);
		tabItem4.setControl(ritwcTabItem);
		
		return area;
	}

	@Override
	protected boolean isResizable() {
		return false;
	}

	@Override
	protected void okPressed() {
		this.saveInformation();
		super.okPressed();
	}
	
	private void saveInformation() {
		rsc.saveInformation();
		ecwfTabItem.saveInformation();
		rcdwcTabItem.saveInformation();
		ritwcTabItem.saveInformation();
		
		PRProp.saveProperties();
	}
	
	public static void main(String[] args) {
		ParametersAndWeightsDialog p = new ParametersAndWeightsDialog(null);
		p.create();
		p.open();
	}
}
