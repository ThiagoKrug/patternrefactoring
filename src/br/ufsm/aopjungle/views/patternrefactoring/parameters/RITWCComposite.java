package br.ufsm.aopjungle.views.patternrefactoring.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import br.ufsm.aopjungle.patternrefactoring.util.PRProp;

public class RITWCComposite extends Composite {
	
	Label lblFormulaHeuristica;
	private Text txtWeight1;
	private Text txtWeight2;
	private Text txtWeight3;
	private Text txtQuantOfTags;
	private Text txtQuantOfClosedTags;
	private Text txtSumOfRelevanceOfTags;
	
	public RITWCComposite(Composite parent, int style) {
		super(parent, style);
		
		int innerSpan = 5;
		int outerSpan = 20;
		int labelHeight = 15;
		int labelParameterWidth = 206;
		int labelWeightWidth = 47;
		int textWidth = 76;
		int textHeight = 21;
		
		// X positioning
		int labelParameterXPosition = 10;
		int textParameterXPosition = labelParameterXPosition + innerSpan + labelParameterWidth;
		int labelWeightXPosition = textParameterXPosition + outerSpan + textWidth;
		int textWeightXPosition = labelWeightXPosition + innerSpan + labelWeightWidth;

		Label lblWeight1 = new Label(this, SWT.NONE);
		lblWeight1.setBounds(317, 145, labelWeightWidth, labelHeight);
		lblWeight1.setText("Weight 1");

		Label lblWeight2 = new Label(this, SWT.NONE);
		lblWeight2.setText("Weight 2");
		lblWeight2.setBounds(317, 169, labelWeightWidth, labelHeight);

		Label lblWeight3 = new Label(this, SWT.NONE);
		lblWeight3.setText("Weight 3");
		lblWeight3.setBounds(317, 193, labelWeightWidth, labelHeight);
		
		Label lblQuantTags = new Label(this, SWT.NONE);
		lblQuantTags.setText("Quantidade de tags");
		lblQuantTags.setBounds(10, 145, labelParameterWidth, labelHeight);

		Label lblQuantClosedTags = new Label(this, SWT.NONE);
		lblQuantClosedTags.setText("Quantidade de tags fechadas");
		lblQuantClosedTags.setBounds(10, 169, labelParameterWidth, labelHeight);

		Label lblSumOfRelevanceOfTags = new Label(this, SWT.NONE);
		lblSumOfRelevanceOfTags.setText("Soma da relev�ncia das tags");
		lblSumOfRelevanceOfTags.setBounds(10, 193, labelParameterWidth, labelHeight);

		Label lblValoresDeNormalizao = new Label(this, SWT.NONE);
		lblValoresDeNormalizao.setBounds(10, 123, labelParameterWidth, labelHeight);
		lblValoresDeNormalizao.setText("Valores dos Par�metros");

		Label lblPesos = new Label(this, SWT.NONE);
		lblPesos.setText("Pesos");
		lblPesos.setBounds(317, 124, 47, labelHeight);
		
		txtQuantOfTags = new Text(this, SWT.BORDER);
		txtQuantOfTags.setBounds(221, 142, textWidth, textHeight);
		txtQuantOfTags.setText(String.valueOf(PRProp.getRITWCQuantityOfTags()));
		txtQuantOfTags.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtWeight1 = new Text(this, SWT.BORDER);
		txtWeight1.setBounds(369, 142, textWidth, textHeight);
		txtWeight1.setText(String.valueOf(PRProp.getRITWCWeight1()));
		txtWeight1.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtQuantOfClosedTags = new Text(this, SWT.BORDER);
		txtQuantOfClosedTags.setBounds(221, 166, textWidth, textHeight);
		txtQuantOfClosedTags.setText(String.valueOf(PRProp.getRITWCQuantityOfClosedTags()));
		txtQuantOfClosedTags.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtWeight2 = new Text(this, SWT.BORDER);
		txtWeight2.setBounds(369, 166, textWidth, textHeight);
		txtWeight2.setText(String.valueOf(PRProp.getRITWCWeight2()));
		txtWeight2.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtSumOfRelevanceOfTags = new Text(this, SWT.BORDER);
		txtSumOfRelevanceOfTags.setBounds(221, 190, textWidth, textHeight);
		txtSumOfRelevanceOfTags.setText(String.valueOf(PRProp.getRITWCRelevanceTags()));
		txtSumOfRelevanceOfTags.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtWeight3 = new Text(this, SWT.BORDER);
		txtWeight3.setBounds(369, 190, textWidth, textHeight);
		txtWeight3.setText(String.valueOf(PRProp.getRITWCWeight3()));
		txtWeight3.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		
		lblFormulaHeuristica = new Label(this, SWT.WRAP);
		lblFormulaHeuristica.setBounds(10, 10, 545, 110);
		setText();
		lblFormulaHeuristica.setFont(new Font(lblFormulaHeuristica.getDisplay(), "Courier New", 10, SWT.BOLD));
		
		Button btnRestaurarValoresPadrao = new Button(this, SWT.NONE);
		btnRestaurarValoresPadrao.setBounds(10, 265, 157, 25);
		btnRestaurarValoresPadrao.setText("Restaurar valores padr�o");
		btnRestaurarValoresPadrao.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				PRProp.loadDefaultProperties();
				
				txtQuantOfTags.setText(String.valueOf(PRProp.getRITWCQuantityOfTags()));
				txtWeight1.setText(String.valueOf(PRProp.getRITWCWeight1()));
				txtQuantOfClosedTags.setText(String.valueOf(PRProp.getRITWCQuantityOfClosedTags()));
				txtWeight2.setText(String.valueOf(PRProp.getRITWCWeight2()));
				txtSumOfRelevanceOfTags.setText(String.valueOf(PRProp.getRITWCRelevanceTags()));
				txtWeight3.setText(String.valueOf(PRProp.getRITWCWeight3()));
			}
		});
	}
	
	private void setText() {
		lblFormulaHeuristica.setText("resultado = \n"
				+ "(normalizarRetilineo(quantidadeTags, " + txtQuantOfTags.getText() + ") * " + txtWeight1.getText() + " + \n"
				+ "normalizarRetilineo(quantidadeTagsFechadas, " + txtQuantOfClosedTags.getText() + ") * " + txtWeight2.getText() + " + \n"
				+ "normalizarRetilineo(somaRelevanciaDasTags, " + txtSumOfRelevanceOfTags.getText() + ") * " + txtWeight3.getText());
	}

	public void saveInformation() {
		PRProp.getProperties().setProperty(PRProp.RITWC_QuantityOfTags, txtQuantOfTags.getText());
		PRProp.getProperties().setProperty(PRProp.RITWC_QuantityOfClosedTags, txtQuantOfClosedTags.getText());
		PRProp.getProperties().setProperty(PRProp.RITWC_RelevanceTags, txtSumOfRelevanceOfTags.getText());
		
		PRProp.getProperties().setProperty(PRProp.RITWC_Weight1, txtWeight1.getText());
		PRProp.getProperties().setProperty(PRProp.RITWC_Weight2, txtWeight2.getText());
		PRProp.getProperties().setProperty(PRProp.RITWC_Weight3, txtWeight3.getText());
	}
}
