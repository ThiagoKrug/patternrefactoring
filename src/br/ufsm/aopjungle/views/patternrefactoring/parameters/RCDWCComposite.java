package br.ufsm.aopjungle.views.patternrefactoring.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import br.ufsm.aopjungle.patternrefactoring.util.PRProp;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class RCDWCComposite extends Composite {
	
	Label lblFormulaHeuristica;
	private Text txtWeight1;
	private Text txtWeight2;
	private Text txtWeight3;
	private Text txtWeight4;
	private Text txtWeight5;
	private Text txtQuantCondicionais;
	private Text txtQuantLOC;
	private Text txtMediaLoCPorCondicao;
	private Text txtQuantExpressoesSimples;
	private Text txtMediaCondicoesSimples;
	
	public RCDWCComposite(Composite parent, int style) {
		super(parent, style);
		
		int innerSpan = 5;
		int outerSpan = 20;
		int labelHeight = 15;
		int labelParameterWidth = 206;
		int labelWeightWidth = 47;
		int textWidth = 76;
		int textHeight = 21;
		
		// X positioning
		int labelParameterXPosition = 10;
		int textParameterXPosition = labelParameterXPosition + innerSpan + labelParameterWidth;
		int labelWeightXPosition = textParameterXPosition + outerSpan + textWidth;
		int textWeightXPosition = labelWeightXPosition + innerSpan + labelWeightWidth;

		Label lblWeight1 = new Label(this, SWT.NONE);
		lblWeight1.setBounds(317, 145, labelWeightWidth, labelHeight);
		lblWeight1.setText("Weight 1");

		Label lblWeight2 = new Label(this, SWT.NONE);
		lblWeight2.setText("Weight 2");
		lblWeight2.setBounds(317, 169, labelWeightWidth, labelHeight);

		Label lblWeight3 = new Label(this, SWT.NONE);
		lblWeight3.setText("Weight 3");
		lblWeight3.setBounds(317, 193, labelWeightWidth, labelHeight);

		Label lblWeight4 = new Label(this, SWT.NONE);
		lblWeight4.setText("Weight 4");
		lblWeight4.setBounds(317, 217, labelWeightWidth, labelHeight);
		
		Label lblWeight5 = new Label(this, SWT.NONE);
		lblWeight5.setText("Weight 5");
		lblWeight5.setBounds(317, 241, labelWeightWidth, labelHeight);
		
		Label lblQuantCondicionais = new Label(this, SWT.NONE);
		lblQuantCondicionais.setText("Quantidade de condicionais");
		lblQuantCondicionais.setBounds(10, 145, labelParameterWidth, labelHeight);

		Label lblQuantLOC = new Label(this, SWT.NONE);
		lblQuantLOC.setText("Quantidade de linhas de c�digo (LOC)");
		lblQuantLOC.setBounds(10, 169, labelParameterWidth, labelHeight);

		Label lblMediaLoCPorCondicao = new Label(this, SWT.NONE);
		lblMediaLoCPorCondicao.setText("M�dia de LOC por condi��o");
		lblMediaLoCPorCondicao.setBounds(10, 193, labelParameterWidth, labelHeight);

		Label lblQuantExpressoesSimples = new Label(this, SWT.NONE);
		lblQuantExpressoesSimples.setText("Quantidade de express�es simples");
		lblQuantExpressoesSimples.setBounds(10, 217, labelParameterWidth, labelHeight);

		Label lblMediaCondicoesSimples = new Label(this, SWT.NONE);
		lblMediaCondicoesSimples.setText("M�dia de condi��es simples");
		lblMediaCondicoesSimples.setBounds(10, 241, labelParameterWidth, labelHeight);

		Label lblValoresDeNormalizao = new Label(this, SWT.NONE);
		lblValoresDeNormalizao.setBounds(10, 123, labelParameterWidth, labelHeight);
		lblValoresDeNormalizao.setText("Valores dos Par�metros");

		Label lblPesos = new Label(this, SWT.NONE);
		lblPesos.setText("Pesos");
		lblPesos.setBounds(317, 124, 47, labelHeight);
		
		txtQuantCondicionais = new Text(this, SWT.BORDER);
		txtQuantCondicionais.setBounds(221, 142, textWidth, textHeight);
		txtQuantCondicionais.setText(String.valueOf(PRProp.getRCDWCQuantityOfConditionals()));
		txtQuantCondicionais.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtWeight1 = new Text(this, SWT.BORDER);
		txtWeight1.setBounds(369, 142, textWidth, textHeight);
		txtWeight1.setText(String.valueOf(PRProp.getRCDWCWeight1()));
		txtWeight1.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtQuantLOC = new Text(this, SWT.BORDER);
		txtQuantLOC.setBounds(221, 166, textWidth, textHeight);
		txtQuantLOC.setText(String.valueOf(PRProp.getRCDWCQuantityOfLOC()));
		txtQuantLOC.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtWeight2 = new Text(this, SWT.BORDER);
		txtWeight2.setBounds(369, 166, textWidth, textHeight);
		txtWeight2.setText(String.valueOf(PRProp.getRCDWCWeight2()));
		txtWeight2.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtMediaLoCPorCondicao = new Text(this, SWT.BORDER);
		txtMediaLoCPorCondicao.setBounds(221, 190, textWidth, textHeight);
		txtMediaLoCPorCondicao.setText(String.valueOf(PRProp.getRCDWCAverageLOCPerConditional()));
		txtMediaLoCPorCondicao.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtWeight3 = new Text(this, SWT.BORDER);
		txtWeight3.setBounds(369, 190, textWidth, textHeight);
		txtWeight3.setText(String.valueOf(PRProp.getRCDWCWeight3()));
		txtWeight3.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtQuantExpressoesSimples = new Text(this, SWT.BORDER);
		txtQuantExpressoesSimples.setBounds(221, 214, textWidth, textHeight);
		txtQuantExpressoesSimples.setText(String.valueOf(PRProp.getRCDWCQuantityOfSimpleExpressions()));
		txtQuantExpressoesSimples.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtWeight4 = new Text(this, SWT.BORDER);
		txtWeight4.setBounds(369, 214, textWidth, textHeight);
		txtWeight4.setText(String.valueOf(PRProp.getRCDWCWeight4()));
		txtWeight4.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		
		txtMediaCondicoesSimples = new Text(this, SWT.BORDER);
		txtMediaCondicoesSimples.setBounds(221, 238, textWidth, textHeight);
		txtMediaCondicoesSimples.setText(String.valueOf(PRProp.getRCDWCSimpleConditions()));
		txtMediaCondicoesSimples.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		
		txtWeight5 = new Text(this, SWT.BORDER);
		txtWeight5.setBounds(369, 238, textWidth, textHeight);
		txtWeight5.setText(String.valueOf(PRProp.getRCDWCWeight5()));
		txtWeight5.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		
		lblFormulaHeuristica = new Label(this, SWT.WRAP);
		lblFormulaHeuristica.setBounds(10, 10, 545, 110);
		setText();
		lblFormulaHeuristica.setFont(new Font(lblFormulaHeuristica.getDisplay(), "Courier New", 10, SWT.BOLD));
		
		Button btnRestaurarValoresPadrao = new Button(this, SWT.NONE);
		btnRestaurarValoresPadrao.setBounds(10, 265, 157, 25);
		btnRestaurarValoresPadrao.setText("Restaurar valores padr�o");
		btnRestaurarValoresPadrao.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				PRProp.loadDefaultProperties();
				
				txtQuantCondicionais.setText(String.valueOf(PRProp.getRCDWCQuantityOfConditionals()));
				txtWeight1.setText(String.valueOf(PRProp.getRCDWCWeight1()));
				txtQuantLOC.setText(String.valueOf(PRProp.getRCDWCQuantityOfLOC()));
				txtWeight2.setText(String.valueOf(PRProp.getRCDWCWeight2()));
				txtMediaLoCPorCondicao.setText(String.valueOf(PRProp.getRCDWCAverageLOCPerConditional()));
				txtWeight3.setText(String.valueOf(PRProp.getRCDWCWeight3()));
				txtQuantExpressoesSimples.setText(String.valueOf(PRProp.getRCDWCQuantityOfSimpleExpressions()));
				txtWeight4.setText(String.valueOf(PRProp.getRCDWCWeight4()));
				txtMediaCondicoesSimples.setText(String.valueOf(PRProp.getRCDWCSimpleConditions()));
				txtWeight5.setText(String.valueOf(PRProp.getRCDWCWeight5()));
			}
		});
		
		Button btnPesosApacheAnt = new Button(this, SWT.NONE);
		btnPesosApacheAnt.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				txtWeight1.setText("1.8");
				txtWeight2.setText("0.9");
				txtWeight3.setText("0.4");
				txtWeight4.setText("0.3");
				txtWeight5.setText("0.5");
			}
		});
		btnPesosApacheAnt.setBounds(173, 265, 157, 25);
		btnPesosApacheAnt.setText("Pesos para o Apache-Ant");
	}
	
	private void setText() {
		lblFormulaHeuristica.setText("resultado = \n"
				+ "(normalizarRetilineo(quantidadeCondicionais, " + txtQuantCondicionais.getText() + ") * " + txtWeight1.getText() + " + \n"
				+ "normalizarRetilineo(quantidadeLoC, " + txtQuantLOC.getText() + ") * " + txtWeight2.getText() + " + \n"
				+ "normalizarRetilineo(mediaLoCPorCondicao, " + txtMediaLoCPorCondicao.getText() + ") * " + txtWeight3.getText() + " + \n"
				+ "normalizarRetilineo(quantidadeExpressoesSimples, " + txtQuantExpressoesSimples.getText() + ") * " + txtWeight4.getText() + " + \n" 
				+ "normalizarRetilineo(mediaCondicoesSimples, " + txtMediaCondicoesSimples.getText() + ") * " + txtWeight5.getText() + ")");
	}

	public void saveInformation() {
		PRProp.getProperties().setProperty(PRProp.RCDWC_QuantityOfLOC, txtQuantLOC.getText());
		PRProp.getProperties().setProperty(PRProp.RCDWC_AverageLOCPerConditional, txtMediaLoCPorCondicao.getText());
		PRProp.getProperties().setProperty(PRProp.RCDWC_QuantityOfConditionals, txtQuantCondicionais.getText());
		PRProp.getProperties().setProperty(PRProp.RCDWC_QuantityOfSimpleExpressions, txtQuantExpressoesSimples.getText());
		PRProp.getProperties().setProperty(PRProp.RCDWC_SimpleConditions, txtMediaCondicoesSimples.getText());
		
		PRProp.getProperties().setProperty(PRProp.RCDWC_Weight1, txtWeight1.getText());
		PRProp.getProperties().setProperty(PRProp.RCDWC_Weight2, txtWeight2.getText());
		PRProp.getProperties().setProperty(PRProp.RCDWC_Weight3, txtWeight3.getText());
		PRProp.getProperties().setProperty(PRProp.RCDWC_Weight4, txtWeight4.getText());
		PRProp.getProperties().setProperty(PRProp.RCDWC_Weight5, txtWeight5.getText());
	}
}
