package br.ufsm.aopjungle.views.patternrefactoring.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import br.ufsm.aopjungle.patternrefactoring.util.PRProp;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;

public class ECWFComposite extends Composite {
	
	Label lblFormulaHeuristica;
	private Text txtWeight1;
	private Text txtWeight2;
	private Text txtWeight3;
	private Text txtWeight4;
	private Text txtWeight5;
	private Text txtSubtipos;
	private Text txtClasseMesmoPacote;
	private Text txtModificadoresPublicos;
	private Text txtVisitor;
	private Text txtSubtypeCalls;
	
	public ECWFComposite(Composite parent, int style) {
		super(parent, style);
		
		Label lblWeight1 = new Label(this, SWT.NONE);
		lblWeight1.setBounds(273, 176, 47, 15);
		lblWeight1.setText("Weight 1");

		Label lblWeight2 = new Label(this, SWT.NONE);
		lblWeight2.setText("Weight 2");
		lblWeight2.setBounds(273, 200, 47, 15);

		Label lblWeight3 = new Label(this, SWT.NONE);
		lblWeight3.setText("Weight 3");
		lblWeight3.setBounds(273, 224, 47, 15);

		Label lblWeight4 = new Label(this, SWT.NONE);
		lblWeight4.setText("Weight 4");
		lblWeight4.setBounds(273, 248, 47, 15);
		
		Label lblWeight5 = new Label(this, SWT.NONE);
		lblWeight5.setText("Weight 5");
		lblWeight5.setBounds(273, 272, 47, 15);

		Label lblSubtipos = new Label(this, SWT.NONE);
		lblSubtipos.setText("Subtipos");
		lblSubtipos.setBounds(10, 176, 143, 15);

		Label lblClassesNoMesmoPacote = new Label(this, SWT.NONE);
		lblClassesNoMesmoPacote.setText("Classes no mesmo pacote");
		lblClassesNoMesmoPacote.setBounds(10, 200, 143, 15);

		Label lblModificadoresPublicos = new Label(this, SWT.NONE);
		lblModificadoresPublicos.setText("Modificadores p�blicos");
		lblModificadoresPublicos.setBounds(10, 224, 143, 15);

		Label lblVisitor = new Label(this, SWT.NONE);
		lblVisitor.setText("Visitor");
		lblVisitor.setBounds(10, 248, 143, 15);

		Label lblValoresDeNormalizao = new Label(this, SWT.NONE);
		lblValoresDeNormalizao.setBounds(10, 154, 143, 15);
		lblValoresDeNormalizao.setText("Valores dos Par�metros");

		Label lblSubtypeCalls = new Label(this, SWT.NONE);
		lblSubtypeCalls.setText("Subtype Calls");
		lblSubtypeCalls.setBounds(10, 272, 143, 15);

		Label lblPesos = new Label(this, SWT.NONE);
		lblPesos.setText("Pesos");
		lblPesos.setBounds(273, 155, 47, 15);
		
		txtSubtipos = new Text(this, SWT.BORDER);
		txtSubtipos.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		txtSubtipos.setBounds(159, 173, 76, 21);
		txtSubtipos.setText(String.valueOf(PRProp.getECWFSubtypes()));
		
		txtWeight1 = new Text(this, SWT.BORDER);
		txtWeight1.setBounds(325, 173, 76, 21);
		txtWeight1.setText(String.valueOf(PRProp.getECWFWeight1()));
		txtWeight1.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtClasseMesmoPacote = new Text(this, SWT.BORDER);
		txtClasseMesmoPacote.setBounds(159, 197, 76, 21);
		txtClasseMesmoPacote.setText(String.valueOf(PRProp.getECWFClassesInTheSamePackage()));
		txtClasseMesmoPacote.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtWeight2 = new Text(this, SWT.BORDER);
		txtWeight2.setBounds(325, 197, 76, 21);
		txtWeight2.setText(String.valueOf(PRProp.getECWFWeight2()));
		txtWeight2.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtModificadoresPublicos = new Text(this, SWT.BORDER);
		txtModificadoresPublicos.setBounds(159, 221, 76, 21);
		txtModificadoresPublicos.setText(String.valueOf(PRProp.getECWFPublicModificators()));
		txtModificadoresPublicos.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtWeight3 = new Text(this, SWT.BORDER);
		txtWeight3.setBounds(325, 221, 76, 21);
		txtWeight3.setText(String.valueOf(PRProp.getECWFWeight3()));
		txtWeight3.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtVisitor = new Text(this, SWT.BORDER);
		txtVisitor.setBounds(159, 245, 76, 21);
		txtVisitor.setText(String.valueOf(PRProp.getECWFVisitor()));
		txtVisitor.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});

		txtWeight4 = new Text(this, SWT.BORDER);
		txtWeight4.setBounds(325, 245, 76, 21);
		txtWeight4.setText(String.valueOf(PRProp.getECWFWeight4()));
		txtWeight4.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		
		txtSubtypeCalls = new Text(this, SWT.BORDER);
		txtSubtypeCalls.setBounds(159, 269, 76, 21);
		txtSubtypeCalls.setText(String.valueOf(PRProp.getECWFSubtypesCalls()));
		txtSubtypeCalls.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		
		txtWeight5 = new Text(this, SWT.BORDER);
		txtWeight5.setBounds(325, 269, 76, 21);
		txtWeight5.setText(String.valueOf(PRProp.getECWFWeight5()));
		txtWeight5.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				setText();
			}
		});
		
		Button btnRestaurarValoresPadrao = new Button(this, SWT.NONE);
		btnRestaurarValoresPadrao.setBounds(10, 296, 157, 25);
		btnRestaurarValoresPadrao.setText("Restaurar valores padr�o");
		
		btnRestaurarValoresPadrao.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				PRProp.loadDefaultProperties();
				
				txtSubtipos.setText(String.valueOf(PRProp.getECWFSubtypes()));
				txtWeight1.setText(String.valueOf(PRProp.getECWFWeight1()));
				txtClasseMesmoPacote.setText(String.valueOf(PRProp.getECWFClassesInTheSamePackage()));
				txtWeight2.setText(String.valueOf(PRProp.getECWFWeight2()));
				txtModificadoresPublicos.setText(String.valueOf(PRProp.getECWFPublicModificators()));
				txtWeight3.setText(String.valueOf(PRProp.getECWFWeight3()));
				txtVisitor.setText(String.valueOf(PRProp.getECWFVisitor()));
				txtWeight4.setText(String.valueOf(PRProp.getECWFWeight4()));
				txtSubtypeCalls.setText(String.valueOf(PRProp.getECWFSubtypesCalls()));
				txtWeight5.setText(String.valueOf(PRProp.getECWFWeight5()));
			}
		});
		
		lblFormulaHeuristica = new Label(this, SWT.WRAP);
		lblFormulaHeuristica.setBounds(10, 10, 578, 123);
		this.setText();
		lblFormulaHeuristica.setFont(new Font(lblFormulaHeuristica.getDisplay(), "Courier New", 10, SWT.BOLD));
	}
	
	private void setText() {
		lblFormulaHeuristica.setText("resultado = \n"
				+ "(normalizarReflexao(numeroSubtipos, " + txtSubtipos.getText() + ") * " + txtWeight1.getText() + " + \n"
				+ "normalizarReflexao(numeroSubtiposNoMesmoPacote, " + txtClasseMesmoPacote.getText() + ") * " + txtWeight2.getText() + ") \n"
				+ "- \n"
				+ "(normalizarRetilineo(numeroModificadoresPublicos, " + txtModificadoresPublicos.getText() + ") * " + txtWeight3.getText() + " + \n"
				+ "normalizarRetilineo(visitor, " + txtVisitor.getText() + ") * " + txtWeight4.getText() + " + \n"
				+ "normalizarRetilineo(numeroDeChamadasSubtipos, " + txtSubtypeCalls.getText() + ") * " + txtWeight5.getText() + ")");
	}

	public void saveInformation() {
		PRProp.getProperties().setProperty(PRProp.ECWF_ClassesInTheSamePackage, txtClasseMesmoPacote.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_PublicModificators, txtModificadoresPublicos.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Subtypes, txtSubtipos.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Visitor, txtVisitor.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_SubtypesCalls, txtSubtypeCalls.getText());
		
		PRProp.getProperties().setProperty(PRProp.ECWF_Weight1, txtWeight1.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Weight2, txtWeight2.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Weight3, txtWeight3.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Weight4, txtWeight4.getText());
		PRProp.getProperties().setProperty(PRProp.ECWF_Weight5, txtWeight5.getText());
	}
}
