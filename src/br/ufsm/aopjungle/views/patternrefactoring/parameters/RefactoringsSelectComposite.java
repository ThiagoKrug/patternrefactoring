package br.ufsm.aopjungle.views.patternrefactoring.parameters;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.hibernate.transaction.BTMTransactionManagerLookup;

import br.ufsm.aopjungle.patternrefactoring.util.PRProp;

public class RefactoringsSelectComposite extends Composite {
	
	private Button btnECWF;
	private Button btnRCDWC;
	private Button btnRITWC;

	public RefactoringsSelectComposite(Composite parent, int style) {
		super(parent, style);
		
		Label lblSelecioneAsMecnicas = new Label(this, SWT.NONE);
		lblSelecioneAsMecnicas.setBounds(10, 27, 301, 15);
		lblSelecioneAsMecnicas.setText("Selecione as mec�nicas de busca que ser�o executadas");
		
		btnECWF = new Button(this, SWT.CHECK);
		btnECWF.setBounds(10, 50, 270, 16);
		btnECWF.setText("Encapsular Classes com Factory");
		btnECWF.setSelection(PRProp.getECWFExecute());
		
		btnRCDWC = new Button(this, SWT.CHECK);
		btnRCDWC.setText("Substituir Envio Condicional por Command");
		btnRCDWC.setBounds(10, 72, 270, 16);
		btnRCDWC.setSelection(PRProp.getRCDWCExecute());
		
		btnRITWC = new Button(this, SWT.CHECK);
		btnRITWC.setText("Substituir �rvore Impl�cita por Composite");
		btnRITWC.setSelection(PRProp.getRITWCExecute());
		btnRITWC.setBounds(10, 94, 270, 16);
	}
	
	public void saveInformation() {
		PRProp.getProperties().setProperty(PRProp.ECWF_Execute, String.valueOf(btnECWF.getSelection()));
		PRProp.getProperties().setProperty(PRProp.RCDWC_Execute, String.valueOf(btnRCDWC.getSelection()));
		PRProp.getProperties().setProperty(PRProp.RITWC_Execute, String.valueOf(btnRITWC.getSelection()));
	}
}
