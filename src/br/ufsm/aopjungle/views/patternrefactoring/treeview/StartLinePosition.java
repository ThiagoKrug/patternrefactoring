package br.ufsm.aopjungle.views.patternrefactoring.treeview;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;

public abstract class StartLinePosition extends Type {
	private ASTNode astNode;

	public StartLinePosition(String name, AOJTypeDeclaration aojTypeDeclaration, ASTNode astNode) {
		super(name, aojTypeDeclaration);
		this.astNode = astNode;
	}

	public int getStartPosition() {
		return astNode.getStartPosition();
	}
}
