package br.ufsm.aopjungle.views.patternrefactoring.treeview;

import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;

public class TreeType extends Type {
	public TreeType(String name, AOJTypeDeclaration aojTypeDeclaration) {
		super(name, aojTypeDeclaration);
	}
}