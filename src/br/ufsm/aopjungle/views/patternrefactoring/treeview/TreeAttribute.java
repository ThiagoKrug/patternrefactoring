package br.ufsm.aopjungle.views.patternrefactoring.treeview;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;

public class TreeAttribute extends StartLinePosition {
	public TreeAttribute(String name, AOJTypeDeclaration aojTypeDeclaration, ASTNode astNode) {
		super(name, aojTypeDeclaration, astNode);
	}
}