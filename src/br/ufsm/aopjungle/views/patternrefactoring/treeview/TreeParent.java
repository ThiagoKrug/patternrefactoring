package br.ufsm.aopjungle.views.patternrefactoring.treeview;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class TreeParent extends TreeObject {
	@XStreamOmitField
	private ArrayList<TreeObject> children;

	public TreeParent(String name) {
		super(name);
		children = new ArrayList<TreeObject>();
	}

	public void addChild(TreeObject child) {
		children.add(child);
		child.setParent(this);
	}

	public TreeParent getChild(int index) {
		return (TreeParent) children.get(index);
	}

	public void setChild(int index, TreeObject child) {
		children.set(index, child);
		// child.setParent(this);
	}

	public void removeChild(TreeObject child) {
		children.remove(child);
		child.setParent(null);
	}

	public TreeObject[] getChildren() {
		return (TreeObject[]) children.toArray(new TreeObject[children.size()]);
	}

	public boolean hasChildren() {
		return children.size() > 0;
	}
}	