package br.ufsm.aopjungle.views.patternrefactoring.treeview;

import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;

public abstract class Type extends TreeParent {
	private AOJTypeDeclaration aojTypeDeclaration;

	public Type(String name, AOJTypeDeclaration aojTypeDeclaration) {
		super(name);
		this.aojTypeDeclaration = aojTypeDeclaration;
	}

	public AOJTypeDeclaration getAojTypeDeclaration() {
		return aojTypeDeclaration;
	}

	public void setAojTypeDeclaration(AOJTypeDeclaration aojTypeDeclaration) {
		this.aojTypeDeclaration = aojTypeDeclaration;
	}
}