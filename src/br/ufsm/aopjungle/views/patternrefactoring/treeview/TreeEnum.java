package br.ufsm.aopjungle.views.patternrefactoring.treeview;

import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;

public class TreeEnum extends TreeType {
	public TreeEnum(String name, AOJTypeDeclaration aojTypeDeclaration) {
		super(name, aojTypeDeclaration);
	}
}