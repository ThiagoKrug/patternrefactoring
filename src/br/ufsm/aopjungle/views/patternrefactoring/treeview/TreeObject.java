package br.ufsm.aopjungle.views.patternrefactoring.treeview;

import org.eclipse.core.runtime.IAdaptable;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class TreeObject implements IAdaptable {
	@XStreamOmitField
	private String name;
	@XStreamOmitField
	private TreeParent parent;

	public TreeObject(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setParent(TreeParent parent) {
		this.parent = parent;
	}

	public TreeParent getParent() {
		return parent;
	}

	@Override
	public String toString() {
		return getName();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Object getAdapter(Class key) {
		return null;
	}
}