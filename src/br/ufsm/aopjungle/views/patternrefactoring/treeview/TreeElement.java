package br.ufsm.aopjungle.views.patternrefactoring.treeview;

import br.ufsm.aopjungle.patternrefactoring.RefactoringOpportunity;

public class TreeElement extends TreeParent {
	private RefactoringOpportunity refactoringOpportunity;

	public RefactoringOpportunity getRefactoringOpportunity() {
		return refactoringOpportunity;
	}

	public void setRefactoringOpportunity(RefactoringOpportunity refactoringOpportunity) {
		this.refactoringOpportunity = refactoringOpportunity;
	}

	public TreeElement(String name, RefactoringOpportunity refactoringOpportunity) {
		super(name);
		this.refactoringOpportunity = refactoringOpportunity;
	}

	public TreeElement(String name) {
		super(name);
	}
}