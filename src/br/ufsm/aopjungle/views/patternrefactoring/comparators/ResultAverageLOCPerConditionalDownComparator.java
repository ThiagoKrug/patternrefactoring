package br.ufsm.aopjungle.views.patternrefactoring.comparators;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import br.ufsm.aopjungle.patternrefactoring.RefOppToReplaceConditionalDispatcherWithCommand;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeElement;

public class ResultAverageLOCPerConditionalDownComparator extends ViewerComparator {
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		if (e1 instanceof TreeElement && e2 instanceof TreeElement)
			return Double.compare(((RefOppToReplaceConditionalDispatcherWithCommand) ((TreeElement) e2).getRefactoringOpportunity()).getResultAverageLOCPerConditional(),
					((RefOppToReplaceConditionalDispatcherWithCommand) ((TreeElement) e1).getRefactoringOpportunity()).getResultAverageLOCPerConditional());

		return 0;
	}
}