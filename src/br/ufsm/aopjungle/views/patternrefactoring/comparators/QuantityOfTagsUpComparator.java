package br.ufsm.aopjungle.views.patternrefactoring.comparators;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import br.ufsm.aopjungle.patternrefactoring.RefOppToReplaceImplicitTreeWithComposite;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeElement;

public class QuantityOfTagsUpComparator extends ViewerComparator {
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		if (e1 instanceof TreeElement && e2 instanceof TreeElement)
			return Double.compare(((RefOppToReplaceImplicitTreeWithComposite) ((TreeElement) e1).getRefactoringOpportunity()).getQuantityOfTags(),
					((RefOppToReplaceImplicitTreeWithComposite) ((TreeElement) e2).getRefactoringOpportunity()).getQuantityOfTags());

		return 0;
	}
}