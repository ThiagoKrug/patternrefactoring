package br.ufsm.aopjungle.views.patternrefactoring.comparators;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import br.ufsm.aopjungle.patternrefactoring.RefOppToReplaceImplicitTreeWithComposite;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeElement;

public class ResultQuantityOfTagsDownComparator extends ViewerComparator {
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		if (e1 instanceof TreeElement && e2 instanceof TreeElement)
			return Double.compare(((RefOppToReplaceImplicitTreeWithComposite) ((TreeElement) e2).getRefactoringOpportunity()).getResultQuantityOfTags(),
					((RefOppToReplaceImplicitTreeWithComposite) ((TreeElement) e1).getRefactoringOpportunity()).getResultQuantityOfTags());

		return 0;
	}
}