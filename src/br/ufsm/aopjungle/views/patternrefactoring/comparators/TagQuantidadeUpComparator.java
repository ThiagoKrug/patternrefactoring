package br.ufsm.aopjungle.views.patternrefactoring.comparators;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import br.ufsm.aopjungle.patternrefactoring.Tag;

public class TagQuantidadeUpComparator extends ViewerComparator {
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		if (e1 instanceof Tag && e2 instanceof Tag)
			return Double.compare(((Tag) e1).getQuantidade(), ((Tag) e2).getQuantidade());

		return 0;
	}
}