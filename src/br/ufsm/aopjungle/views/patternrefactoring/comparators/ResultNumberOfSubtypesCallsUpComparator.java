package br.ufsm.aopjungle.views.patternrefactoring.comparators;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import br.ufsm.aopjungle.patternrefactoring.RefOppToEncapsulateClassesWithFactory;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeElement;

public class ResultNumberOfSubtypesCallsUpComparator extends ViewerComparator {
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		if (e1 instanceof TreeElement && e2 instanceof TreeElement)
			return Double.compare(((RefOppToEncapsulateClassesWithFactory) ((TreeElement) e1).getRefactoringOpportunity()).getResultNumberOfSubTypesCalls(),
					((RefOppToEncapsulateClassesWithFactory) ((TreeElement) e2).getRefactoringOpportunity()).getResultNumberOfSubTypesCalls());

		return 0;
	}
}