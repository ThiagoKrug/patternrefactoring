package br.ufsm.aopjungle.views.patternrefactoring.comparators;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeObject;

public class NameUpComparator extends ViewerComparator {
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		return ((TreeObject) e1).getName().compareTo(((TreeObject) e2).getName());
	}
}
