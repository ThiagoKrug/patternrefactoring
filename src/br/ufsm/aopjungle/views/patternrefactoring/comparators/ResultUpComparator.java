package br.ufsm.aopjungle.views.patternrefactoring.comparators;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeElement;

public class ResultUpComparator extends ViewerComparator {
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		if (e1 instanceof TreeElement && e2 instanceof TreeElement) {
			int result = Double.compare(((TreeElement) e1).getRefactoringOpportunity().getValue(), ((TreeElement) e2).getRefactoringOpportunity().getValue());
			if (result == 0)
				Double.compare(((TreeElement) e1).getRefactoringOpportunity().getPosition(), ((TreeElement) e2).getRefactoringOpportunity().getPosition());

			return result;
		}
		return 0;
	}
}