package br.ufsm.aopjungle.views.patternrefactoring;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;

import br.ufsm.aopjungle.patternrefactoring.RefactoringOpportunity;
import br.ufsm.aopjungle.util.IconProvider;
import br.ufsm.aopjungle.views.AOPJungleMonitor;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.NameDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.NameUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.PositionDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.PositionUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.providers.NameLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.PositionLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultLabelProvider;

public abstract class MechanicCompositeView {

	protected TreeViewer viewer;
	protected CTabItem tabItem;
	protected String label;
	public boolean GB_NONE = true;
	public boolean GB_PROJECT = false;
	public boolean GB_PACKAGE = false;
	public boolean GB_TYPE = false;

	public void createView(CTabFolder tabFolder, IViewSite viewSite, Composite parent, String label) {
		tabItem = new CTabItem(tabFolder, SWT.NONE);
		tabItem.setText(label);

		viewer = new TreeViewer(tabFolder, SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		viewer.setContentProvider(new ViewContentProvider(viewSite, label, this));
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);

		this.fillBaseContent();

		viewer.setInput(viewSite);
		viewer.expandAll();

		viewer.addDoubleClickListener(new DoubleClickListener());

		GridLayoutFactory.fillDefaults().generateLayout(parent);

		tabItem.setControl(viewer.getTree());
		viewer.getTree().setVisible(false);
	}

	protected abstract void fillContent();

	protected void fillBaseContent() {
		treeViewerColumnBuilder(viewer, "Refactoring Opportunities", 550, new NameLabelProvider(), new NameUpComparator(), new NameDownComparator());
		treeViewerColumnBuilder(viewer, "#", 30, new PositionLabelProvider(), new PositionUpComparator(), new PositionDownComparator());
		TreeViewerColumn resultColumn = treeViewerColumnBuilder(viewer, "Result", 100, new ResultLabelProvider(), new ResultUpComparator(), new ResultDownComparator());

		fillContent();

		viewer.setComparator(new ResultDownComparator());
		viewer.getTree().setSortColumn(resultColumn.getColumn());
		viewer.getTree().setSortDirection(SWT.DOWN);
	}

	protected TableViewerColumn tableViewerColumnBuilder(TableViewer viewer, String columnName, int width, IStyledLabelProvider provider, ViewerComparator upComparator,
			ViewerComparator downComparator) {
		TableViewerColumn column = new TableViewerColumn(viewer, SWT.NONE);
		column.getColumn().setWidth(width);
		column.getColumn().setText(columnName);
		column.getColumn().setToolTipText(columnName);
		column.setLabelProvider(new DelegatingStyledCellLabelProvider(provider));
		if (upComparator != null && downComparator != null)
			column.getColumn().addSelectionListener(new SelectionListenerHelper(viewer, column, upComparator, downComparator));
		return column;
	}

	protected TreeViewerColumn treeViewerColumnBuilder(TreeViewer viewer, String columnName, int width, IStyledLabelProvider provider, ViewerComparator upComparator, ViewerComparator downComparator) {
		TreeViewerColumn column = new TreeViewerColumn(viewer, SWT.NONE);
		column.getColumn().setWidth(width);
		column.getColumn().setText(columnName);
		column.getColumn().setToolTipText(columnName);
		column.setLabelProvider(new DelegatingStyledCellLabelProvider(provider));
		column.getColumn().addSelectionListener(new SelectionListenerHelper(viewer, column, upComparator, downComparator));
		return column;
	}

	protected class SelectionListenerHelper implements SelectionListener {
		TreeViewer treeViewer;
		TreeViewerColumn treeColumn;
		TableViewer tableViewer;
		TableViewerColumn tableColumn;
		ViewerComparator upComparator;
		ViewerComparator downComparator;

		public SelectionListenerHelper(TreeViewer treeViewer, TreeViewerColumn column, ViewerComparator upComparator, ViewerComparator downComparator) {
			this.treeViewer = treeViewer;
			this.treeColumn = column;
			this.upComparator = upComparator;
			this.downComparator = downComparator;
		}

		public SelectionListenerHelper(TableViewer tableViewer, TableViewerColumn column, ViewerComparator upComparator, ViewerComparator downComparator) {
			this.tableViewer = tableViewer;
			this.tableColumn = column;
			this.upComparator = upComparator;
			this.downComparator = downComparator;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			IProgressService progressService = PlatformUI.getWorkbench().getProgressService();
			try {
				progressService.runInUI(PlatformUI.getWorkbench().getProgressService(), new IRunnableWithProgress() {

					@Override
					public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
						AOPJungleMonitor.mainMonitor = SubMonitor.convert(monitor, 100);
						AOPJungleMonitor.mainMonitor.setTaskName("Refreshing...");
						if (treeViewer != null) {
							Tree tree = treeViewer.getTree();
							tree.setSortColumn(treeColumn.getColumn());
							if (tree.getSortDirection() == SWT.DOWN) {
								tree.setSortDirection(SWT.UP);
								treeViewer.setComparator(upComparator);
							} else {
								tree.setSortDirection(SWT.DOWN);
								treeViewer.setComparator(downComparator);
							}
						} else {
							Table table = tableViewer.getTable();
							table.setSortColumn(tableColumn.getColumn());
							if (table.getSortDirection() == SWT.DOWN) {
								table.setSortDirection(SWT.UP);
								tableViewer.setComparator(upComparator);
							} else {
								table.setSortDirection(SWT.DOWN);
								tableViewer.setComparator(downComparator);
							}
						}
					}
				}, null);
			} catch (InvocationTargetException | InterruptedException ex) {
				ex.printStackTrace();
			}
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
		}
	}

	public TreeViewer getViewer() {
		return viewer;
	}

	public void setViewer(TreeViewer viewer) {
		this.viewer = viewer;
	}

	public String checkResults(Map<String, Map<String, List<RefactoringOpportunity>>> generalReport) {
		new File(PatternView.generalReportCurrentPath + label + "/").mkdirs();
		String message = ReportManager.checkResults(PatternView.generalReportOriginalsPath, PatternView.generalReportCurrentPath + label + "/", generalReport);

		if (message.isEmpty()) {
			tabItem.setImage(IconProvider.getIcon("passed.png"));
		} else {
			tabItem.setImage(IconProvider.getIcon("message_error.png"));
		}
		return message;
	}

	public CTabItem getTabItem() {
		return tabItem;
	}

	public void setTabItem(CTabItem tabItem) {
		this.tabItem = tabItem;
	}

}
