package br.ufsm.aopjungle.views.patternrefactoring;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.part.ViewPart;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.ufsm.aopjungle.patternrefactoring.EncapsulateClassWithFactory;
import br.ufsm.aopjungle.patternrefactoring.RefactoringOpportunity;
import br.ufsm.aopjungle.patternrefactoring.ReplaceConditionalDispatcherWithCommand;
import br.ufsm.aopjungle.patternrefactoring.ReplaceImplicitTreeWithComposite;
import br.ufsm.aopjungle.patternrefactoring.util.PRProp;
import br.ufsm.aopjungle.util.IconProvider;
import br.ufsm.aopjungle.views.SaveFileFacade;

public class PatternView extends ViewPart {
	@XStreamOmitField
	public static final String ID = "br.ufsm.aopjungle.views.patternrefactoring.PatternView";
	@XStreamOmitField
	private CTabFolder tabFolder;
	private EncapsulateClassesWithFactoryView ecwfView;
	private ReplaceConditionalDispatcherWithCommandView rcdwcView;
	private ReplaceImplicitTreeWithCompositeView ritwcView;
	private Action actionGroupByProject;
	private Action actionGroupByPackage;
	private Action actionGroupByType;
	private Action actionGroupByNone;
	private Action actionExportResults;
	private Action actionResultVerification;
	private Action actionExpandAll;
	private Action actionCollapseAll;
	private Map<String, Map<String, List<RefactoringOpportunity>>> generalReport;
	public static String generalReportPath = ResourcesPlugin.getWorkspace().getRoot().getLocation().toString() + "/results/";
	public static String generalReportOriginalsPath = generalReportPath + "/originals/";
	public static String generalReportCurrentPath = generalReportPath + "/current/";
	private String reportMessage = "";
	public static String ENGINE_SELECTED;
	public static final String ECWF_ENGINE_LABEL = new EncapsulateClassWithFactory().getLabel();
	public static final String RCDWC_ENGINE_LABEL = new ReplaceConditionalDispatcherWithCommand().getLabel();
	public static final String RITWC_ENGINE_LABEL = new ReplaceImplicitTreeWithComposite().getLabel();

	@Override
	public void createPartControl(Composite parent) {
		ENGINE_SELECTED = ECWF_ENGINE_LABEL;
		ecwfView = new EncapsulateClassesWithFactoryView();
		rcdwcView = new ReplaceConditionalDispatcherWithCommandView();
		ritwcView = new ReplaceImplicitTreeWithCompositeView();

		tabFolder = new CTabFolder(parent, SWT.BORDER);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		if (PRProp.getECWFExecute())
			ecwfView.createView(tabFolder, getViewSite(), parent);
		if (PRProp.getRCDWCExecute())
			rcdwcView.createView(tabFolder, getViewSite(), parent);
		if (PRProp.getRITWCExecute())
			ritwcView.createView(tabFolder, getViewSite(), parent);

		this.fillLocalToolBar();

		generalReport = ReportManager.generateReport();
		this.checkResults();

		tabFolder.setSelection(0);

		tabFolder.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				MechanicCompositeView mcv = getMechanicCompositeViewFocused();
				actionGroupByProject.setChecked(mcv.GB_PROJECT);
				actionGroupByPackage.setChecked(mcv.GB_PACKAGE);
				actionGroupByType.setChecked(mcv.GB_TYPE);
				actionGroupByNone.setChecked(mcv.GB_NONE);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	private void checkResults() {
		// renew de current results
		File current = new File(generalReportCurrentPath);
		current.delete();
		current.mkdirs();
		ReportManager.saveReport(current.getPath() + "/resultados.xml", generalReport);

		// call every check
		if (PRProp.getECWFExecute())
			reportMessage += ecwfView.checkResults(generalReport);
		if (PRProp.getRCDWCExecute())
			reportMessage += rcdwcView.checkResults(generalReport);

		if (reportMessage.isEmpty()) {
			reportMessage += "Tudo OK!\n";
			actionResultVerification.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("passed.png")));
		} else {
			actionResultVerification.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("message_error.png")));
		}
	}

	private TreeViewer getViewerFocus() {
		if (PRProp.getECWFExecute() && (ecwfView.getViewer().getControl().isFocusControl() || tabFolder.getSelection().equals(ecwfView.getTabItem()))) {
			ENGINE_SELECTED = ECWF_ENGINE_LABEL;
			return ecwfView.getViewer();
		} else if (PRProp.getRCDWCExecute() && (rcdwcView.getViewer().getControl().isFocusControl() || tabFolder.getSelection().equals(rcdwcView.getTabItem()))) {
			ENGINE_SELECTED = RCDWC_ENGINE_LABEL;
			return rcdwcView.getViewer();
		} else if (PRProp.getRITWCExecute() && (ritwcView.getViewer().getControl().isFocusControl() || tabFolder.getSelection().equals(ritwcView.getTabItem()))) {
			ENGINE_SELECTED = RITWC_ENGINE_LABEL;
			return ritwcView.getViewer();
		}
		return null;
	}

	private MechanicCompositeView getMechanicCompositeViewFocused() {
		if (PRProp.getECWFExecute() && (ecwfView.getViewer().getControl().isFocusControl() || tabFolder.getSelection().equals(ecwfView.getTabItem()))) {
			return ecwfView;
		} else if (PRProp.getRCDWCExecute() && (rcdwcView.getViewer().getControl().isFocusControl() || tabFolder.getSelection().equals(rcdwcView.getTabItem()))) {
			return rcdwcView;
		} else if (PRProp.getRITWCExecute() && (ritwcView.getViewer().getControl().isFocusControl() || tabFolder.getSelection().equals(ritwcView.getTabItem()))) {
			return ritwcView;
		}
		return null;
	}

	private void fillLocalToolBar() {
		IActionBars bars = getViewSite().getActionBars();
		IToolBarManager manager = bars.getToolBarManager();

		actionExpandAll = new Action() {
			@Override
			public void run() {
				getViewerFocus().expandAll();
			}
		};
		actionExpandAll.setText("Expandir tudo");
		actionExpandAll.setToolTipText("Expandir tudo");
		actionExpandAll.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("expandall.png")));
		manager.add(actionExpandAll);

		actionCollapseAll = new Action() {
			@Override
			public void run() {
				getViewerFocus().collapseAll();
			}
		};
		actionCollapseAll.setText("Colapsar tudo");
		actionCollapseAll.setToolTipText("Colapsar tudo");
		actionCollapseAll.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("collapseall.png")));
		manager.add(actionCollapseAll);

		manager.add(new Separator());

		actionGroupByProject = new Action("Agrupar por projeto", Action.AS_CHECK_BOX) {
			@Override
			public void run() {
				MechanicCompositeView mcv = getMechanicCompositeViewFocused();
				mcv.GB_PROJECT = mcv.GB_PROJECT ? false : true;
				getViewerFocus().setContentProvider(new ViewContentProvider(getViewSite(), ENGINE_SELECTED, mcv));
				getViewerFocus().refresh();
				getViewerFocus().expandAll();
				setChecked(mcv.GB_PROJECT);
			}
		};
		actionGroupByProject.setToolTipText("Agrupar por projeto");
		actionGroupByProject.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("prj_mode.png")));
		manager.add(actionGroupByProject);

		actionGroupByPackage = new Action("Agrupar por pacote", Action.AS_CHECK_BOX) {
			@Override
			public void run() {
				MechanicCompositeView mcv = getMechanicCompositeViewFocused();
				mcv.GB_PACKAGE = mcv.GB_PACKAGE ? false : true;
				getViewerFocus().setContentProvider(new ViewContentProvider(getViewSite(), ENGINE_SELECTED, mcv));
				getViewerFocus().refresh();
				getViewerFocus().expandAll();
				setChecked(mcv.GB_PACKAGE);
			}
		};
		actionGroupByPackage.setToolTipText("Agrupar por pacote");
		actionGroupByPackage.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("package_mode.png")));
		manager.add(actionGroupByPackage);

		actionGroupByType = new Action("Agrupar por tipo", Action.AS_CHECK_BOX) {
			@Override
			public void run() {
				MechanicCompositeView mcv = getMechanicCompositeViewFocused();
				mcv.GB_TYPE = mcv.GB_TYPE ? false : true;
				getViewerFocus().setContentProvider(new ViewContentProvider(getViewSite(), ENGINE_SELECTED, mcv));
				getViewerFocus().refresh();
				getViewerFocus().expandAll();
				setChecked(mcv.GB_TYPE);
			}
		};
		actionGroupByType.setToolTipText("Agrupar por tipo");
		actionGroupByType.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("file_mode.png")));
		manager.add(actionGroupByType);

		actionGroupByNone = new Action("N�o agrupar", Action.AS_CHECK_BOX) {
			@Override
			public void run() {
				MechanicCompositeView mcv = getMechanicCompositeViewFocused();
				mcv.GB_NONE = true;
				getViewerFocus().setContentProvider(new ViewContentProvider(getViewSite(), ENGINE_SELECTED, mcv));
				getViewerFocus().refresh();
				getViewerFocus().expandAll();
				setChecked(mcv.GB_NONE);
			}
		};
		actionGroupByNone.setToolTipText("N�o agrupar");
		actionGroupByNone.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("type_mode.png")));
		manager.add(actionGroupByNone);

		manager.add(new Separator());

		actionExportResults = new Action() {
			@Override
			public void run() {
				SaveFileFacade dialog = new SaveFileFacade(new Shell());
				dialog.setFilterNames(new String[] { "Arquivo XML", "Arquivo de texto", "All Files (*.*)" });
				dialog.setFilterExtensions(new String[] { "*.xml", "*.txt", "*.*" }); // Windows wild cards
				dialog.setFilterPath("c:\\"); // Windows path
				dialog.setFileName("resultados.xml");
				String filePath = dialog.open();

				if (!filePath.isEmpty()) {
					ReportManager.saveReport(filePath, generalReport);
				}
				// para a verifica��o autom�tica dos resultados
				File f = new File(generalReportOriginalsPath);
				f.delete();
				f.mkdir();
				ReportManager.saveReport(generalReportOriginalsPath + "resultados.xml", generalReport);
			}

		};
		actionExportResults.setText("Exportar resultados");
		actionExportResults.setToolTipText("Exportar resultados");
		actionExportResults.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("export_wiz.png")));
		manager.add(actionExportResults);

		actionResultVerification = new Action() {
			@Override
			public void run() {
				ResultCheckDialog dialog = new ResultCheckDialog(new Shell(), reportMessage);
				dialog.create();
				dialog.open();
			}
		};
		actionResultVerification.setText("Verifica��o dos resultados");
		actionResultVerification.setToolTipText("Verifica��o dos resultados");
		actionResultVerification.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("methpro_obj.png")));
		manager.add(actionResultVerification);
	}

	@Override
	public void setFocus() {
		this.tabFolder.setFocus();
	}

}
