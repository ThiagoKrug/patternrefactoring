package br.ufsm.aopjungle.views.patternrefactoring;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IViewSite;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.ufsm.aopjungle.astor.AstorSession;
import br.ufsm.aopjungle.astor.CodeSmellEngine;
import br.ufsm.aopjungle.astor.CodeSmellReport;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeCodeSmell;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeElement;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeObject;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreePackage;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeParent;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeProject;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeType;

public class ViewContentProvider implements IStructuredContentProvider, ITreeContentProvider {
	@XStreamOmitField
	private AstorSession session = AstorSession.getInstance();
	@XStreamOmitField
	private TreeParent invisibleRoot;
	private IViewSite viewSite;
	private CodeSmellEngine engine;
	private MechanicCompositeView mcv;

	public ViewContentProvider(IViewSite viewSite, String engineLabel, MechanicCompositeView mcv) {
		this.viewSite = viewSite;
		this.engine = session.getRegisteredEngine(engineLabel);
		this.mcv = mcv;
	}

	@Override
	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public Object[] getElements(Object parent) {
		if (parent.equals(viewSite)) {
			if (invisibleRoot == null)
				initialize();
			return getChildren(invisibleRoot);
		}
		return getChildren(parent);
	}

	@Override
	public Object getParent(Object child) {
		if (child instanceof TreeObject) {
			return ((TreeObject) child).getParent();
		}
		return null;
	}

	@Override
	public Object[] getChildren(Object parent) {
		if (parent instanceof TreeParent) {
			return ((TreeParent) parent).getChildren();
		}
		return new Object[0];
	}

	@Override
	public boolean hasChildren(Object parent) {
		if (parent instanceof TreeParent)
			return ((TreeParent) parent).hasChildren();
		return false;
	}

	private void initialize() {
		invisibleRoot = new TreeParent("");
		TreeParent codeSmellNode = new TreeCodeSmell(engine.getLabel());
		if (!engine.getReport().isEmpty())
			codeSmellNode = new TreeCodeSmell(engine.getLabel() + " -> " + engine.getReport().size());

		invisibleRoot.addChild(codeSmellNode);
		TreeParent current = codeSmellNode;

		for (CodeSmellReport report : engine.getReport()) {
			current = codeSmellNode;
			if (mcv.GB_PROJECT) {
				TreeParent projectNode = addProjectNode(report, current);
				current = projectNode;
			}
			if (mcv.GB_PACKAGE) {
				TreeParent packageNode = addPackageNode(report, current);
				current = packageNode;
			}
			if (mcv.GB_TYPE) {
				TreeParent typeNode = addTypeNode(report, current);
				current = typeNode;
			}
			addRefactoringOpportunitiesNodes(report, current);
		}
	}

	private void addRefactoringOpportunitiesNodes(CodeSmellReport report, TreeParent typeNode) {
		TreeParent elementNode = new TreeElement(report.getRefactoringOpportunity().getMessage(), report.getRefactoringOpportunity());
		typeNode.addChild(elementNode);
	}

	private TreeParent addTypeNode(CodeSmellReport report, TreeParent packageNode) {
		TreeParent typeNode = getNode(packageNode, report.getContainerName());
		if (typeNode == null) {
			typeNode = new TreeType(report.getContainerName(), report.getRefactoringOpportunity().getClazz());
			packageNode.addChild(typeNode);
		}
		return typeNode;
	}

	private TreeParent addPackageNode(CodeSmellReport report, TreeParent projectNode) {
		TreeParent packageNode = getNode(projectNode, report.getPackageName());
		if (packageNode == null) {
			packageNode = new TreePackage(report.getPackageName());
			projectNode.addChild(packageNode);
		}
		return packageNode;
	}

	private TreeParent addProjectNode(CodeSmellReport report, TreeParent codeSmellNode) {
		TreeParent projectNode = getNode(codeSmellNode, report.getProjectName());
		if (projectNode == null) {
			projectNode = new TreeProject(report.getProjectName());
			codeSmellNode.addChild(projectNode);
		}
		return projectNode;
	}

	private TreeParent getNode(TreeParent parent, String name) {
		TreeObject[] objects = parent.getChildren();
		int i = 0;
		while ((i < objects.length) && (!objects[i].getName().equals(name))) {
			i++;
		}
		return i < objects.length ? (TreeParent) objects[i] : null;
	}
}