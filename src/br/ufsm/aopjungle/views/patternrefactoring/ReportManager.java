package br.ufsm.aopjungle.views.patternrefactoring;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import br.ufsm.aopjungle.astor.AstorSession;
import br.ufsm.aopjungle.astor.CodeSmellEngine;
import br.ufsm.aopjungle.astor.CodeSmellReport;
import br.ufsm.aopjungle.builder.AOJExporterXML;
import br.ufsm.aopjungle.patternrefactoring.RefactoringOpportunity;

public class ReportManager {

	public static Map<String, List<RefactoringOpportunity>> generateProjectsResults(CodeSmellEngine engine) {
		Map<String, List<RefactoringOpportunity>> projectsResults = new HashMap<>();
		List<String> projects = getProjects(engine.getReport());
		for (String project : projects) {
			List<RefactoringOpportunity> opportunities = new ArrayList<>();
			for (CodeSmellReport report : engine.getReport()) {
				if (report.getProjectName().compareTo(project) == 0) {
					opportunities.add(report.getRefactoringOpportunity());
				}
			}
			projectsResults.put(project, opportunities);
		}
		return projectsResults;
	}

	public static List<String> getProjects(List<CodeSmellReport> reports) {
		List<String> projects = new ArrayList<>();
		for (CodeSmellReport report : reports) {
			if (!projects.contains(report.getProjectName())) {
				projects.add(report.getProjectName());
			}
		}
		return projects;
	}

	public static Map<String, Map<String, List<RefactoringOpportunity>>> generateReport() {
		AstorSession badSmellEngine = AstorSession.getInstance();
		Map<String, Map<String, List<RefactoringOpportunity>>> engineReports = new HashMap<>();
		for (CodeSmellEngine engine : badSmellEngine.getRegisteredEngines()) {
			engineReports.put(engine.getLabel(), generateProjectsResults(engine));
		}
		return engineReports;
	}

	public static void saveReport(String filePath, Map<String, Map<String, List<RefactoringOpportunity>>> generalReport) {
		// System.out.println(filePath);
		try {
			for (Map.Entry<String, Map<String, List<RefactoringOpportunity>>> engineReport : generalReport.entrySet()) {
				for (Map.Entry<String, List<RefactoringOpportunity>> projectReport : engineReport.getValue().entrySet()) {
					File file = makeFile(filePath, engineReport.getKey(), projectReport.getKey());
					AOJExporterXML.generate(file, projectReport);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static File makeFile(String filePath, String engineName, String projectName) {
		String fileName = filePath.substring(0, filePath.lastIndexOf("."));
		String fileExtension = filePath.substring(filePath.lastIndexOf("."), filePath.length());
		String finalName = fileName + "-" + engineName + "-" + projectName + fileExtension;
		File file = new File(finalName);
		return file;
	}

	public static String checkResults(String generalReportOriginalsPath, String generalReportCurrentPath, Map<String, Map<String, List<RefactoringOpportunity>>> generalReport) {
		String reportMessage = "";
		try {
			File originals = new File(generalReportOriginalsPath);
			File current = new File(generalReportCurrentPath);
			for (File original : originals.listFiles()) {
				for (File corrente : current.listFiles()) {
					if (original.getName().compareTo(corrente.getName()) == 0) {
						if (!FileUtils.contentEquals(original, corrente)) {
							reportMessage += original.getName() + " est� diferente!\n";
						}
						break;
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return reportMessage;
	}
}
