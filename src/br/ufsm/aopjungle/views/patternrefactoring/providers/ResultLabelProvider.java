package br.ufsm.aopjungle.views.patternrefactoring.providers;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.StyledString;

import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeElement;

public class ResultLabelProvider extends ColumnLabelProvider implements IStyledLabelProvider {

	@Override
	public StyledString getStyledText(Object element) {
		if (element instanceof TreeElement)
			return new StyledString(((TreeElement) element).getRefactoringOpportunity().getValue() + "");
		else
			return new StyledString("");
	}

}