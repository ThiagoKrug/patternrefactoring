package br.ufsm.aopjungle.views.patternrefactoring.providers;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;

import br.ufsm.aopjungle.patternrefactoring.Tag;

public class TagQuantidadeLabelProvider extends ColumnLabelProvider implements IStyledLabelProvider {

	@Override
	public StyledString getStyledText(Object element) {
		return new StyledString(((Tag) element).getQuantidade() + "");
	}

}