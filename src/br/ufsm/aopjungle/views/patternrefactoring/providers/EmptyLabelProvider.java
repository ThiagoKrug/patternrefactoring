package br.ufsm.aopjungle.views.patternrefactoring.providers;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.StyledString;

public class EmptyLabelProvider extends ColumnLabelProvider implements IStyledLabelProvider {

	@Override
	public StyledString getStyledText(Object element) {
		return new StyledString("");
	}

}