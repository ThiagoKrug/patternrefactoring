package br.ufsm.aopjungle.views.patternrefactoring.providers;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.StyledString;

import br.ufsm.aopjungle.patternrefactoring.RefOppToReplaceImplicitTreeWithComposite;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeElement;

public class SumOfRelevanceOfTagsLabelProvider extends ColumnLabelProvider implements IStyledLabelProvider {

	@Override
	public StyledString getStyledText(Object element) {
		if (element instanceof TreeElement)
			return new StyledString(((RefOppToReplaceImplicitTreeWithComposite) ((TreeElement) element).getRefactoringOpportunity()).getSumOfRelevanceTags() + "");
		else
			return new StyledString("");
	}

}