package br.ufsm.aopjungle.views.patternrefactoring.providers;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ICheckStateProvider;

import br.ufsm.aopjungle.patternrefactoring.Tag;

public class SelectedLabelProvider extends ColumnLabelProvider implements ICheckStateProvider {

	@Override
	public boolean isChecked(Object element) {
		return ((Tag) element).isSelected();
	}

	@Override
	public boolean isGrayed(Object element) {
		// TODO Auto-generated method stub
		return false;
	}

}