package br.ufsm.aopjungle.views.patternrefactoring;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.patternrefactoring.RefOppToReplaceConditionalDispatcherWithCommand;
import br.ufsm.aopjungle.patternrefactoring.RefOppToReplaceImplicitTreeWithComposite;
import br.ufsm.aopjungle.patternrefactoring.RefactoringOpportunity;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.StartLinePosition;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeElement;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeParent;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.Type;

public class DoubleClickListener implements IDoubleClickListener {

	@Override
	public void doubleClick(DoubleClickEvent event) {
		IStructuredSelection thisSelection = (IStructuredSelection) event.getSelection();
		TreeParent selectedNode = (TreeParent) thisSelection.getFirstElement();
		AOJTypeDeclaration clazz = null;
		Map<String, Integer> lineLocation = null;
		if (selectedNode instanceof StartLinePosition) {
			clazz = ((StartLinePosition) selectedNode).getAojTypeDeclaration();
			int startLine = ((StartLinePosition) selectedNode).getStartPosition();
			lineLocation = new HashMap<>();
			lineLocation.put(IMarker.CHAR_START, new Integer(startLine));
		} else if (selectedNode instanceof TreeElement) {
			RefactoringOpportunity ro = ((TreeElement) selectedNode).getRefactoringOpportunity();
			clazz = ro.getClazz();
			int startLine = -1;
			if (ro instanceof RefOppToReplaceConditionalDispatcherWithCommand) {
				startLine = ((RefOppToReplaceConditionalDispatcherWithCommand) ro).getIfStatement().getIfStatement().getStartPosition();
			} else if (ro instanceof RefOppToReplaceImplicitTreeWithComposite) {
				startLine = ((RefOppToReplaceImplicitTreeWithComposite) ro).getMethod().getNode().getStartPosition();
			}
			lineLocation = new HashMap<>();
			// map.put(IMarker.LINE_NUMBER, new Integer(5));
			lineLocation.put(IMarker.CHAR_START, new Integer(startLine));
		} else if (selectedNode instanceof Type) {
			clazz = ((Type) selectedNode).getAojTypeDeclaration();
		}
		
		if (lineLocation != null)
			openFileOnLine(getFile(clazz), lineLocation);
		else if (clazz != null)
			openFile(getFile(clazz));
		else
			System.out.println("Double-click detected on " + selectedNode.toString());

	}

	private IFile getFile(AOJTypeDeclaration clazz) {
		IProject project = clazz.getProject().getResourceProject();
		IFile fileToOpen = project.getFile(clazz.getCompilationUnit().getCompilationUnit().getResource().getProjectRelativePath());
		return fileToOpen;
	}

	private void openFileOnLine(IFile fileToOpen, Map<String, Integer> map) {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

		try {
			IMarker marker = fileToOpen.createMarker(IMarker.TEXT);
			marker.setAttributes(map);
			IDE.openEditor(page, marker);
		} catch (PartInitException e) {
			e.printStackTrace();
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	private void openFile(IFile fileToOpen) {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

		try {
			IDE.openEditor(page, fileToOpen);
		} catch (PartInitException e) {
			e.printStackTrace();
		}
	}

}