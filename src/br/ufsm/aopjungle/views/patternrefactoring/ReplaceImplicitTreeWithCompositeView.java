package br.ufsm.aopjungle.views.patternrefactoring;

import java.util.List;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IViewSite;

import br.ufsm.aopjungle.astor.AstorSession;
import br.ufsm.aopjungle.patternrefactoring.ReplaceImplicitTreeWithComposite;
import br.ufsm.aopjungle.patternrefactoring.Tag;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.QuantityOfClosedTagsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.QuantityOfClosedTagsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.QuantityOfTagsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.QuantityOfTagsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultQuantityOfClosedTagsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultQuantityOfClosedTagsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultQuantityOfTagsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultQuantityOfTagsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultRelevanceOfTagsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.ResultRelevanceOfTagsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.SumOfRelevanceOfTagsDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.SumOfRelevanceOfTagsUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.TagDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.TagQuantidadeDownComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.TagQuantidadeUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.comparators.TagUpComparator;
import br.ufsm.aopjungle.views.patternrefactoring.providers.EmptyLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.QuantityOfClosedTagsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.QuantityOfTagsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultQuantityOfClosedTagsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultQuantityOfTagsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.ResultRelevanceOfTagsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.SelectedLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.SumOfRelevanceOfTagsLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.TagLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.providers.TagQuantidadeLabelProvider;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeElement;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreePackage;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeParent;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeProject;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeType;

public class ReplaceImplicitTreeWithCompositeView extends MechanicCompositeView {

	private CheckboxTableViewer tags;
	private String lastProject = "";

	public void createView(CTabFolder tabFolder, IViewSite viewSite, Composite parent) {
		GB_PROJECT = true;
		label = new ReplaceImplicitTreeWithComposite().getLabel();
		tabItem = new CTabItem(tabFolder, SWT.NONE);
		tabItem.setText(label);

		SashForm sashForm = new SashForm(tabFolder, SWT.HORIZONTAL);
		sashForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		viewer = new TreeViewer(sashForm, SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		tags = CheckboxTableViewer.newCheckList(sashForm, SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		tags.setContentProvider(new ArrayContentProvider());
		tableViewerColumnBuilder(tags, "", 30, new EmptyLabelProvider(), null, null);
		TableViewerColumn quantidade = tableViewerColumnBuilder(tags, "Quantity", 40, new TagQuantidadeLabelProvider(), new TagQuantidadeUpComparator(), new TagQuantidadeDownComparator());
		tableViewerColumnBuilder(tags, "Project Tag", 100, new TagLabelProvider(), new TagUpComparator(), new TagDownComparator());
		tags.setCheckStateProvider(new SelectedLabelProvider());
		tags.setComparator(new TagQuantidadeDownComparator());
		tags.getTable().setSortColumn(quantidade.getColumn());
		tags.getTable().setSortDirection(SWT.DOWN);
		tags.getTable().setHeaderVisible(true);
		tags.getTable().setLinesVisible(true);
		tags.addCheckStateListener(new ICheckStateListener() {
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				Tag tag = (Tag) event.getElement();
				tag.setSelected(event.getChecked());
				getRITWCEngine().reRun(lastProject);
				viewer.refresh();
			}
		});

		viewer.setContentProvider(new ViewContentProvider(viewSite, label, this));
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);
		fillBaseContent();
		viewer.setInput(viewSite);
		viewer.expandAll();
		viewer.addDoubleClickListener(new DoubleClickListener());
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection thisSelection = (IStructuredSelection) event.getSelection();
				TreeParent selectedNode = (TreeParent) thisSelection.getFirstElement();
				String projectName = "";
				if (selectedNode instanceof TreeElement) {
					projectName = ((TreeElement) selectedNode).getRefactoringOpportunity().getClazz().getProject().getName();
				} else if (selectedNode instanceof TreeProject) {
					projectName = ((TreeProject) selectedNode).getName();
				} else if (selectedNode instanceof TreePackage) {
					TreeParent tp = null;
					TreeParent tp1 = ((TreePackage) selectedNode).getChild(0);
					tp = (tp1.hasChildren()) ? tp1.getChild(0) : tp1;
					projectName = ((TreeElement) tp).getRefactoringOpportunity().getClazz().getProject().getName();
				} else if (selectedNode instanceof TreeType) {
					projectName = ((TreeElement) ((TreeType) selectedNode).getChild(0)).getRefactoringOpportunity().getClazz().getProject().getName();
				}
				if (!(projectName.compareTo("") == 0) && !(projectName.compareTo(lastProject) == 0)) {
					tags.setInput(getProjectTags(projectName));
					lastProject = projectName;
				}
			}
		});

		sashForm.setWeights(new int[] { 5, 1 });

		GridLayoutFactory.fillDefaults().generateLayout(parent);

		tabItem.setControl(sashForm);
	}

	private List<Tag> getProjectTags(String projectName) {
		ReplaceImplicitTreeWithComposite ritwc = getRITWCEngine();
		return ritwc.getProjectsTags().get(projectName);
	}
	
	private ReplaceImplicitTreeWithComposite getRITWCEngine() {
		return (ReplaceImplicitTreeWithComposite) AstorSession.getInstance().getRegisteredEngine(new ReplaceImplicitTreeWithComposite().getLabel());
	}

	protected void fillContent() {
		treeViewerColumnBuilder(viewer, "Result quantity of tags", 100, new ResultQuantityOfTagsLabelProvider(), new ResultQuantityOfTagsUpComparator(), new ResultQuantityOfTagsDownComparator());
		treeViewerColumnBuilder(viewer, "Result quantity of closed tags", 100, new ResultQuantityOfClosedTagsLabelProvider(), new ResultQuantityOfClosedTagsUpComparator(), new ResultQuantityOfClosedTagsDownComparator());
		treeViewerColumnBuilder(viewer, "Result of the relevance of tags", 100, new ResultRelevanceOfTagsLabelProvider(), new ResultRelevanceOfTagsUpComparator(), new ResultRelevanceOfTagsDownComparator());
		treeViewerColumnBuilder(viewer, "Quantity of tags", 100, new QuantityOfTagsLabelProvider(), new QuantityOfTagsUpComparator(), new QuantityOfTagsDownComparator());
		treeViewerColumnBuilder(viewer, "Quantity of closed tags", 100, new QuantityOfClosedTagsLabelProvider(), new QuantityOfClosedTagsUpComparator(), new QuantityOfClosedTagsDownComparator());
		treeViewerColumnBuilder(viewer, "Sum of the relevance of tags", 100, new SumOfRelevanceOfTagsLabelProvider(), new SumOfRelevanceOfTagsUpComparator(), new SumOfRelevanceOfTagsDownComparator());
	}
}
