package br.ufsm.aopjungle.views;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.org.eclipse.jdt.core.dom.TypePattern;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IViewSite;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.ufsm.aopjungle.binding.AOJBinding;
import br.ufsm.aopjungle.builder.AOJungleBuilder;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareCompilationEnforcement;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareField;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareMethod;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareParents;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclarePrecedence;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareSoft;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutExpression;
import br.ufsm.aopjungle.metamodel.commons.AOJAnnotable;
import br.ufsm.aopjungle.metamodel.commons.AOJAnnotationDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJAttributeDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJBehaviourKind;
import br.ufsm.aopjungle.metamodel.commons.AOJBindble;
import br.ufsm.aopjungle.metamodel.commons.AOJClassHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJConcreteTypeMember;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJException;
import br.ufsm.aopjungle.metamodel.commons.AOJInterfaceble;
import br.ufsm.aopjungle.metamodel.commons.AOJMember;
import br.ufsm.aopjungle.metamodel.commons.AOJModifier;
import br.ufsm.aopjungle.metamodel.commons.AOJModifierEnum;
import br.ufsm.aopjungle.metamodel.commons.AOJPackageDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJParameter;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJAnonymousClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJEnumDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;
import br.ufsm.aopjungle.metrics.AOJContainerMetrics;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeAttribute;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeConstructorMethod;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeEnum;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeMethod;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeObject;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreePackage;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeParent;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeProject;
import br.ufsm.aopjungle.views.patternrefactoring.treeview.TreeType;

public class ViewContentProvider implements IStructuredContentProvider, ITreeContentProvider {
	@XStreamOmitField
	private AOJungleBuilder aopjunglebuilder;
	@XStreamOmitField
	private TreeParent invisibleRoot;
	private IViewSite viewSite;

	public ViewContentProvider(IViewSite viewSite, AOJungleBuilder aopjunglebuilder) {
		this.viewSite = viewSite;
		this.aopjunglebuilder = aopjunglebuilder;
	}

	@Override
	public void inputChanged(Viewer v, Object oldInput, Object newInput) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public Object[] getElements(Object parent) {
		if (parent.equals(viewSite)) {
			if (invisibleRoot == null)
				initialize();
			return getChildren(invisibleRoot);
		}
		return getChildren(parent);
	}

	@Override
	public Object getParent(Object child) {
		if (child instanceof TreeObject) {
			return ((TreeObject) child).getParent();
		}
		return null;
	}

	@Override
	public Object[] getChildren(Object parent) {
		if (parent instanceof TreeParent) {
			return ((TreeParent) parent).getChildren();
		}
		return new Object[0];
	}

	@Override
	public boolean hasChildren(Object parent) {
		if (parent instanceof TreeParent)
			return ((TreeParent) parent).hasChildren();
		return false;
	}

	private void loadContainerMetrics(AOJContainerMetrics metrics, TreeParent node) {
		TreeParent metricNode = new TreeParent("Metrics");
		node.addChild(metricNode);
		TreeParent locNode = new TreeParent("Lines of Code: " + metrics.getNumberOfLines());
		metricNode.addChild(locNode);
		TreeParent nocNode = new TreeParent("Number of Classes: " + metrics.getNumberOfClasses());
		metricNode.addChild(nocNode);
		TreeParent noaNode = new TreeParent("Number of Aspects: " + metrics.getNumberOfAspects());
		metricNode.addChild(noaNode);
		TreeParent noiNode = new TreeParent("Number of Interfaces: " + metrics.getNumberOfInterfaces());
		metricNode.addChild(noiNode);
		TreeParent noeNode = new TreeParent("Number of Enums: " + metrics.getNumberOfEnums());
		metricNode.addChild(noeNode);
		TreeParent nonNode = new TreeParent("Number of Annotations Declarations: " + metrics.getNumberOfAnnotationDecls());
		metricNode.addChild(nonNode);

	}

	// private void loadingProjectInClassPath(){
	// List<IJavaProject> javaProjects = new ArrayList<IJavaProject>();
	// IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
	// for(IProject project: projects){
	// try {
	// project.open(null /* IProgressMonitor */);
	// IJavaProject javaProject = JavaCore.create(project);
	// javaProjects.add(javaProject);
	// } catch (CoreException e) {
	// e.printStackTrace();
	// }
	// }
	//
	// List<URLClassLoader> loaders = new ArrayList<URLClassLoader>();
	// for(IJavaProject project : javaProjects){
	// try {
	// String[] classPathEntries =
	// JavaRuntime.computeDefaultRuntimeClassPath(project);
	// List<URL> urlList = new ArrayList<URL>();
	// for (int i = 0; i < classPathEntries.length; i++) {
	// String entry = classPathEntries[i];
	// IPath path = new Path(entry);
	// URL url = url = path.toFile().toURI().toURL();
	// urlList.add(url);
	// }
	// //---------------------------------------
	// ClassLoader parentClassLoader = project.getClass().getClassLoader();
	// URL[] urls = (URL[]) urlList.toArray(new URL[urlList.size()]);
	// URLClassLoader classLoader = new URLClassLoader(urls, parentClassLoader);
	// loaders.add(classLoader);
	//
	// } catch (CoreException e) {
	// e.printStackTrace();
	// } catch (MalformedURLException e) {
	// e.printStackTrace();
	// }
	// }
	//
	//// List<URLClassLoader> loaders = new ArrayList<URLClassLoader>();
	//// for (IJavaProject javaProject : javaProjects) {
	//// loaders.add(getProjectClassLoader(javaProject));
	//// }
	//
	// for (URLClassLoader loader : loaders) {
	// try {
	// Class<?> clazz = loader.loadClass("fi.Interface04");
	// System.out.println(clazz.getName());
	// } catch (ClassNotFoundException e) {
	// }
	// }
	// //Class c = getClass("fi.Interface04");
	// //System.out.println(c.getName());
	//
	//
	// }

	// private Class getClass(String fullyQualifiedName){
	// for (URLClassLoader loader : loaders) {
	// try {
	// Class<?> clazz = loader.loadClass(fullyQualifiedName);
	// return clazz;
	// } catch (ClassNotFoundException e) {
	// }
	// }
	// return null;
	//
	// }
	//
	public void initialize() {
		List<TreeParent> projectTree = new ArrayList<TreeParent>();
		List<AOJProject> projects = aopjunglebuilder.getWorkspace().getProjects();
		for (AOJProject project : projects) {
			// for (AOJAspectBindingModel b : project.getBindings())
			// AOJAdviceListener.print(b);
			TreeProject projectNode = new TreeProject(project.getName());
			projectTree.add(projectNode);
			loadContainerMetrics(project.getMetrics(), projectNode);
			List<AOJPackageDeclaration> packages = project.getPackages();
			for (AOJPackageDeclaration pack : packages) {
				TreePackage packageNode = new TreePackage(pack.getMetaName());
				projectNode.addChild(packageNode);
				loadContainerMetrics(pack.getMetrics(), packageNode);
				List<AOJCompilationUnit> cUnits = pack.getCompilationUnits();
				for (AOJCompilationUnit cUnit : cUnits) {
					for (AOJContainer typeDeclaration : cUnit.getDeclaredTypes()) {
						loadTypeDeclaration(packageNode, typeDeclaration);
					}
				}
			}
		}

		TreeParent root = new TreeParent("AOP Projects");
		for (TreeParent p : projectTree) {
			root.addChild(p);
		}

		invisibleRoot = new TreeParent("");
		invisibleRoot.addChild(root);
	}

	private void loadTypeDeclaration(TreeParent upperNode, AOJContainer typeDeclaration) {
		TreeParent typeNode;
		if (typeDeclaration instanceof AOJEnumDeclaration) {
			typeNode = new TreeEnum(typeDeclaration.getMetaName() + "." + typeDeclaration.getName(), (AOJTypeDeclaration) typeDeclaration);
		} else {
			typeNode = new TreeType(typeDeclaration.getMetaName() + "." + typeDeclaration.getName(), (AOJTypeDeclaration) typeDeclaration);
		}
		upperNode.addChild(typeNode);
		loadParent(typeDeclaration, typeNode);
		loadTypeModifier(typeDeclaration, typeNode);
		loadAttributes(typeDeclaration, typeNode);
		loadMethods(typeDeclaration, typeNode);
		loadMetrics(typeDeclaration, typeNode);
		loadFunctionalInterface(typeDeclaration, typeNode);

		if (typeDeclaration instanceof AOJBindble)
			loadBindings((AOJBindble) typeDeclaration, typeNode);
		if (typeDeclaration.getMembers() instanceof AOJConcreteTypeMember)
			loadConstructors(typeDeclaration, typeNode);

		if (typeDeclaration instanceof AOJClassHolder)
			loadNestedClasses((AOJClassHolder) typeDeclaration, typeNode);

		if (typeDeclaration instanceof AOJAnnotable)
			loadAnnotations((AOJAnnotable) typeDeclaration, typeNode, true);

		if (typeDeclaration instanceof AOJInterfaceble)
			loadInterfaces((AOJInterfaceble) typeDeclaration, typeNode);

		if (typeDeclaration instanceof AOJAspectDeclaration) {
			AOJAspectDeclaration aspect = (AOJAspectDeclaration) typeDeclaration;
			loadAspectView(aspect, typeNode);
		}

		if (typeDeclaration instanceof AOJEnumDeclaration)
			loadEnumConstants((AOJEnumDeclaration) typeDeclaration, typeNode);
	}

	/**
	 * @param typeDeclaration
	 * @param typeNode
	 */
	private void loadMetrics(AOJContainer typeDeclaration, TreeParent typeNode) {
		TreeParent metricNode = new TreeParent("Metrics");
		typeNode.addChild(metricNode);
		TreeParent locNode = new TreeParent("Lines of Code: " + typeDeclaration.getMetrics().getNumberOfLines());
		metricNode.addChild(locNode);
		TreeParent dtreeNode = new TreeParent("Number of Descendents: " + typeDeclaration.getMetrics().getNumberOfDescendents());
		metricNode.addChild(dtreeNode);
		TreeParent upsizeNode = new TreeParent("Number of Ascendents: " + typeDeclaration.getMetrics().getNumberOfAscendents());
		metricNode.addChild(upsizeNode);
		TreeParent affInNode = new TreeParent("Number of IN Affects: " + typeDeclaration.getMetrics().getNumberOfInAffects());
		metricNode.addChild(affInNode);
		TreeParent affOutNode = new TreeParent("Number of OUT Affects: " + typeDeclaration.getMetrics().getNumberOfOutAffects());
		metricNode.addChild(affOutNode);
	}

	private void loadEnumConstants(AOJEnumDeclaration typeDeclaration, TreeParent typeNode) {
		TreeParent node = newPlaceHolder(typeNode, "Constants");
		for (String c : typeDeclaration.getConstants()) {
			TreeParent nodeC = new TreeParent(c);
			node.addChild(nodeC);
		}
	}

	/*
	 * private void loadInnerClass(AOJTypeDeclaration typeDeclaration, TreeParent
	 * typeNode) { if (typeDeclaration instanceof AOJClassDeclaration) { String
	 * classType = ""; if ( ((AOJClassDeclaration) typeDeclaration).isAnonymous() )
	 * classType = "Anonymous.True"; else if ( ((AOJClassDeclaration)
	 * typeDeclaration).isInnerClass() ) classType = "Innerclass.True";
	 * 
	 * if (! classType.isEmpty() ) newPlaceHolder(typeNode, classType); } }
	 */
	private void loadInterfaces(AOJInterfaceble typeInterfaceble, TreeParent typeNode) {

		if (typeInterfaceble.getImplementedInterfaces().size() > 0) {
			TreeParent node = newPlaceHolder(typeNode, "Implements");
			for (AOJReferencedType intf : typeInterfaceble.getImplementedInterfaces()) {
				TreeParent nodeIntf = new TreeParent(intf.getName());
				node.addChild(nodeIntf);
			}
		}
	}

	private TreeParent newPlaceHolder(TreeParent typeNode, String name) {
		TreeParent node = new TreeParent(name);
		typeNode.addChild(node);
		return node;
	}

	private void loadParent(AOJContainer typeDeclaration, TreeParent typeNode) {
		if (typeDeclaration.getParent() != null) {
			TreeParent node = newPlaceHolder(typeNode, "Extends");
			TreeParent nodeExtends = new TreeParent(typeDeclaration.getParent().getFullQualifiedName());
			node.addChild(nodeExtends);
		}
	}

	private void loadMethods(AOJContainer typeDeclaration, TreeParent typeNode) {
		for (AOJBehaviourKind methodDeclaration : typeDeclaration.getMembers().getMethods()) {
			TreeMethod methodNode = new TreeMethod(methodDeclaration.getMetaName() + "." + methodDeclaration.getName(), (AOJTypeDeclaration)typeDeclaration, methodDeclaration.getNode());
			typeNode.addChild(methodNode);
			loadAnnotations(methodDeclaration, methodNode, true);
			loadMethodReturnType(methodDeclaration, methodNode);
			loadMethodParams(methodDeclaration, methodNode);
			loadMethodThrowExceptions(methodDeclaration, methodNode);
			loadMethodModifier(methodDeclaration, methodNode);
			loadNestedClasses(methodDeclaration, methodNode);
			loadMethodMetrics(methodDeclaration, methodNode);
			if (typeDeclaration instanceof AOJBindble)
				loadBindings(methodDeclaration, methodNode);

		}
	}

	private void loadConstructors(AOJContainer typeDeclaration, TreeParent typeNode) {
		AOJConcreteTypeMember members = (AOJConcreteTypeMember) typeDeclaration.getMembers();
		for (AOJConstructorDeclaration constructorDeclaration : members.getConstructors()) {
			TreeConstructorMethod constructorNode = new TreeConstructorMethod(constructorDeclaration.getMetaName() + "." + constructorDeclaration.getName(), (AOJTypeDeclaration) typeDeclaration, constructorDeclaration.getNode());
			typeNode.addChild(constructorNode);
			loadMethodParams(constructorDeclaration, constructorNode);
			loadMethodThrowExceptions(constructorDeclaration, constructorNode);
			loadMethodModifier(constructorDeclaration, constructorNode);
			loadMethodMetrics(constructorDeclaration, constructorNode);
			if (constructorDeclaration instanceof AOJBindble)
				loadBindings(constructorDeclaration, constructorNode);
		}
	}

	private void loadMethodMetrics(AOJBehaviourKind methodDeclaration, TreeParent methodNode) {
		TreeParent metricNode = new TreeParent("Metrics");
		methodNode.addChild(metricNode);
		TreeParent nstaNode = new TreeParent("Number of Statements: " + methodDeclaration.getMetrics().getNumberOfStatements());
		metricNode.addChild(nstaNode);
		TreeParent locNode = new TreeParent("Lines of Code: " + methodDeclaration.getMetrics().getNumberOfLines());
		metricNode.addChild(locNode);
		TreeParent affInNode = new TreeParent("Number of IN Affects: " + methodDeclaration.getMetrics().getNumberOfInAffects());
		metricNode.addChild(affInNode);
		TreeParent affOutNode = new TreeParent("Number of OUT Affects: " + methodDeclaration.getMetrics().getNumberOfOutAffects());
		metricNode.addChild(affOutNode);
	}

	private void loadBindings(AOJBindble binding, TreeParent node) {
		TreeParent bindingNode = new TreeParent("Bindings");
		node.addChild(bindingNode);
		TreeParent out = new TreeParent("Out");
		bindingNode.addChild(out);
		TreeParent in = new TreeParent("In");
		bindingNode.addChild(in);

		for (AOJBinding b : binding.getBindingModel().getOut()) {
			TreeParent affectsNodeData = new TreeParent("(" + b.getKind() + ") " + b.getBindingHandler());
			out.addChild(affectsNodeData);
		}
		for (AOJBinding b : binding.getBindingModel().getIn()) {
			TreeParent affectsNodeData = new TreeParent("(" + b.getKind() + ") " + b.getBindingHandler());
			in.addChild(affectsNodeData);
		}
	}

	private void loadMethodModifier(AOJMember methodDeclaration, TreeParent methodNode) {
		AOJModifier aojMethodModifier = methodDeclaration.getModifiers();
		if (aojMethodModifier.getModifiers().size() > 0) {
			TreeParent modifiersMethodNode = new TreeParent("Modifiers");
			methodNode.addChild(modifiersMethodNode);
			for (AOJModifierEnum modifier : aojMethodModifier.getModifiers()) {
				TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
				modifiersMethodNode.addChild(modifierNode);
			}
		}
	}

	private void loadMethodThrowExceptions(AOJBehaviourKind methodDeclaration, TreeParent methodNode) {
		if (methodDeclaration.getThrownExceptions().size() > 0) {
			TreeParent exceptionsMethodNode = newPlaceHolder(methodNode, "ThrowExceptions");
			List<AOJException> exceptions = methodDeclaration.getThrownExceptions();
			int i = 0;
			for (AOJException exception : exceptions) {
				TreeParent exceptionMethodNode = new TreeParent("Type" + i++ + "." + exception.getName());
				exceptionsMethodNode.addChild(exceptionMethodNode);
			}
		}
	}

	private void loadMethodParams(AOJBehaviourKind methodDeclaration, TreeParent methodNode) {
		if (methodDeclaration.getParameters().size() > 0) {
			TreeParent paramsMethodNode = new TreeParent("Params");
			methodNode.addChild(paramsMethodNode);
			List<AOJParameter> params = methodDeclaration.getParameters();
			int i = 0;
			for (AOJParameter param : params) {
				TreeParent paramMethodNode = new TreeParent("Param" + i++ + "." + param.getName());
				paramsMethodNode.addChild(paramMethodNode);
				TreeParent paramTypeMethodNode = new TreeParent("Type." + param.getType().getName());
				paramMethodNode.addChild(paramTypeMethodNode);
			}
		}
	}

	private void loadMethodReturnType(AOJBehaviourKind methodDeclaration, TreeParent methodNode) {
		methodNode.addChild(new TreeParent("ReturnType." + methodDeclaration.getReturnType().getName()));
	}

	private void loadAttributes(AOJContainer typeDeclaration, TreeParent typeNode) {
		for (AOJAttributeDeclaration attributeDeclaration : typeDeclaration.getMembers().getAttributes()) {
			TreeAttribute attributeNode = new TreeAttribute(attributeDeclaration.getMetaName() + "." + attributeDeclaration.getName(), (AOJTypeDeclaration)typeDeclaration, attributeDeclaration.getNode());
			typeNode.addChild(attributeNode);
			loadAnnotations(attributeDeclaration, attributeNode, true);
			loadAttributeType(attributeDeclaration, attributeNode);
			loadAttributeModifier(attributeDeclaration, attributeNode);
			loadNestedClasses(attributeDeclaration, attributeNode);
			if (attributeDeclaration instanceof AOJBindble)
				loadBindings(attributeDeclaration, attributeNode);
		}
	}

	private void loadNestedClasses(AOJClassHolder holderDeclaration, TreeParent node) {
		for (AOJContainer clazz : holderDeclaration.getAnonymousClasses())
			loadTypeDeclaration(node, clazz);

		for (AOJContainer clazz : holderDeclaration.getInnerClasses())
			loadTypeDeclaration(node, clazz);
	}

	private void loadAnnotations(AOJAnnotable node, TreeParent treeParent, boolean createPlaceHolder) {
		TreeParent treeNode = null;
		if ((createPlaceHolder) && (node.getAnnotations().size() > 0)) {
			TreeParent annotationNode = new TreeParent("Annotations");
			treeParent.addChild(annotationNode);
			treeNode = annotationNode;
		} else {
			treeNode = treeParent;
		}

		for (AOJAnnotationDeclaration annotationDeclaration : node.getAnnotations()) {
			TreeParent annotationDeclarationNode = new TreeParent("@" + annotationDeclaration.getName());
			treeNode.addChild(annotationDeclarationNode);
		}
	}

	private void loadAttributeModifier(AOJAttributeDeclaration attributeDeclaration, TreeParent attributeNode) {
		AOJModifier aojAttributeModifier = attributeDeclaration.getModifiers();

		if (aojAttributeModifier.getModifiers().size() > 0) {
			TreeParent modifiersAttributeNode = new TreeParent("Modifiers");
			attributeNode.addChild(modifiersAttributeNode);
			for (AOJModifierEnum modifier : aojAttributeModifier.getModifiers()) {
				TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
				modifiersAttributeNode.addChild(modifierNode);
			}
		}
	}

	private void loadAttributeType(AOJAttributeDeclaration attributeDeclaration, TreeParent attributeNode) {
		attributeNode.addChild(new TreeParent("Type." + attributeDeclaration.getType().getName()));
	}

	private void loadTypeModifier(AOJContainer typeDeclaration, TreeParent typeNode) {
		AOJModifier aojTypeModifier = typeDeclaration.getModifiers();
		if (aojTypeModifier.getModifiers().size() > 0) {
			TreeParent modifiersTypeNode = new TreeParent("Modifiers");
			typeNode.addChild(modifiersTypeNode);
			for (AOJModifierEnum modifier : aojTypeModifier.getModifiers()) {
				TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
				modifiersTypeNode.addChild(modifierNode);
			}
		}
	}

	private void loadFunctionalInterface(AOJContainer typeDeclaration, TreeParent typeNode) {
		if ((typeDeclaration instanceof AOJAnonymousClassDeclaration)) {
			AOJAnonymousClassDeclaration anonymous = (AOJAnonymousClassDeclaration) typeDeclaration;

			TreeParent modifiersTypeNode = new TreeParent("FunctionalInterface");
			typeNode.addChild(modifiersTypeNode);

			TreeParent modifierNode = new TreeParent(anonymous.isFunctionalInterface().toString());
			modifiersTypeNode.addChild(modifierNode);
		}
		if (typeDeclaration instanceof AOJInterfaceDeclaration) {
			AOJInterfaceDeclaration i = (AOJInterfaceDeclaration) typeDeclaration;

			TreeParent modifiersTypeNode = new TreeParent("FunctionalInterface");
			typeNode.addChild(modifiersTypeNode);

			TreeParent modifierNode = new TreeParent(i.isFunctionalInterface().toString());
			modifiersTypeNode.addChild(modifierNode);
		}
	}

	private void loadCompileWarningsAndErrors(AOJAspectDeclaration aspect, TreeParent typeNode) {
		if (aspect.getMembers().getDeclareWarnings() != null) {
			for (AOJDeclareCompilationEnforcement declare : aspect.getMembers().getDeclareWarnings()) {
				TreeParent compileWarningNode = new TreeParent("Compile.Warnings");
				typeNode.addChild(compileWarningNode);
				compileWarningNode.addChild(new TreeParent("Pointcut." + declare.getPointcutAsString()));
				compileWarningNode.addChild(new TreeParent("Message." + declare.getMessage()));
			}
		}

		if (aspect.getMembers().getDeclareErrors() != null) {
			for (AOJDeclareCompilationEnforcement declare : aspect.getMembers().getDeclareErrors()) {
				TreeParent compileErrorsNode = new TreeParent("Compile.Errors");
				typeNode.addChild(compileErrorsNode);
				compileErrorsNode.addChild(new TreeParent("Pointcut." + declare.getPointcutAsString()));
				compileErrorsNode.addChild(new TreeParent("Message." + declare.getMessage()));
			}
		}
	}

	private void loadAspectView(AOJAspectDeclaration aspect, TreeParent typeNode) {
		loadPrivileged(aspect, typeNode);
		loadDeclarePrecedences(aspect, typeNode);
		loadDeclareParents(aspect, typeNode);
		loadDeclareFields(aspect, typeNode);
		loadDeclareMethods(aspect, typeNode);
		loadDeclareConstructors(aspect, typeNode);
		loadDeclareSoft(aspect, typeNode);
		loadCompileWarningsAndErrors(aspect, typeNode);
		loadPointcutDeclaration(aspect, typeNode);
		loadPerClause(aspect, typeNode);
		loadAdvices(aspect, typeNode);
	}

	private void loadDeclareSoft(AOJAspectDeclaration aspect, TreeParent typeNode) {
		if (aspect.getMembers().getDeclareSofts().size() > 0) {
			TreeParent node = newPlaceHolder(typeNode, "Declare.Soft");
			for (AOJDeclareSoft declareSoft : aspect.getMembers().getDeclareSofts()) {
				loadDeclareSoftPointcuts(declareSoft, node);
				loadDeclareSoftExceptionPatterns(declareSoft, node);
				if (declareSoft instanceof AOJBindble)
					loadBindings(declareSoft, node);
				loadDeclareSoftMetrics(declareSoft, node);
			}

		}
	}

	private void loadDeclareSoftMetrics(AOJDeclareSoft declareSoft, TreeParent node) {
		TreeParent metricNode = new TreeParent("Metrics");
		node.addChild(metricNode);
		TreeParent affInNode = new TreeParent("Number of IN Affects: " + declareSoft.getMetrics().getNumberOfInAffects());
		metricNode.addChild(affInNode);
		TreeParent affOutNode = new TreeParent("Number of OUT Affects: " + declareSoft.getMetrics().getNumberOfOutAffects());
		metricNode.addChild(affOutNode);
	}

	private void loadDeclareSoftExceptionPatterns(AOJDeclareSoft declareSoft, TreeParent nodeDeclareSoftName) {
		TreeParent node1 = newPlaceHolder(nodeDeclareSoftName, "Exception");
		node1.addChild(new TreeParent(declareSoft.getExceptionPatterns()));
	}

	private void loadDeclareSoftPointcuts(AOJDeclareSoft declareSoft, TreeParent nodeDeclareSoftName) {
		TreeParent node1 = newPlaceHolder(nodeDeclareSoftName, "Pointcut");
		node1.addChild(new TreeParent(declareSoft.getPointcut().getCode()));
	}

	private void loadPerClause(AOJAspectDeclaration aspect, TreeParent typeNode) {
		if (aspect.getPerClause() != null) {
			TreeParent node = new TreeParent("Instanciation");
			typeNode.addChild(node);
			TreeParent node2 = new TreeParent("Type");
			node.addChild(node2);
			TreeParent node3 = new TreeParent("Pointcut");
			node.addChild(node3);
			AOJPointcutExpression pcExpression = new AOJPointcutExpression(aspect.getPerClause());
			node2.addChild(new TreeParent(pcExpression.getTypeAsString(AOJPointcutExpression.FATSTRING)));
			node3.addChild(new TreeParent(pcExpression.getCode()));
		}
	}

	// private void loadPointcutCFlow(AOJAspectDeclaration aspect,
	// TreeParent typeNode) {
	// for (AOJPointcutCFlow pointcutCFlow :
	// aspect.getMembers().getcFlowPointcuts()) {
	// TreeParent pointcutNode = new
	// TreeParent(pointcutCFlow.getMetaName()+"."+pointcutCFlow.getName());
	// typeNode.addChild(pointcutNode);
	// }
	// }

	private void loadDeclareConstructors(AOJAspectDeclaration aspect, TreeParent typeNode) {
		if (aspect.getMembers().getDeclareConstructors().size() > 0) {
			TreeParent node = newPlaceHolder(typeNode, "Declare.Constructors");
			for (AOJDeclareMethod declareConstructor : aspect.getMembers().getDeclareConstructors())
				loadDeclareMethodNode(node, declareConstructor);
		}
	}

	private void loadDeclareMethods(AOJAspectDeclaration aspect, TreeParent typeNode) {
		if (aspect.getMembers().getDeclareMethods().size() > 0) {
			TreeParent node = newPlaceHolder(typeNode, "Declare.Methods");
			for (AOJDeclareMethod declareMethod : aspect.getMembers().getDeclareMethods())
				loadDeclareMethodNode(node, declareMethod);
		}
	}

	private void loadDeclareMethodMetrics(AOJDeclareMethod declareMethod, TreeParent node) {
		TreeParent metricNode = new TreeParent("Metrics");
		node.addChild(metricNode);
		TreeParent affInNode = new TreeParent("Number of IN Affects: " + declareMethod.getMetrics().getNumberOfInAffects());
		metricNode.addChild(affInNode);
		TreeParent affOutNode = new TreeParent("Number of OUT Affects: " + declareMethod.getMetrics().getNumberOfOutAffects());
		metricNode.addChild(affOutNode);
	}

	private void loadDeclareMethodNode(TreeParent node, AOJDeclareMethod declareMethod) {
		TreeParent nodeDeclareMethodName = new TreeParent("Name." + declareMethod.getName());
		node.addChild(nodeDeclareMethodName);
		loadAnnotations(declareMethod, nodeDeclareMethodName, true);
		loadDeclareMethodReturnType(declareMethod, nodeDeclareMethodName);
		loadDeclareMethodParams(declareMethod, nodeDeclareMethodName);
		loadDeclareMethodThrowExceptions(declareMethod, nodeDeclareMethodName);
		loadDeclareMethodModifier(declareMethod, nodeDeclareMethodName);
		loadDeclareMethodApplyOnType(declareMethod, nodeDeclareMethodName);
		if (declareMethod instanceof AOJBindble)
			loadBindings(declareMethod, nodeDeclareMethodName);
		loadDeclareMethodMetrics(declareMethod, node);
	}

	private void loadDeclareMethodApplyOnType(AOJDeclareMethod declareMethod, TreeParent node) {
		TreeParent nodeDeclareMethodOnType = new TreeParent("ApplyOnType." + declareMethod.getOnType());
		node.addChild(nodeDeclareMethodOnType);
	}

	private void loadDeclareMethodModifier(AOJDeclareMethod declareMethod, TreeParent node) {
		AOJModifier aojMethodModifier = declareMethod.getModifiers();
		if (aojMethodModifier.getModifiers().size() > 0) {
			TreeParent modifiersMethodNode = new TreeParent("Modifiers");
			node.addChild(modifiersMethodNode);

			for (AOJModifierEnum modifier : aojMethodModifier.getModifiers()) {
				TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
				modifiersMethodNode.addChild(modifierNode);
			}
		}
	}

	private void loadDeclareMethodThrowExceptions(AOJDeclareMethod declareMethod, TreeParent node) {
		if (declareMethod.getThrownExceptions().size() > 0) {
			List<AOJException> exceptions = declareMethod.getThrownExceptions();
			TreeParent exceptionsMethodNode = newPlaceHolder(node, "ThrowExceptions");
			int i = 0;
			for (AOJException exception : exceptions) {
				TreeParent exceptionMethodNode = new TreeParent("Type" + i++ + "." + exception.getName());
				exceptionsMethodNode.addChild(exceptionMethodNode);
			}
		}
	}

	private void loadDeclareMethodParams(AOJDeclareMethod declareMethod, TreeParent node) {
		if (declareMethod.getParameters().size() > 0) {
			TreeParent paramsMethodNode = new TreeParent("Params");
			node.addChild(paramsMethodNode);
			List<AOJParameter> params = declareMethod.getParameters();
			int i = 0;
			for (AOJParameter param : params) {
				TreeParent paramMethodNode = new TreeParent("Param" + i++ + "." + param.getName());
				paramsMethodNode.addChild(paramMethodNode);
				TreeParent paramTypeMethodNode = new TreeParent("Type." + param.getType().getName());
				paramMethodNode.addChild(paramTypeMethodNode);
			}
		}
	}

	private void loadDeclareMethodReturnType(AOJDeclareMethod declareMethod, TreeParent node) {
		node.addChild(new TreeParent("ReturnType." + declareMethod.getReturnType().getName()));
	}

	private void loadDeclareFields(AOJAspectDeclaration aspect, TreeParent typeNode) {

		TreeParent node = null;

		if (aspect.getMembers().getDeclareFields().size() > 0) {
			node = newPlaceHolder(typeNode, "Declare.Fields");
			for (AOJDeclareField declareField : aspect.getMembers().getDeclareFields()) {
				TreeParent nodeDeclareFieldName = new TreeParent("Name." + declareField.getName());
				node.addChild(nodeDeclareFieldName);

				loadDeclareFieldType(nodeDeclareFieldName, declareField);
				loadFieldApplyOnType(nodeDeclareFieldName, declareField);
				loadDeclareFieldModifier(nodeDeclareFieldName, declareField);
				if (declareField instanceof AOJBindble)
					loadBindings(declareField, nodeDeclareFieldName);
				loadDeclareFieldsMetrics(declareField, node);
			}
		}

	}

	private void loadDeclareFieldsMetrics(AOJDeclareField declareField, TreeParent node) {
		TreeParent metricNode = new TreeParent("Metrics");
		node.addChild(metricNode);
		TreeParent affInNode = new TreeParent("Number of IN Affects: " + declareField.getMetrics().getNumberOfInAffects());
		metricNode.addChild(affInNode);
		TreeParent affOutNode = new TreeParent("Number of OUT Affects: " + declareField.getMetrics().getNumberOfOutAffects());
		metricNode.addChild(affOutNode);
	}

	private void loadDeclareFieldModifier(TreeParent node, AOJDeclareField declareField) {
		AOJModifier aojDeclareFieldModifiers = declareField.getModifiers();

		if (aojDeclareFieldModifiers.getModifiers().size() > 0) {
			TreeParent nodeDeclareFieldModifier = new TreeParent("Modifiers");
			node.addChild(nodeDeclareFieldModifier);
			for (AOJModifierEnum modifier : aojDeclareFieldModifiers.getModifiers()) {
				TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
				nodeDeclareFieldModifier.addChild(modifierNode);
			}
		}
	}

	private void loadFieldApplyOnType(TreeParent node, AOJDeclareField declareField) {
		TreeParent nodeDeclareFieldOnType = new TreeParent("ApplyOnType." + declareField.getOnType());
		node.addChild(nodeDeclareFieldOnType);
	}

	private void loadDeclareFieldType(TreeParent node, AOJDeclareField declareField) {
		TreeParent nodeDeclareFieldType = new TreeParent("Type." + declareField.getType().getName());
		node.addChild(nodeDeclareFieldType);
	}

	private void loadDeclareParents(AOJAspectDeclaration aspect, TreeParent typeNode) {
		TreeParent node = null;

		List<AOJDeclareParents> extds = aspect.getMembers().getDeclareParentsExtension();
		List<AOJDeclareParents> impls = aspect.getMembers().getDeclareParentsImplementation();

		if ((extds.size() > 0) || (impls.size() > 0)) {
			node = newPlaceHolder(typeNode, "Declare.Parents");
			loadDeclareParentsMetrics(aspect, node);
		}

		if (extds.size() > 0) {
			TreeParent node2 = newPlaceHolder(node, "Extends");
			for (AOJDeclareParents declareParent : extds) {
				loadParents(node2, declareParent);
			}
		}

		if (impls.size() > 0) {
			TreeParent node2 = newPlaceHolder(node, "Implements");
			for (AOJDeclareParents declareParent : impls) {
				loadImplements(node2, declareParent);
			}
		}

	}

	private void loadDeclareParentsMetrics(AOJAspectDeclaration aspect, TreeParent node) {
		int inAff = 0, outAff = 0;
		for (AOJDeclareParents declareParents : aspect.getMembers().getDeclareParents()) {
			inAff += declareParents.getMetrics().getNumberOfInAffects();
			outAff += declareParents.getMetrics().getNumberOfOutAffects();
		}

		TreeParent metricNode = new TreeParent("Metrics");
		node.addChild(metricNode);
		TreeParent affInNode = new TreeParent("NAFFin " + inAff);
		metricNode.addChild(affInNode);
		TreeParent affOutNode = new TreeParent("NAFFout " + outAff);
		metricNode.addChild(affOutNode);
	}

	private void loadImplements(TreeParent node, AOJDeclareParents declareParent) {
		TreeParent nodeChild = new TreeParent("Type." + declareParent.getChildType().getTypePatternExpression().toString());
		node.addChild(nodeChild);
		for (TypePattern type : declareParent.getParentType()) {
			TreeParent nodeParent = new TreeParent("Interface." + type.getTypePatternExpression().toString());
			node.addChild(nodeParent);
		}
	}

	private void loadParents(TreeParent node, AOJDeclareParents declareParent) {
		TreeParent nodeChild = new TreeParent("Child." + declareParent.getChildType().getTypePatternExpression().toString());
		node.addChild(nodeChild);
		TreeParent nodeParent = new TreeParent("Super." + declareParent.getParentType().get(0).getTypePatternExpression().toString());
		node.addChild(nodeParent);
	}

	private void loadAdvices(AOJAspectDeclaration aspect, TreeParent typeNode) {
		loadAdviceBefore(aspect, typeNode);
		loadAdviceAfter(aspect, typeNode);
		loadAdviceAfterReturning(aspect, typeNode);
		loadAdviceAfterThrowing(aspect, typeNode);
		loadAdviceAround(aspect, typeNode);
	}

	private void loadAdviceAround(AOJAspectDeclaration aspect, TreeParent typeNode) {
		for (AOJAdviceDeclaration adviceDeclaration : aspect.getMembers().getAdvicesAround()) {
			TreeParent adviceNode = new TreeParent(adviceDeclaration.getMetaName());
			typeNode.addChild(adviceNode);
			loadAnnotations(adviceDeclaration, adviceNode, true);
			loadAdvicePointcut(adviceDeclaration, adviceNode);
			loadAdviceParams(adviceDeclaration, adviceNode);
			loadAdviceThrowException(adviceDeclaration, adviceNode);
			loadAdviceReturnType(adviceDeclaration, adviceNode);
			if (adviceDeclaration instanceof AOJBindble)
				loadBindings(adviceDeclaration, adviceNode);
			loadAdviceMetrics(adviceDeclaration, adviceNode);
		}
	}

	private void loadAdviceReturnType(AOJAdviceDeclaration adviceDeclaration, TreeParent adviceNode) {
		adviceNode.addChild(new TreeParent("ReturnType." + adviceDeclaration.getReturnType().getName()));
	}

	private void loadAdvicePointcut(AOJAdviceDeclaration adviceDeclaration, TreeParent adviceNode) {
		TreeParent pointcutNode = new TreeParent("Pointcut." + adviceDeclaration.getPointcut().getCode());
		adviceNode.addChild(pointcutNode);
	}

	private void loadAdviceAfterThrowing(AOJAspectDeclaration aspect, TreeParent typeNode) {
		for (AOJAdviceDeclaration adviceDeclaration : aspect.getMembers().getAdvicesAfterThrowing()) {
			TreeParent adviceNode = new TreeParent(adviceDeclaration.getMetaName());
			typeNode.addChild(adviceNode);
			loadAnnotations(adviceDeclaration, typeNode, true);
			loadAdvicePointcut(adviceDeclaration, adviceNode);
			loadAdviceParams(adviceDeclaration, adviceNode);
			loadAdviceThrowException(adviceDeclaration, adviceNode);
			if (adviceDeclaration instanceof AOJBindble)
				loadBindings(adviceDeclaration, adviceNode);
			loadAdviceMetrics(adviceDeclaration, adviceNode);
		}
	}

	private void loadAdviceAfterReturning(AOJAspectDeclaration aspect, TreeParent typeNode) {
		for (AOJAdviceDeclaration adviceDeclaration : aspect.getMembers().getAdvicesAfterReturning()) {
			TreeParent adviceNode = new TreeParent(adviceDeclaration.getMetaName());
			typeNode.addChild(adviceNode);
			loadAnnotations(adviceDeclaration, adviceNode, true);
			loadAdvicePointcut(adviceDeclaration, adviceNode);
			loadAdviceParams(adviceDeclaration, adviceNode);
			loadAdviceThrowException(adviceDeclaration, adviceNode);
			if (adviceDeclaration instanceof AOJBindble)
				loadBindings(adviceDeclaration, adviceNode);
			loadAdviceMetrics(adviceDeclaration, adviceNode);
		}
	}

	private void loadAdviceAfter(AOJAspectDeclaration aspect, TreeParent typeNode) {
		for (AOJAdviceDeclaration adviceDeclaration : aspect.getMembers().getAdvicesAfter()) {
			TreeParent adviceNode = new TreeParent(adviceDeclaration.getMetaName());
			typeNode.addChild(adviceNode);
			loadAnnotations(adviceDeclaration, adviceNode, true);
			loadAdvicePointcut(adviceDeclaration, adviceNode);
			loadAdviceParams(adviceDeclaration, adviceNode);
			loadAdviceThrowException(adviceDeclaration, adviceNode);
			if (adviceDeclaration instanceof AOJBindble)
				loadBindings(adviceDeclaration, adviceNode);
			loadAdviceMetrics(adviceDeclaration, adviceNode);
		}
	}

	private void loadAdviceBefore(AOJAspectDeclaration aspect, TreeParent typeNode) {
		for (AOJAdviceDeclaration adviceDeclaration : aspect.getMembers().getAdvicesBefore()) {
			TreeParent adviceNode = new TreeParent(adviceDeclaration.getMetaName());
			typeNode.addChild(adviceNode);

			loadAnnotations(adviceDeclaration, adviceNode, true);
			loadAdvicePointcut(adviceDeclaration, adviceNode);
			loadAdviceParams(adviceDeclaration, adviceNode);
			loadAdviceThrowException(adviceDeclaration, adviceNode);
			if (adviceDeclaration instanceof AOJBindble)
				loadBindings(adviceDeclaration, adviceNode);
			loadAdviceMetrics(adviceDeclaration, adviceNode);

		}
	}

	// private void loadAdviceBindings(AOJAdviceDeclaration adviceDeclaration,
	// TreeParent adviceNode) {
	// TreeParent bindingNode = new TreeParent("Bindings");
	// adviceNode.addChild(bindingNode);
	// TreeParent affectsNode = new TreeParent("Affects ");
	// bindingNode.addChild(affectsNode);
	// TreeParent affectedNode = new TreeParent("Affected By");
	// bindingNode.addChild(affectedNode);
	//
	// for (AOJBinding b: adviceDeclaration.getBindingModel().getOut()) {
	// TreeParent affectsNodeData = new TreeParent("Affects (" + b.getKind() + ")" +
	// b.getHandler());
	// affectsNode.addChild(affectsNodeData);
	// }
	// for (AOJBinding b: adviceDeclaration.getBindingModel().getIn()) {
	// TreeParent affectsNodeData = new TreeParent("Affected by (" + b.getKind() +
	// ")" + b.getHandler());
	// affectedNode.addChild(affectsNodeData);
	// }
	// }

	private void loadAdviceMetrics(AOJAdviceDeclaration adviceDeclaration, TreeParent adviceNode) {
		TreeParent metricNode = new TreeParent("Metrics");
		adviceNode.addChild(metricNode);
		TreeParent nstaNode = new TreeParent("Number of Statements: " + adviceDeclaration.getMetrics().getNumberOfStatements());
		metricNode.addChild(nstaNode);
		TreeParent locNode = new TreeParent("Lines of Code: " + adviceDeclaration.getMetrics().getNumberOfLines());
		metricNode.addChild(locNode);
		TreeParent affInNode = new TreeParent("Number of IN Affects: " + adviceDeclaration.getMetrics().getNumberOfInAffects());
		metricNode.addChild(affInNode);
		TreeParent affOutNode = new TreeParent("Number of OUT Affects: " + adviceDeclaration.getMetrics().getNumberOfOutAffects());
		metricNode.addChild(affOutNode);
	}

	private void loadAdviceThrowException(AOJBehaviourKind adviceDeclaration, TreeParent adviceNode) {
		if (adviceDeclaration.getThrownExceptions().size() > 0) {
			TreeParent exceptionsMethodNode = new TreeParent("ThrowExceptions");
			adviceNode.addChild(exceptionsMethodNode);
			List<AOJException> exceptions = adviceDeclaration.getThrownExceptions();
			int i = 0;
			for (AOJException exception : exceptions) {
				TreeParent exceptionMethodNode = new TreeParent("Type" + i++ + "." + exception.getName());
				exceptionsMethodNode.addChild(exceptionMethodNode);
			}
		}
	}

	private void loadAdviceParams(AOJBehaviourKind adviceDeclaration, TreeParent adviceNode) {
		if (adviceDeclaration.getParameters().size() > 0) {
			TreeParent paramsMethodNode = new TreeParent("Params");
			adviceNode.addChild(paramsMethodNode);
			List<AOJParameter> params = adviceDeclaration.getParameters();
			int i = 0;
			for (AOJParameter param : params) {
				TreeParent paramMethodNode = new TreeParent("Param" + i++ + "." + param.getName());
				paramsMethodNode.addChild(paramMethodNode);
				TreeParent paramTypeMethodNode = new TreeParent("Type." + param.getType().getName());
				paramMethodNode.addChild(paramTypeMethodNode);
			}
		}
	}

	private void loadPointcutDeclaration(AOJAspectDeclaration aspect, TreeParent typeNode) {
		for (AOJPointcutDeclaration pointcutDeclaration : aspect.getMembers().getPointcuts()) {
			TreeParent pointcutNode = new TreeParent(pointcutDeclaration.getMetaName() + "." + pointcutDeclaration.getName());
			typeNode.addChild(pointcutNode);
			loadAnnotations(pointcutDeclaration, pointcutNode, true);
			loadPointcutModifier(pointcutDeclaration, pointcutNode);
			loadPointcutCode(pointcutDeclaration, pointcutNode);
		}
	}

	private void loadPointcutCode(AOJPointcutDeclaration pointcutDeclaration, TreeParent pointcutNode) {
		TreeParent pointcutCodeNameNode = new TreeParent("Code");
		pointcutNode.addChild(pointcutCodeNameNode);
		TreeParent pointcutCodeNode = new TreeParent(pointcutDeclaration.getPointcutExpression().getCode());
		pointcutCodeNameNode.addChild(pointcutCodeNode);
	}

	private void loadPointcutModifier(AOJPointcutDeclaration pointcutDeclaration, TreeParent pointcutNode) {
		AOJModifier aojPointcutModifier = pointcutDeclaration.getModifiers();
		if (aojPointcutModifier.getModifiers().size() > 0) {
			TreeParent modifiersPointcutNode = new TreeParent("Modifiers");
			pointcutNode.addChild(modifiersPointcutNode);

			for (AOJModifierEnum modifier : aojPointcutModifier.getModifiers()) {
				TreeParent modifierNode = new TreeParent(modifier.toString().toLowerCase());
				modifiersPointcutNode.addChild(modifierNode);
			}
		}
	}

	private void loadDeclarePrecedences(AOJAspectDeclaration aspect, TreeParent typeNode) {
		if (aspect.getMembers().getDeclarePrecedences() != null)
			for (AOJDeclarePrecedence precedence : aspect.getMembers().getDeclarePrecedences()) {
				if (precedence.toString().length() > 0) {
					TreeParent precedenceNode = new TreeParent("Precedences");
					typeNode.addChild(precedenceNode);
					int i = 1;
					for (String expression : precedence.getExpressions())
						precedenceNode.addChild(new TreeParent(i++ + "." + expression));
				}
			}
	}

	private void loadPrivileged(AOJAspectDeclaration aspect, TreeParent typeNode) {
		typeNode.addChild(new TreeParent(aspect.isPriviledge() ? "Privileged.ON" : "Privileged.OFF"));
	}
}