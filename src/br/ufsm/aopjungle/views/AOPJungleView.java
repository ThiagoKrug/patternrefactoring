package br.ufsm.aopjungle.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.DrillDownAdapter;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.IProgressService;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.ufsm.aopjungle.astor.AstorSession;
import br.ufsm.aopjungle.astor.ConvertFunctionalInterfaceToDefaultFunctionalInterface;
import br.ufsm.aopjungle.astor.ConvertITMDtoDefaultMethod;
import br.ufsm.aopjungle.astor.ConvertInterfaceToDefaultMethod;
import br.ufsm.aopjungle.astor.EncloseEnhancedFor;
import br.ufsm.aopjungle.astor.EncloseForWithIfToFilter;
import br.ufsm.aopjungle.astor.EncloseFunctionalInterface;
import br.ufsm.aopjungle.astor.EncloseSort;
import br.ufsm.aopjungle.binding.AOJInheritanceResolver;
import br.ufsm.aopjungle.builder.AOJExporterXML;
import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.builder.AOJungleBuilder;
import br.ufsm.aopjungle.hql.AOJProjectDAO;
import br.ufsm.aopjungle.labs.AOJBindingOptionEnum;
import br.ufsm.aopjungle.log.AOJLogger;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.patternrefactoring.EncapsulateClassWithFactory;
import br.ufsm.aopjungle.patternrefactoring.ReplaceConditionalDispatcherWithCommand;
import br.ufsm.aopjungle.patternrefactoring.ReplaceImplicitTreeWithComposite;
import br.ufsm.aopjungle.patternrefactoring.util.PRProp;
import br.ufsm.aopjungle.resource.ProjectResources;
import br.ufsm.aopjungle.util.IconProvider;
import br.ufsm.aopjungle.views.patternrefactoring.DoubleClickListener;
import br.ufsm.aopjungle.views.patternrefactoring.SelectProjectDialog;
import br.ufsm.aopjungle.views.patternrefactoring.parameters.ParametersAndWeightsDialog;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 *         Federal University of Santa Maria <br>
 *         Programming Languages and Database Research Group <br>
 */
public class AOPJungleView extends ViewPart {
	@XStreamOmitField
	public static final String ID = "br.ufsm.aopjungle.views.AOPJungleView";
	@XStreamOmitField
	private TreeViewer viewer;
	@XStreamOmitField
	private DrillDownAdapter drillDownAdapter;
	@XStreamOmitField
	private Action actionExportXML;
	@XStreamOmitField
	private Action actionExecuteAstor;
	@XStreamOmitField
	private Action actionExecutePattern;
	@XStreamOmitField
	private Action actionExecuteQuery;
	@XStreamOmitField
	private Action actionExecuteAOPJungle;
	@XStreamOmitField
	private Action actionPatternsSettings;
	@XStreamOmitField
	private Action actionRefreshAOPJungle;
	@XStreamOmitField
	private Action doubleClickAction;
	private AOJungleBuilder aopJungleBuilder;
	public static String[] selectedProjects;
	private boolean playSound;

	public AOPJungleView() {
		playSound = false;
		aopJungleBuilder = new AOJungleBuilder();
	}

	@Override
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		drillDownAdapter = new DrillDownAdapter(viewer);
		viewer.setContentProvider(new ViewContentProvider(getViewSite(), aopJungleBuilder));
		viewer.setLabelProvider(new ViewLabelProvider());
		viewer.setInput(getViewSite());
		viewer.addDoubleClickListener(new DoubleClickListener());

		// Create the help context id for the viewer's control
		PlatformUI.getWorkbench().getHelpSystem().setHelp(viewer.getControl(), "AOPJungle.viewer");
		makeActions();
		hookContextMenu();
		contributeToActionBars();
	}

	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			@Override
			public void menuAboutToShow(IMenuManager manager) {
				AOPJungleView.this.fillContextMenu(manager);
			}
		});
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(actionExportXML);
		manager.add(actionExecuteAstor);
		manager.add(actionExecutePattern);
		manager.add(actionPatternsSettings);
		manager.add(actionExecuteAOPJungle);
		manager.add(actionExecuteQuery);
		manager.add(actionRefreshAOPJungle);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(actionExportXML);
		manager.add(actionExecuteAstor);
		manager.add(actionExecutePattern);
		manager.add(actionPatternsSettings);
		manager.add(actionExecuteAOPJungle);
		manager.add(actionExecuteQuery);
		manager.add(actionRefreshAOPJungle);
		// manager.add(new Separator());
		// drillDownAdapter.addNavigationActions(manager);
		// Other plug-ins can contribute there actions here
		// manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(actionExportXML);
		manager.add(actionExecuteAstor);
		manager.add(actionExecutePattern);
		manager.add(actionPatternsSettings);
		manager.add(actionExecuteAOPJungle);
		manager.add(actionExecuteQuery);
		manager.add(actionRefreshAOPJungle);
		// manager.add(new Separator());
		// drillDownAdapter.addNavigationActions(manager);
	}

	private void makeActions() {
		actionExportXML = new Action() {
			@Override
			public void run() {
				SaveFileFacade saveFile = new SaveFileFacade(new Shell(new Display()));
				String fileName = saveFile.open();
				if (!fileName.isEmpty()) {
					try {
						AOJExporterXML.generate(fileName, AOJWorkspace.getInstance());
						showMessage("All projects were exported to " + fileName);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}
		};
		actionExportXML.setText("XML");
		actionExportXML.setToolTipText("Export to XML");
		actionExportXML.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("downloadxml.gif")));

		actionExecuteAstor = new Action() {
			@Override
			public void run() {
				showCodeSmell();
			}
		};
		actionExecuteAstor.setText("Astor");
		actionExecuteAstor.setToolTipText("Run Astor Tool");
		actionExecuteAstor.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("blue_lambda_24.png")));

		actionPatternsSettings = new Action() {
			@Override
			public void run() {
				ParametersAndWeightsDialog dialog = new ParametersAndWeightsDialog(new Shell());
				dialog.create();
				dialog.open();
			}
		};
		actionPatternsSettings.setText("Patterns Refactoring Settings");
		actionPatternsSettings.setToolTipText("Patterns Refactoring Settings");
		actionPatternsSettings.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("configure.gif")));

		actionExecutePattern = new Action() {
			@Override
			public void run() {
				IProgressService progressService = PlatformUI.getWorkbench().getProgressService();
				try {
					// progressService.runInUI(PlatformUI.getWorkbench().getProgressService(), new
					// IRunnableWithProgress() {
					progressService.busyCursorWhile(new IRunnableWithProgress() {
						public void run(IProgressMonitor monitor) throws InterruptedException {
							AOPJungleMonitor.mainMonitor = SubMonitor.convert(monitor, 100);
							AOPJungleMonitor.mainMonitor.setTaskName("Initializing Patterns Refactoring Plug-in...");
							showRefactoringToPatterns();
						}
					});
				} catch (InvocationTargetException | InterruptedException e) {
					e.printStackTrace();
				}
				try {
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView("Pattern.ResultView");
				} catch (PartInitException e) {
					AOJLogger.getLogger().error("Error on create Pattern Result View : " + e.getMessage());
				}
			}
		};
		actionExecutePattern.setText("Pattern");
		actionExecutePattern.setToolTipText("Run Pattern Tool");
		actionExecutePattern.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("logo-pattern.jpg")));

		actionExecuteQuery = new Action() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				System.out.println("*** Start persistence process *** ");
				AOJProjectDAO projectDAO = new AOJProjectDAO(AOJWorkspace.getInstance());
				projectDAO.saveAll();
				System.out.println("*** Persistence process ended in " + (System.currentTimeMillis() - startTime) / 1000 + " seconds ");
			}
		};
		actionExecuteQuery.setText("Query");
		actionExecuteQuery.setToolTipText("Run Query Tool");
		actionExecuteQuery.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("folders.gif")));

		actionExecuteAOPJungle = new Action() {
			@Override
			public void run() {
				SelectProjectDialog dialog = new SelectProjectDialog(new Shell());
				dialog.create();
				List<IProject> projects = ProjectResources.getInstance().getProjects();
				
				// ordena os projetos por nome
				Collections.sort(projects, new ProjectNameComparator());
				for (IProject project : projects) {
					dialog.getProjectsList().add(project.getName());
				}
				
				if (dialog.open() == Window.OK) {
					playSound = false;
					Timer t = new Timer();
					t.schedule(new TimerTask() {

						@Override
						public void run() {
							playSound = true;

						}
					}, 15000); // 15 segundos

					selectedProjects = dialog.getSelectedProjects();

					IProgressService progressService = PlatformUI.getWorkbench().getProgressService();
					try {
						// progressService.runInUI(PlatformUI.getWorkbench().getProgressService(), new
						// IRunnableWithProgress() {
						progressService.busyCursorWhile(new IRunnableWithProgress() {
							public void run(IProgressMonitor monitor) throws InterruptedException {
								AOPJungleMonitor.mainMonitor = SubMonitor.convert(monitor, 100);
								AOPJungleMonitor.mainMonitor.setTaskName("Initializing plugin AOP Jungle...");
								runAOPJungle();
							}
						});
					} catch (InvocationTargetException | InterruptedException e) {
						e.printStackTrace();
					}
					refreshAOPJungleView();
					if (playSound)
						playSound();
				}
			}
		};
		actionExecuteAOPJungle.setText("AOPJungle");
		actionExecuteAOPJungle.setToolTipText("Executar AOPJungle");
		actionExecuteAOPJungle.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("execution_obj.gif")));

		actionRefreshAOPJungle = new Action() {
			@Override
			public void run() {
				refreshAOPJungleView();
			}
		};
		actionRefreshAOPJungle.setText("Atualizar AOPJungle");
		actionRefreshAOPJungle.setToolTipText("Atualizar AOPJungle");
		actionRefreshAOPJungle.setImageDescriptor(ImageDescriptor.createFromImage(IconProvider.getIcon("refresh.png")));

		// PlatformUI.getWorkbench().getSharedImages().
		// getImageDescriptor(ISharedImages.IMG_LCL_LINKTO_HELP));
	}

	private void showMessage(String message) {
		MessageDialog.openInformation(viewer.getControl().getShell(), "AOP Jungle View", message);
	}

	private void showCodeSmell() {
		AstorSession badSmellEngine = AstorSession.getInstance();
		badSmellEngine.clearEngines();
		// badSmellEngine.registerEngine(new DivergentChanges());
		// badSmellEngine.registerEngine(new LargeAspect());
		// badSmellEngine.registerEngine(new LargePointcut());
		// badSmellEngine.registerEngine(new LargeAdvice());
		// badSmellEngine.registerEngine(new AbstractMethodIntroduction());
		// badSmellEngine.registerEngine(new AnonymousPointcutDefinition());
		// badSmellEngine.registerEngine(new SpeculativeGenerality());
		// badSmellEngine.registerEngine(new AdviceCodeDuplication());
		// badSmellEngine.registerEngine(new FeatureEnvy());
		// badSmellEngine.registerEngine(new LazyAspect());
		// badSmellEngine.registerEngine(new DoublePersonality());
		badSmellEngine.registerEngine(new EncloseFunctionalInterface());
		badSmellEngine.registerEngine(new EncloseEnhancedFor());
		badSmellEngine.registerEngine(new EncloseSort());
		badSmellEngine.registerEngine(new EncloseForWithIfToFilter());
		badSmellEngine.registerEngine(new ConvertFunctionalInterfaceToDefaultFunctionalInterface());
		badSmellEngine.registerEngine(new ConvertInterfaceToDefaultMethod());
		badSmellEngine.registerEngine(new ConvertITMDtoDefaultMethod());

		badSmellEngine.run();
		badSmellEngine.printReport();
		try {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView("Astor.ResultView");
		} catch (PartInitException e) {
			AOJLogger.getLogger().error("Error on create Code Smell Result View : " + e.getMessage());
		}
	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	private void runAOPJungle() {
		try {
			aopJungleBuilder.getWorkspace().loadProjects(AOJBindingOptionEnum.BINDING_ON);
		} catch (JavaModelException e) {
			e.printStackTrace();
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	private void refreshAOPJungleView() {
		ViewContentProvider vcp = new ViewContentProvider(getViewSite(), aopJungleBuilder);
		viewer.setContentProvider(vcp);
		viewer.expandToLevel(2);
	}

	private void showRefactoringToPatterns() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) {
			// corrige as referÍncias das superclasses e subclasses
			AOJInheritanceResolver resolver = new AOJInheritanceResolver(project);
			resolver.resolve();
		}

		AstorSession badSmellEngine = AstorSession.getInstance();
		badSmellEngine.clearEngines();

		if (PRProp.getECWFExecute())
			badSmellEngine.registerEngine(new EncapsulateClassWithFactory());
		if (PRProp.getRCDWCExecute())
			badSmellEngine.registerEngine(new ReplaceConditionalDispatcherWithCommand());
		if (PRProp.getRITWCExecute())
			badSmellEngine.registerEngine(new ReplaceImplicitTreeWithComposite());

		AOPJungleMonitor.smellEnginesMonitor = AOPJungleMonitor.mainMonitor.split(100).setWorkRemaining(badSmellEngine.getRegisteredEngines().size());
		badSmellEngine.run();
		// badSmellEngine.printReport();

		System.out.println(Printer.OUTPUT);
	}

	public synchronized void playSound() {
		new Thread(new Runnable() {
			// The wrapper thread is unnecessary, unless it blocks on the
			// Clip finishing; see comments.
			public void run() {
				try {
					Clip clip = AudioSystem.getClip();
					AudioInputStream inputStream = AudioSystem.getAudioInputStream(new File("C:\\Windows\\media\\Ring02.wav"));
					clip.open(inputStream);
					clip.start();
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
			}
		}).start();
	}
	
	class ProjectNameComparator implements Comparator<IProject> {

		@Override
		public int compare(IProject o1, IProject o2) {
			return o1.getName().compareToIgnoreCase(o2.getName());
		}
		
	}

}