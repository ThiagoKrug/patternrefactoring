package br.ufsm.aopjungle.views;

import org.eclipse.core.runtime.SubMonitor;

public class AOPJungleMonitor {
	public static SubMonitor mainMonitor;
	public static SubMonitor projectsMonitor;
	public static SubMonitor projectMonitor;
	public static SubMonitor packagesMonitor;
	public static SubMonitor compilationUnitsMonitor;
	
	public static SubMonitor smellEnginesMonitor;
	public static SubMonitor smellEngineMonitor;
}

