package br.ufsm.aopjungle.exception;

@SuppressWarnings("serial")
public class StackObjectReferenceIllegalStateException extends Exception {
	public StackObjectReferenceIllegalStateException(String message) {
		super(message);
	}
}
