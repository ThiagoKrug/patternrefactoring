package br.ufsm.aopjungle.exception;

@SuppressWarnings("serial")
public class IllegalReferenceMapState extends Exception {
	public IllegalReferenceMapState(String message) {
		super(message);
	}
}
