package br.ufsm.aopjungle.exception;

@SuppressWarnings("serial")
public class AOJQueryFailException extends Exception {

	public AOJQueryFailException (Throwable arg0) {
		super(arg0);
	}
}
