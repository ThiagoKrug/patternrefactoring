package br.ufsm.aopjungle.exception;

@SuppressWarnings("serial")
public class AOJCompilerException extends Exception {
	public AOJCompilerException(String message) {
		super(message);
	}
}
