package br.ufsm.aopjungle.labs;

import java.util.Hashtable;

import br.ufsm.aopjungle.metamodel.commons.AOJContainer;

public class AOJBindingMapping {
	
	/**
	 * Keeps <fullQualifiedName, object> for binding resolver 
	 */
	private Hashtable<String, AOJContainer> containerTable;
	
	public AOJBindingMapping() {
		containerTable = new Hashtable<String, AOJContainer>();
	}
	
	public void put (String fullQualifiedName, AOJContainer type) {
		containerTable.put(fullQualifiedName,  type);
	}
	
	public AOJContainer get (String key) {
		return containerTable.get(key);
	}
	
	public boolean containsKey (String key) {
		return containerTable.containsKey(key);
	}
	
	public boolean containsValue (AOJContainer value) {
		return containerTable.containsValue(value);
	}
}
