package br.ufsm.aopjungle.util;

public class AOJPairValue<L, R> {
	private L l;
	private R r;
	
	public AOJPairValue(L l, R r) {
		put (l, r);
	}
	
	public void put (L l, R r) {
		this.l = l;
		this.r = r;		
	}

	public L getL() {
		return l;
	}

	public void setL(L l) {
		this.l = l;
	}

	public R getR() {
		return r;
	}

	public void setR(R r) {
		this.r = r;
	}

	
}
