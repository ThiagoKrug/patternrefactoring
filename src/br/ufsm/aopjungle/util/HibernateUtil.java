package br.ufsm.aopjungle.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import br.ufsm.aopjungle.log.AOJLogger;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceAfter;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceAfterReturning;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceAfterThrowing;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceAround;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAdviceBefore;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectMember;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareCompilationEnforcement;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareField;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareMethod;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareParents;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclarePrecedence;
import br.ufsm.aopjungle.metamodel.aspectj.AOJDeclareSoft;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutCFlow;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutDeclaration;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutExpression;
import br.ufsm.aopjungle.metamodel.aspectj.AOJPointcutPrimitive;
import br.ufsm.aopjungle.metamodel.commons.AOJAnnotationDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJAttributeDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJCommonContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJCommonTypeMember;
import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJConcreteTypeMember;
import br.ufsm.aopjungle.metamodel.commons.AOJContainerPlaceHolder;
import br.ufsm.aopjungle.metamodel.commons.AOJException;
import br.ufsm.aopjungle.metamodel.commons.AOJImportDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJMethodDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJModifier;
import br.ufsm.aopjungle.metamodel.commons.AOJPackageDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJParameter;
import br.ufsm.aopjungle.metamodel.commons.AOJPrimitiveType;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.java.AOJAnonymousClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;

public class HibernateUtil {
	private static final SessionFactory sessionFactory;
	
	static {
		try {
			sessionFactory = new Configuration().addAnnotatedClass(AOJProject.class).configure()
					                            .addAnnotatedClass(AOJPackageDeclaration.class).configure()
					                            .addAnnotatedClass(AOJCompilationUnit.class).configure()
					                            .addAnnotatedClass(AOJImportDeclaration.class).configure()
					                            .addAnnotatedClass(AOJClassDeclaration.class).configure()
					                            .addAnnotatedClass(AOJAnonymousClassDeclaration.class).configure()
					                            .addAnnotatedClass(AOJInterfaceDeclaration.class).configure()
					                            .addAnnotatedClass(AOJAspectDeclaration.class).configure()
					                            .addAnnotatedClass(AOJMethodDeclaration.class).configure()
					                            .addAnnotatedClass(AOJConstructorDeclaration.class).configure()
					                            .addAnnotatedClass(AOJAttributeDeclaration.class).configure()
					                            .addAnnotatedClass(AOJAnnotationDeclaration.class).configure()					                            
					                            .addAnnotatedClass(AOJModifier.class).configure()					                            
					                            .addAnnotatedClass(AOJImportDeclaration.class).configure()	
					                            .addAnnotatedClass(AOJAdviceAfter.class).configure()	
					                            .addAnnotatedClass(AOJAdviceAfterReturning.class).configure()	
					                            .addAnnotatedClass(AOJAdviceAfterThrowing.class).configure()	
					                            .addAnnotatedClass(AOJAdviceAround.class).configure()	
					                            .addAnnotatedClass(AOJAdviceBefore.class).configure()	
					                            .addAnnotatedClass(AOJDeclareCompilationEnforcement.class).configure()	
					                            .addAnnotatedClass(AOJDeclareField.class).configure()	
					                            .addAnnotatedClass(AOJDeclareMethod.class).configure()	
					                            .addAnnotatedClass(AOJDeclareParents.class).configure()	
					                            .addAnnotatedClass(AOJDeclarePrecedence.class).configure()	
					                            .addAnnotatedClass(AOJDeclareSoft.class).configure()	
					                            .addAnnotatedClass(AOJPointcutCFlow.class).configure()	
					                            .addAnnotatedClass(AOJPointcutDeclaration.class).configure()	
					                            .addAnnotatedClass(AOJPointcutExpression.class).configure()	
					                            .addAnnotatedClass(AOJPointcutPrimitive.class).configure()	
					                            .addAnnotatedClass(AOJReferencedType.class).configure()	
					                            .addAnnotatedClass(AOJPrimitiveType.class).configure()	
					                            .addAnnotatedClass(AOJException.class).configure()	
					                            .addAnnotatedClass(AOJParameter.class).configure()	
					                            .addAnnotatedClass(AOJCommonTypeMember.class).configure()	
					                            .addAnnotatedClass(AOJConcreteTypeMember.class).configure()	
					                            .addAnnotatedClass(AOJAspectMember.class).configure()	
					                            .addAnnotatedClass(AOJCommonContainer.class).configure()	
					                            .addAnnotatedClass(AOJContainerPlaceHolder.class).configure()	
					                            .buildSessionFactory();
		} catch (Throwable ex) {
			AOJLogger.getLogger().error("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
