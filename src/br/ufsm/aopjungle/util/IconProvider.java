package br.ufsm.aopjungle.util;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public class IconProvider {
	private static final String appPath = IconProvider.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	private static final String DEFAULTPATH = appPath + "icons/";
	private static String filePath = DEFAULTPATH + "defaulticon.jpg";

	private static Display getDisplay() {
		Display display = Display.getCurrent();
		if (display == null)
			display = Display.getDefault();
		return display;
	}

	public static Image getIcon(String fileName) {
		if (fileName.length() == 0)
			throw new IllegalArgumentException("Empty icon file name was found");
		filePath = DEFAULTPATH + fileName;
		Image icon = new Image(getDisplay(), filePath);

		return icon;
	}

	public static ImageDescriptor getImageDescriptor(String name) {
		try {
			filePath = "file:" + DEFAULTPATH + name;
			URL url = new URL(filePath);
			return ImageDescriptor.createFromURL(url);
		} catch (MalformedURLException e) {
			// should not happen
			e.printStackTrace();
			return ImageDescriptor.getMissingImageDescriptor();
		}
	}
}
