package br.ufsm.aopjungle.metamodel.commons;

import org.aspectj.org.eclipse.jdt.core.dom.Expression;
import org.aspectj.org.eclipse.jdt.core.dom.Type;
import org.aspectj.org.eclipse.jdt.core.dom.TypeLiteral;

/**
 * Representa a espress�o ( Type | void ) . class
 * 
 * @author thiag
 *
 */
public class AOJTypeLiteral extends AOJExpression {

	private Type type;

	public AOJTypeLiteral(AOJProgramElement owner, Expression expression) {
		super(owner, expression.toString());
		type = ((TypeLiteral)expression).getType();
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

}
