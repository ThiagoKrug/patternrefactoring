package br.ufsm.aopjungle.metamodel.commons;

import org.aspectj.org.eclipse.jdt.core.dom.Assignment;
import org.aspectj.org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.aspectj.org.eclipse.jdt.core.dom.Expression;

import br.ufsm.aopjungle.views.Printer;

public class AOJAssignment extends AOJExpression {

	private Expression leftHandSide;
	private Object rightHandSide;
	private String identifier = "unknown";

	public AOJAssignment(AOJProgramElement owner, Expression expression) {		
		super(owner, expression.toString());
		Assignment assignment = (Assignment)expression;
		if (assignment.getLeftHandSide() != null)
			this.leftHandSide = assignment.getLeftHandSide();
		if (assignment.getRightHandSide() != null)
			if (assignment.getRightHandSide() instanceof ClassInstanceCreation) {
				Printer.addOutput("\n    ClassInstanceCreation");
				Printer.addOutput("\n    " + assignment.getRightHandSide().toString());
				this.rightHandSide = new AOJClassInstanceCreation(owner, assignment.getRightHandSide());
			} else {
				this.rightHandSide = assignment.getRightHandSide();
			}
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Expression getLeftHandSide() {
		return leftHandSide;
	}

	public void setLeftHandSide(Expression leftHandSide) {
		this.leftHandSide = leftHandSide;
	}

	public Object getRightHandSide() {
		return rightHandSide;
	}

	public void setRightHandSide(Object rightHandSide) {
		this.rightHandSide = rightHandSide;
	}

}



