package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

//import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

@MappedSuperclass
public abstract class ASTElement extends AOJProgramElement {
	@XStreamOmitField
	@Transient
	private org.aspectj.org.eclipse.jdt.core.dom.ASTNode node;
	@XStreamOmitField
	@Transient
	private org.aspectj.org.eclipse.jdt.internal.compiler.ast.ASTNode internalNode;
	private int hash;
	@Transient
	private AOJCompilationUnit compilationUnit;

	public org.aspectj.org.eclipse.jdt.core.dom.ASTNode getNode() {
		return node;
	}

	public void setNode(org.aspectj.org.eclipse.jdt.core.dom.ASTNode node) {
		this.node = node;
	}
	
	protected ASTElement() {
		
	}
	
	public ASTElement(org.aspectj.org.eclipse.jdt.core.dom.ASTNode node, AOJProgramElement owner) {
		super(owner);
		this.node = node;
		setCompilationUnit();
		if (null != node)
			setHash(node.hashCode());
	}

	/**
	 * A facility to find the compilation unit this node is owned to
	 */
	private void setCompilationUnit() {
		compilationUnit = resolveCompilationUnit(getOwner());
	}

	private AOJCompilationUnit resolveCompilationUnit (AOJProgramElement owner) {
		if ( owner == null) 
			return null;
		if (owner instanceof AOJCompilationUnit)
			return (AOJCompilationUnit)owner;
		return resolveCompilationUnit(owner.getOwner());
	}
	
	public int getHash() {
		return hash;
	}

	private void setHash(int hash) {
		this.hash = hash;
	}

	public AOJCompilationUnit getCompilationUnit() {
		return compilationUnit;
	}

	public org.aspectj.org.eclipse.jdt.internal.compiler.ast.ASTNode getInternalNode() {
		return internalNode;
	}

	public void setInternalNode(org.aspectj.org.eclipse.jdt.internal.compiler.ast.ASTNode internalNode) {
		this.internalNode = internalNode;
	}
}
