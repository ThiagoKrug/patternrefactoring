package br.ufsm.aopjungle.metamodel.commons;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.aspectj.org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import br.ufsm.aopjungle.views.Printer;

public class AOJVariableDeclarationStatement extends AOJStatement {

	private AOJExpression aojExpression;
	private AOJExpression aojInitializer;
	
	public AOJVariableDeclarationStatement(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		VariableDeclarationFragment e = (VariableDeclarationFragment)node;
		
		Printer.addOutput("\n\nVariableDeclarationStatement");
		Printer.addOutput("\n" + node.toString());
		if (e.getInitializer() instanceof ClassInstanceCreation) {
			Printer.addOutput("\n  ClassInstanceCreation");
			Printer.addOutput("\n  " + e.getInitializer().toString());
			this.aojInitializer = new AOJClassInstanceCreation(owner, e.getInitializer());
		}
		this.aojExpression = new AOJExpression(owner, node.toString()) {	};
	}

	public AOJExpression getAojExpression() {
		return aojExpression;
	}

	public void setAojExpression(AOJExpression aojExpression) {
		this.aojExpression = aojExpression;
	}

	public AOJExpression getAojInitializer() {
		return aojInitializer;
	}

	public void setAojInitializer(AOJExpression aojInitializer) {
		this.aojInitializer = aojInitializer;
	}

}



