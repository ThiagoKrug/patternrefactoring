package br.ufsm.aopjungle.metamodel.commons;


/**
 * @author  defaveri
 */
public enum AOJModifierEnum {
	PUBLIC,
	PROTECTED,
	PRIVATE,
	ABSTRACT,
	STATIC,
	FINAL,
	TRANSIENT,
	VOLATILE,
	SYNCHRONIZED,
	NATIVE,
	STRICTFP,
	INTERFACE;
	
	private String name;

	private AOJModifierEnum() {
		this.setName(toString());
	}
						
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
