package br.ufsm.aopjungle.metamodel.commons;

import java.util.List;

public interface AOJAnnotable {
	public List<AOJAnnotationDeclaration> getAnnotations();
}
