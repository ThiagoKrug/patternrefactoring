package br.ufsm.aopjungle.metamodel.commons;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.aspectj.org.eclipse.jdt.internal.compiler.classfmt.ClassFileConstants;

@Entity
public class AOJModifier {
	@Id
    @GeneratedValue
	private long id;

	private int code;
	
	@ElementCollection(targetClass=AOJModifierEnum.class)
    @Enumerated(EnumType.STRING) 
    @CollectionTable(name="modifier_type")
    @Column(name="type") 
	private List<AOJModifierEnum> descriptors;
	
	public String getMetaName() {
		return "Modifier";
	}
	
	public List<String> getNames() {
		List<String> result = new ArrayList<String>();
		for (AOJModifierEnum modifier : descriptors) 
			result.add(modifier.toString());
		
		return result;
	}
	
	public boolean exists(AOJModifierEnum modifier) {
		return descriptors.indexOf(modifier) == -1 ? false : true; 
	}
	
	public void loadModifiers(int modifiers) {
		code = modifiers;
		this.descriptors = new ArrayList<AOJModifierEnum>();
		StringTokenizer st = new StringTokenizer(Modifier.toString(code));
		while (st.hasMoreTokens()) 
			this.descriptors.add(AOJModifierEnum.valueOf(st.nextToken().toUpperCase()));
	}
	
	protected AOJModifier() {
		
	}
	
	public AOJModifier(int modifiers) {
		loadModifiers(modifiers);
	}

	public List<AOJModifierEnum> getModifiers() {
		return descriptors;
	}

	public void setModifiers(List<AOJModifierEnum> modifiers) {
		this.descriptors = modifiers;
	}
	
	public void add(AOJModifierEnum modifier) {
		descriptors.add(modifier);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public StringBuffer print(StringBuffer output) {
		if ((code & ClassFileConstants.AccPublic) != 0)
			output.append("public "); //$NON-NLS-1$
		if ((code & ClassFileConstants.AccPrivate) != 0)
			output.append("private "); //$NON-NLS-1$
		if ((code & ClassFileConstants.AccProtected) != 0)
			output.append("protected "); //$NON-NLS-1$
		if ((code & ClassFileConstants.AccStatic) != 0)
			output.append("static "); //$NON-NLS-1$
		if ((code & ClassFileConstants.AccFinal) != 0)
			output.append("final "); //$NON-NLS-1$
		if ((code & ClassFileConstants.AccSynchronized) != 0)
			output.append("synchronized "); //$NON-NLS-1$
		if ((code & ClassFileConstants.AccVolatile) != 0)
			output.append("volatile "); //$NON-NLS-1$
		if ((code & ClassFileConstants.AccTransient) != 0)
			output.append("transient "); //$NON-NLS-1$
		if ((code & ClassFileConstants.AccNative) != 0)
			output.append("native "); //$NON-NLS-1$
		if ((code & ClassFileConstants.AccAbstract) != 0)
			output.append("abstract "); //$NON-NLS-1$
		return output;		
	}
	
}
