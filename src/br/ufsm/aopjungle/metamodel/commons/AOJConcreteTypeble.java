package br.ufsm.aopjungle.metamodel.commons;

import java.util.List;

import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;

public interface AOJConcreteTypeble extends AOJTypeble {
	public List<AOJReferencedType> getImplementedInterfaces();
	public List<AOJConstructorDeclaration> getConstructors();
}
