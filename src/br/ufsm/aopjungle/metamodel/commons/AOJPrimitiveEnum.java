package br.ufsm.aopjungle.metamodel.commons;

/**
 * @author  defaveri
 */
public enum AOJPrimitiveEnum { 
BYTE, 
SHORT,
INT, 
LONG, 
FLOAT, 
DOUBLE, 
BOOLEAN, 
CHAR;

private String name;
							
	private AOJPrimitiveEnum() {
		this.setName(toString());
	}
							
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}							
}
