package br.ufsm.aopjungle.metamodel.commons;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.swing.plaf.synth.SynthSeparatorUI;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.AjTypeDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.ArrayType;
import org.aspectj.org.eclipse.jdt.core.dom.ParameterizedType;
import org.aspectj.org.eclipse.jdt.core.dom.PrimitiveType;
import org.aspectj.org.eclipse.jdt.core.dom.QualifiedType;
import org.aspectj.org.eclipse.jdt.core.dom.SimpleType;
import org.aspectj.org.eclipse.jdt.core.dom.Type;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.MetaValue;

import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJInterfaceDeclaration;
import br.ufsm.aopjungle.metrics.AOJMetrics;
import br.ufsm.aopjungle.metrics.AOJTypeMetrics;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 *         Federal University of Santa Maria <br>
 *         Programming Languages and Database Research Group <br>
 */
@MappedSuperclass
public abstract class AOJTypeDeclaration extends AOJMember implements AOJContainer, AOJAnnotable, AOJMetrics {
	private String fullQualifiedName;
	/**
	 * Back track package for query facilities of pack and project
	 */
	@OneToOne(cascade = CascadeType.ALL)
	private AOJPackageDeclaration pack;
	/**
	 * Parent represents the original node
	 * 
	 * @see use superType() to get the extends type of this type
	 */
	@Transient
	private AOJReferencedType parent;

	/**
	 * A normalized superType represented by AOJContainer
	 */
	@Any(metaColumn = @Column(name = "super_type"), fetch = FetchType.EAGER)
	@AnyMetaDef(idType = "long", metaType = "string", metaValues = { @MetaValue(value = "ASP", targetEntity = AOJAspectDeclaration.class),
			@MetaValue(value = "INT", targetEntity = AOJInterfaceDeclaration.class), @MetaValue(value = "CLA", targetEntity = AOJClassDeclaration.class),
			@MetaValue(value = "PHL", targetEntity = AOJContainerPlaceHolder.class) })
	@JoinColumn(name = "super_id")
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private AOJContainer superType;

	protected abstract void loadMembers();

	public abstract String getASTTypeName();

	public abstract String getASTTypeFullQualifiedName();

	// added by Thiago
	private List<AOJContainer> subTypes;
	private boolean print;

	public List<AOJContainer> getSubTypes() {
		return subTypes;
	}

	public void setSubTypes(List<AOJContainer> subTypes) {
		this.subTypes = subTypes;
	}

	@Override
	public boolean addSubType(AOJContainer e) {
		return this.subTypes.add(e);
	}
	// end

	@Override
	public void loadHandler() {
		setInternalHandler(getFullQualifiedName());
	}

	@Override
	public void loadMetrics() {
		long nOfLines = 0;// getMetrics().getNumberOfLines();
		List<AOJMetrics> members = new ArrayList<AOJMetrics>();
		getMembers().getMetricParticipants(members);
		for (AOJMetrics metricParticipant : members)
			nOfLines += metricParticipant.getMetrics().getNumberOfLines();

		nOfLines += 3; // Add open/close curly brackets + header
		getMetrics().setNumberOfLines(nOfLines);

		long nOfMethods = getMetrics().getNumberOfMethods() + getMembers().getMethods().size();
		long nOfFields = getMetrics().getNumberOfFields() + getMembers().getAttributes().size();

		getMetrics().setNumberOfMethods(nOfMethods);
		getMetrics().setNumberOfFields(nOfFields);
	}

	public AOJTypeDeclaration(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		fullQualifiedName = getASTTypeFullQualifiedName();
		// binding = AOJBindingFactory.eINSTANCE.createBinding();
		parent = null;
		superType = null;
		subTypes = new ArrayList<>();

		/*if (fullQualifiedName.compareTo("org.eevolution.form.action.ZoomMenuAction") == 0)
			print = true;
		else
			print = false;*/

		loadName();
		loadParent();
		loadSuperType();
		loadMembers();
		loadPack();
		loadHandler();
		loadToReferenceMap();
	}

	/*
	 * private void loadSuperType() { if (getNode() instanceof AjTypeDeclaration) {
	 * Type type = ((AjTypeDeclaration)getNode()).getSuperclassType(); setSuperType(
	 * new AOJContainerPlaceHolder(getFullyQualifiedName(type), type, this) ); } }
	 */

	private void loadSuperType() {
		// print = false;
		if (getNode() instanceof AjTypeDeclaration) {
			Type type = ((AjTypeDeclaration) getNode()).getSuperclassType();
			setSuperType(new AOJContainerPlaceHolder(getFullyQualifiedName(type), type, this));

			//if (print) {
				//System.out.println("\nNode: " + getNode());
				//System.out.println("SuperType: " + type + "\n");
			//}
		}
	}

	protected void loadName() {
		setName(getASTTypeName());
	}

	private void loadPack() {
		pack = getOwner() instanceof AOJCompilationUnit ? ((AOJCompilationUnit) getOwner()).getPackage() : null;
	}

	@Override
	abstract public AOJTypeMetrics getMetrics();

	protected AOJTypeDeclaration() {

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fullQualifiedName == null) ? 0 : fullQualifiedName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AOJTypeDeclaration))
			return false;
		AOJTypeDeclaration other = (AOJTypeDeclaration) obj;
		if (fullQualifiedName == null) {
			if (other.fullQualifiedName != null)
				return false;
		} else if (!fullQualifiedName.equals(other.fullQualifiedName))
			return false;
		return true;
	}

	protected abstract void loadParent();

	@Override
	public String getMetaName() {
		return "AbstractType";
	}

	@Override
	public AOJReferencedType getParent() {
		return parent;
	}

	public void setParent(AOJReferencedType parent) {
		// parent.addNumberOfDescendents(1);
		this.parent = parent;
		// findNumberOfParents(parent);
	}

	@Override
	public AOJContainer getSuperType() {
		return superType;
	}

	@Override
	public void setSuperType(AOJContainer superType) {
		this.superType = superType;
	}

	@Override
	public String getFullQualifiedName() {
		return fullQualifiedName;
	}

	public void setFullQualifiedName(String name) {
		fullQualifiedName = name;
	}

	private String getFullyQualifiedName(ASTNode node) {
		try {
			if (null == node)
				return "java.lang.Object";
			if (node instanceof PrimitiveType)
				return ((PrimitiveType) node).toString();
			if (node instanceof SimpleType)
				return getFullyQualifiedNameFromImport(((SimpleType) node).getName().toString());
			if (node instanceof QualifiedType)
				return ((QualifiedType) node).getName().toString();
			if (node instanceof ArrayType)
				return ((ArrayType) node).getElementType().toString() + getBracket(((ArrayType) node).getDimensions());
			if (node instanceof ParameterizedType) {
				if (((ParameterizedType) node).getType() instanceof SimpleType)
					return getFullyQualifiedNameFromImport(((SimpleType) ((ParameterizedType) node).getType()).getName().toString());
			}
		} catch (Exception e) {
			// d� erro porque n�o consigo buscar as classes internas
			// e.printStackTrace();
			//if (print)
				//System.out.println(e.getMessage());
		}
		return "Unknown";
	}

	private String getBracket(int dimensions) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < dimensions; i++)
			sb.append("[]");
		return sb.toString();
	}

	private String getFullyQualifiedNameFromImport(String name) throws Exception {
		try {
			// se � uma chamada direta � classe (ex: ... extends org.eclipse.Classe {)
			if (fileExists(name))
				return name;
		} catch (Exception e) {
			e.printStackTrace();
		}
		// se � uma chamada direta � classe (ex: ... extends org.eclipse.Classe {) mas
		// pertence a uma classe de fora do projeto
		if (forClass(name))
			return name;

		String fullQualifiedName;
		AOJCompilationUnit cu = getCompilationUnit(getOwner());
		if (null != cu) {
			// pega os imports do arquivo java
			for (AOJImportDeclaration aojImportDeclaration : cu.getImportsDeclaration()) {
				// se o type da cria��o estiver no import
				if (aojImportDeclaration.getClassName().equals(name)) {
					return aojImportDeclaration.qualifiedName();
				} else {
					// caso esteja em um "*"
					fullQualifiedName = this.findByWildcard(aojImportDeclaration, name);
					if (fullQualifiedName.isEmpty() == false) {
						return fullQualifiedName;
					}
				}
			}
		}

		// se faz parte do java.lang
		fullQualifiedName = findAtJavaLang(name);
		if (!fullQualifiedName.isEmpty())
			return fullQualifiedName;

		// If no type was found, the extends type is located at the same package
		fullQualifiedName = getPackage().getName() + "." + name;
		try {
			if (fileExists(fullQualifiedName)) {
				return fullQualifiedName;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// return getPackage().getName() + "." + name;
		throw new Exception("Supertipo " + name + " n�o encontrado!");
	}

	private String findAtJavaLang(String name) {
		String fullQualifiedName = "java.lang." + name;
		if (forClass(fullQualifiedName))
			return fullQualifiedName;
		return "";
	}

	private String findByWildcard(AOJImportDeclaration imp, String name) {
		if (imp.getCode().indexOf("*") != -1) { // there's not * on import declaration
			String fullQualifiedName = imp.qualifiedName().replace("*", name); // replace * by type name
			if (forClass(fullQualifiedName)) { // try to find by classloader
				return fullQualifiedName;
			}
			try {
				if (fileExists(fullQualifiedName)) {
					return fullQualifiedName;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "";
	}

	private boolean fileExists(String fullQualifiedName) {// throws Exception {
		// if
		// (getProject().getResourceProject().isNatureEnabled("org.eclipse.m2e.core.maven2Nature"))
		// {
		// } else if
		// (getProject().getResourceProject().isNatureEnabled("org.eclipse.jdt.core.javanature"))
		// {

		//if (print) {
			IJavaProject project = JavaCore.create(getProject().getResourceProject());
			try {
				for (IPackageFragmentRoot src : project.getAllPackageFragmentRoots()) {
					if (src.getKind() == IPackageFragmentRoot.K_SOURCE) {
						// remove o caminho da pasta do projeto
						IPath path = src.getPath().removeFirstSegments(1);
						String filePath = path + "/" + fullQualifiedName.replace(".", "/") + ".java";
						//System.out.println(filePath);
						
						IFile file = getProject().getResourceProject().getFile(filePath);
						if (file.exists()) {
							return true;
						}
					}
				}
			} catch (JavaModelException e) {
				e.printStackTrace();
			}
		//}

		// se for maven correto
		IFile file = getProject().getResourceProject().getFile("/src/main/java/" + fullQualifiedName.replace(".", "/") + ".java");
		if (file.exists()) {
			return true;
		}

		// se funcionar blz
		if (forClass(fullQualifiedName)) {
			return true;
		}

		// pode ser um maven configurado errado
		IFile file2 = getProject().getResourceProject().getFile("/src/" + fullQualifiedName.replace(".", "/") + ".java");
		if (file2.exists()) {
			return true;
		}

		return false;
		// throw new Exception("Natureza n�o dispon�vel");
	}

	private boolean forClass(String fullQualifiedName) {
		try {
			Class.forName(fullQualifiedName);
		} catch (ClassNotFoundException e) {
			return false;
		}
		return true;
	}

	private AOJCompilationUnit getCompilationUnit(AOJProgramElement owner) {
		if (owner instanceof AOJCompilationUnit)
			return (AOJCompilationUnit) owner;
		if (owner instanceof AOJProject)
			return null;
		return getCompilationUnit(owner.getOwner());
	}
}