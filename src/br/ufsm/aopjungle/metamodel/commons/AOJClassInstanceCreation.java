package br.ufsm.aopjungle.metamodel.commons;

import org.aspectj.org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.aspectj.org.eclipse.jdt.core.dom.Expression;
import org.aspectj.org.eclipse.jdt.core.dom.Type;

public class AOJClassInstanceCreation extends AOJExpression {
		
	private Type type;
	private String identifier = "unknown";
	
	public AOJClassInstanceCreation(AOJProgramElement owner, Expression expression) {		
		super(owner, expression.toString());
		ClassInstanceCreation cic = (ClassInstanceCreation)expression;
		if (cic.getType() != null)
			this.setType(cic.getType());
		if (cic.getExpression() != null)
			this.setIdentifier(cic.getExpression().toString());
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

}



