package br.ufsm.aopjungle.metamodel.commons;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import br.ufsm.aopjungle.metrics.AOJMetrics;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
public class AOJCommonTypeMember {
	@Id 
	@GeneratedValue
	//@SequenceGenerator(name = "seqGenerator", sequenceName = "SEQ_AOPJUNGLE")
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGenerator")
	private long id;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJMethodDeclaration> methods;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJAttributeDeclaration> attributes;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJAnnotationDeclaration> annotations;
	
	public AOJCommonTypeMember() {
		
	}
	
	public List<AOJMethodDeclaration> getMethods() {
		if (methods == null)
			methods = new ArrayList<AOJMethodDeclaration>();
		return methods;
	}

	public List<AOJAttributeDeclaration> getAttributes() {
		if (attributes == null)
			attributes = new ArrayList<AOJAttributeDeclaration>();
		return attributes;
	}

	public List<AOJAnnotationDeclaration> getAnnotations() {
		if (annotations == null)
			annotations = new ArrayList<AOJAnnotationDeclaration>();		
		return annotations;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public void getMetricParticipants (List<AOJMetrics> members) {
		for (AOJMethodDeclaration member : getMethods()) 
			if (member instanceof AOJMetrics)
				members.add(member);
		
		for (AOJAttributeDeclaration member : getAttributes()) 
			if (member instanceof AOJMetrics)
				members.add(member);

		for (AOJAnnotationDeclaration member : getAnnotations()) 
			if (member instanceof AOJMetrics)
				members.add(member);
	}
	
}