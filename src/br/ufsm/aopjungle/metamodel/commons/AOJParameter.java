package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.Modifier;
import org.aspectj.org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.MetaValue;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
@ForeignKey(name="parameter_fk")
public class AOJParameter extends ASTElement {
	@XStreamOmitField
	@Transient
	SingleVariableDeclaration parameterDeclaration = (SingleVariableDeclaration)getNode();
	
	private boolean isFinal;
	private boolean isQualifiedType;
	private boolean isArrayType;
	private boolean isVarargs;
	private boolean isPrimitiveType;
	private boolean isParametrizedType;
	private boolean isSimpleType;
	private boolean isWildcardType;

	@Any(metaColumn = @Column(name = "detail_type"))
	@AnyMetaDef(idType = "long", metaType = "string", metaValues = {
	@MetaValue(value = "R", targetEntity = AOJReferencedType.class),
	@MetaValue(value = "P", targetEntity = AOJPrimitiveType.class) })
	@JoinColumn(name = "detail_id")
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	private AOJType type;

	protected AOJParameter() {

	}
	
	public AOJParameter(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		setName();
		setFinal();
		setVarargs();
		setType();
		setQualifiedType();
		setArrayType();
		setPrimitiveType();
		setParametrizedType();
		setSimpleType();
		setWildcardType();
		
	}	
	
    private void setVarargs() {
    	this.isVarargs = parameterDeclaration.isVarargs();
	}

	private void setType() {
		if (parameterDeclaration.getType().isPrimitiveType())
			this.type = new AOJPrimitiveType(parameterDeclaration.getType(), this);
		else
			this.type = new AOJReferencedType(parameterDeclaration.getType(), this);
	}

	private void setWildcardType() {
		this.isWildcardType = parameterDeclaration.getType().isWildcardType();
	}

	private void setPrimitiveType() {
		this.isPrimitiveType = parameterDeclaration.getType().isPrimitiveType();
	}

	private void setName() {
		super.setName(parameterDeclaration.getName().toString());
	}

	private void setFinal() {
		this.isFinal = Modifier.isFinal(parameterDeclaration.getModifiers());
	}

	/**
	 * @return
	 */
	public boolean isFinal() {
		return isFinal;
	}

	/**
	 * @return
	 */
	public boolean isVarargs() {
		return isVarargs;
	}

	/**
	 * @return
	 */
	public boolean isQualifiedType() {
		return isQualifiedType;
	}

	private void setQualifiedType() {
		this.isQualifiedType = parameterDeclaration.getType().isQualifiedType();
	}

	/**
	 * @return
	 */
	public boolean isArrayType() {
		return isArrayType;
	}

	private void setArrayType() {
		this.isArrayType = parameterDeclaration.getType().isArrayType();
	}

	/**
	 * @return
	 */
	public boolean isParametrizedType() {
		return isParametrizedType;
	}

	private void setParametrizedType() {
		this.isParametrizedType = parameterDeclaration.getType().isParameterizedType();
	}

	/**
	 * @return
	 */
	public boolean isSimpleType() {
		return isSimpleType;
	}

	/**
	 * @return
	 */
	public boolean isPrimitiveType() {
		return isPrimitiveType;
	}
	
	/**
	 * @return
	 */
	public boolean isWildcardType() {
		return isWildcardType;
	}
	
	private void setSimpleType() {
		this.isSimpleType = parameterDeclaration.getType().isSimpleType();
	}

	/**
	 * @return
	 */
	public AOJType getType() {
		return type;
	}
}
