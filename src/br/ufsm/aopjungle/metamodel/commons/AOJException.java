package br.ufsm.aopjungle.metamodel.commons;

import javax.persistence.Entity;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.Name;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

@Entity
public class AOJException extends ASTElement {
	/**
	 */
	@XStreamOmitField
	@Transient
	private Name aojException = (Name)getNode();
	
	protected AOJException() {

	}
	
	public AOJException(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		setName(aojException.getFullyQualifiedName());
	}

}
