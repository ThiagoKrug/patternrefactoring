package br.ufsm.aopjungle.metamodel.commons;

import java.util.List;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

/**
 * @author  defaveri
 */
public interface AOJTypeble extends AOJAnnotable {
	/**
	 */
	public AOJProgramElement getOwner();
	/**
	 */
	public String getMetaName();
	/**
	 */
	public ASTNode getNode();
	/**
	 */
	public String getName();
	public int getNumberOfChildren();
	public void addNumberOfChildren(int arg0);
	public void subNumberOfChildren(int arg0);
	/**
	 */
	public String getFullQualifiedName();
	/**
	 */
	public AOJReferencedType getParent();
	public void setSuperType(AOJContainer type);
	/**
	 */
	public AOJModifier getModifiers();
	public List<AOJMethodDeclaration> getMethods();
	public List<AOJAttributeDeclaration> getAttributes();
}
