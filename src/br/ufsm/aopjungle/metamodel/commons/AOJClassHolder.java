package br.ufsm.aopjungle.metamodel.commons;

import java.util.List;

public interface AOJClassHolder {
	public List<AOJContainer> getInnerClasses();
	public List<AOJContainer> getAnonymousClasses(); 
}
