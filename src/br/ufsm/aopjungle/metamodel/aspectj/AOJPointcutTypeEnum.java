package br.ufsm.aopjungle.metamodel.aspectj;

public enum AOJPointcutTypeEnum {
	AND("AndPointcut"),
	OR("OrPointcut"),
	NOT("NotPointcut"),
	PRIMITIVE("DefaultPointcut"),
	CFLOW("Cflow"),
	CFLOWBELOW("CflowBelow"),
	PERCFLOW("PerCflow"),
	WITHIN("PerTypeWithin"),
	WITHINTOKEN("Within"),
	PERTHIS("PerObjectThis"),
	PERTARGET("PerObjectTarget"),
	EXECUTION("execution"),
	CALL("call"),
	GET("get"),
	SET("set"),
	INITIALIZATION("initialization"),
	PREINITILIZATION("preinitialization"),
	STATICINITILIZATION("staticinitialization"),
	HANDLER("handler"),
	ADVICEEXECUTION("adviceexecution"),
	WITHINCODE("withincode"),
	THIS("this"),
	TARGET("target"),
	ARGS("args"),
	IF("if"),
	ERROR("Error-KindNotFound");
	
	/**
	 */
	private String clause;
	
	AOJPointcutTypeEnum(String arg) {
		clause = arg;
	}
	
	public String getAsString() {
		return clause;
	}
	
	public static boolean existWithin(String arg0) {
		if (arg0 != null) {
			for (AOJPointcutTypeEnum pcTypeEnum : AOJPointcutTypeEnum.values()) {
				if (arg0.contains(pcTypeEnum.clause))
					return true;
			}		
		}
		return false;		
	}
	
	public static AOJPointcutTypeEnum getAsEnum(String className) {
		if (className != null) {
			for (AOJPointcutTypeEnum pcTypeEnum : AOJPointcutTypeEnum.values()) {
				if (className.equalsIgnoreCase(pcTypeEnum.clause))
					return pcTypeEnum;
			}		
		}
		return null;
	}
}