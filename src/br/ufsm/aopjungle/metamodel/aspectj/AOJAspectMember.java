package br.ufsm.aopjungle.metamodel.aspectj;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import br.ufsm.aopjungle.metamodel.commons.AOJConcreteTypeMember;
import br.ufsm.aopjungle.metrics.AOJMetrics;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
@Entity
public class AOJAspectMember extends AOJConcreteTypeMember {
	@Transient
//	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJAdviceDeclaration> advicesBefore;
	@Transient
//	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJAdviceDeclaration> advicesAround;
	@Transient
//	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJAdviceDeclaration> advicesAfter;
	@Transient
//	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJAdviceDeclaration> advicesAfterReturning;
	@Transient
//	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJAdviceDeclaration> advicesAfterThrowing;
//    @ManyToAny(metaColumn = @Column( name = "cunit_type" ))
//    @AnyMetaDef(
//    	idType = "long", metaType = "string",
//        metaValues = {
//                @MetaValue( targetEntity = AOJAdviceBefore.class, value="AB" ),
//                @MetaValue( targetEntity = AOJAdviceAround.class, value="AA" ),
//                @MetaValue( targetEntity = AOJAdviceAfter.class, value="AF" ),
//                @MetaValue( targetEntity = AOJAdviceAfterReturning.class, value="AR" ),
//                @MetaValue( targetEntity = AOJAdviceAfterThrowing.class, value="AT" ),
//        }
//      )
//        @Cascade( { org.hibernate.annotations.CascadeType.ALL } )
//        @JoinTable(
//            name = "aspectmember_advice", 
//            joinColumns = @JoinColumn( name = "aspectmember_id" ),
//            inverseJoinColumns = @JoinColumn( name = "advice_id" )
//        )
	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJAdviceDeclaration> allAdvices;
	@OneToMany(cascade=CascadeType.ALL)
	private List<AOJDeclareField> declareFields;
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="member_declaremethod_fk")
	private List<AOJDeclareMethod> declareMethods;
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="member_declareprecedence_fk")
	private List<AOJDeclarePrecedence> declarePrecedences;
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="member_declareerror_fk")
	private List<AOJDeclareCompilationEnforcement> declareErrors;
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="member_declarewarning_fk")
	private List<AOJDeclareCompilationEnforcement> declareWarnings;
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="member_declareparent_fk")
	private List<AOJDeclareParents> declareParents;
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="member_declaresoft_fk")
	private List<AOJDeclareSoft> declareSofts;

	@Override
	public void getMetricParticipants(List<AOJMetrics> members) {
		super.getMetricParticipants(members);
		for (AOJAdviceDeclaration member : getAllAdvices()) 
			if (member instanceof AOJMetrics)
				members.add(member);
		
		for (AOJDeclarePrecedence member : getDeclarePrecedences()) 
			if (member instanceof AOJMetrics)
				members.add(member);

		for (AOJDeclareCompilationEnforcement member : getDeclareErrors()) 
			if (member instanceof AOJMetrics)
				members.add(member);

		for (AOJDeclareCompilationEnforcement member : getDeclareWarnings()) 
			if (member instanceof AOJMetrics)
				members.add(member);

		for (AOJDeclareParents member : getDeclareParents()) 
			if (member instanceof AOJMetrics)
				members.add(member);
	}

	public AOJAspectMember() {
		AOJAdviceDeclaration.seed = 0;
		AOJDeclareMethod.seed = 0;
		AOJDeclareParents.seed = 0;
		AOJDeclareField.seed = 0;
		AOJDeclareCompilationEnforcement.seed = 0;
		AOJDeclarePrecedence.seed = 0;
	}
	
	@Deprecated
	private void updateAdvices() {
//		allAdvices = new ArrayList<AOJAdviceDeclaration>();
//		allAdvices.addAll(getAdvicesBefore());
//		allAdvices.addAll(getAdvicesAround());
//		allAdvices.addAll(getAdvicesAfter());
//		allAdvices.addAll(getAdvicesAfterReturning());
//		allAdvices.addAll(getAdvicesAfterThrowing());
	}
	
	public List<AOJDeclarePrecedence> getDeclarePrecedences() {
		if (declarePrecedences == null)
			declarePrecedences = new ArrayList<AOJDeclarePrecedence>();
		return declarePrecedences;
	}

	public List<AOJDeclareCompilationEnforcement> getDeclareErrors() {
		if (declareErrors == null)
			declareErrors = new ArrayList<AOJDeclareCompilationEnforcement>();
		return declareErrors;
	}

	public List<AOJDeclareCompilationEnforcement> getDeclareWarnings() {
		if (declareWarnings == null)
			declareWarnings = new ArrayList<AOJDeclareCompilationEnforcement>();
		return declareWarnings;
	}

	public List<AOJAdviceDeclaration> getAllAdvices() {
		if (allAdvices == null) 
			allAdvices = new ArrayList<AOJAdviceDeclaration>(); 	
		return allAdvices;
	}

	public List<AOJAdviceDeclaration> getAdvicesAround() {
		if (advicesAround == null) 
			advicesAround = new ArrayList<AOJAdviceDeclaration>();
		loadAdvice(advicesAround, AOJAdviceKindEnum.AROUND);
		return advicesAround;
	}

	public List<AOJAdviceDeclaration> getAdvicesAfter() {
		if (advicesAfter == null) 
			advicesAfter = new ArrayList<AOJAdviceDeclaration>();
		loadAdvice(advicesAfter, AOJAdviceKindEnum.AFTER);
		return advicesAfter;
	}

	public List<AOJAdviceDeclaration> getAdvicesAfterReturning() {
		if (advicesAfterReturning == null) 
			advicesAfterReturning = new ArrayList<AOJAdviceDeclaration>();
		loadAdvice(advicesAfterReturning, AOJAdviceKindEnum.AFTERRETURNING);
		return advicesAfterReturning;
	}

	public List<AOJAdviceDeclaration> getAdvicesAfterThrowing() {
		if (advicesAfterThrowing == null) 
			advicesAfterThrowing = new ArrayList<AOJAdviceDeclaration>();
		loadAdvice(advicesAfterThrowing, AOJAdviceKindEnum.AFTERTHROWING);				
		return advicesAfterThrowing;
	}

	public List<AOJAdviceDeclaration> getAdvicesBefore() {
		if (advicesBefore == null) 
			advicesBefore = new ArrayList<AOJAdviceDeclaration>();
		loadAdvice(advicesBefore, AOJAdviceKindEnum.BEFORE);				
		return advicesBefore;
	}
	
	private void loadAdvice(List<AOJAdviceDeclaration> list, AOJAdviceKindEnum kind) {
		for (AOJAdviceDeclaration advice : getAllAdvices()) 
			if (advice.getKind() == kind)
				list.add(advice);
	}
	

	public List<AOJDeclareParents> getDeclareParents() {
		if (declareParents == null) 
			declareParents = new ArrayList<AOJDeclareParents>(); 
		return declareParents;
	}

	public List<AOJDeclareParents> getDeclareParentsExtension() {
		List<AOJDeclareParents> result = new ArrayList<AOJDeclareParents>();
		
		if (declareParents != null) {
			for (AOJDeclareParents dParent : getDeclareParents()) {
				if (dParent.isExtends()) 
					result.add(dParent);
			}
		}
		
		return result;
	}

	public List<AOJDeclareParents> getDeclareParentsImplementation() {
		List<AOJDeclareParents> result = new ArrayList<AOJDeclareParents>();
		
		if (declareParents != null) {
			for (AOJDeclareParents dParent : getDeclareParents()) {
				if (! dParent.isExtends()) 
					result.add(dParent);
			}
		}
		
		return result;
	}

	public List<AOJDeclareField> getDeclareFields() {
		if (declareFields == null) 
			declareFields = new ArrayList<AOJDeclareField>(); 
	
		return declareFields;
	}

	public List<AOJDeclareMethod> getAllDeclareMethods() {
		if (declareMethods == null) 
			declareMethods = new ArrayList<AOJDeclareMethod>(); 
		return declareMethods;
	}

	public List<AOJDeclareMethod> getDeclareMethods() {
		List<AOJDeclareMethod> result = new ArrayList<AOJDeclareMethod>(); 
		
		for (AOJDeclareMethod declareMethod : getAllDeclareMethods() )
			if (! declareMethod.isConstructor())
				result.add(declareMethod);
		
		return result;
	}

	public List<AOJDeclareMethod> getDeclareConstructors() {
		List<AOJDeclareMethod> result = new ArrayList<AOJDeclareMethod>(); 
		
		for (AOJDeclareMethod declareMethod : getAllDeclareMethods() )
			if (declareMethod.isConstructor())
				result.add(declareMethod);
		
		return result;
	}

	public List<AOJDeclareSoft> getDeclareSofts() {
		if (declareSofts == null)
			declareSofts = new ArrayList<AOJDeclareSoft>();
		return declareSofts;
	}

}
