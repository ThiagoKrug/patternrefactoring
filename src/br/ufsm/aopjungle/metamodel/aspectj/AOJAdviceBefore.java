package br.ufsm.aopjungle.metamodel.aspectj;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;

@Deprecated
//@Entity
//@ForeignKey(name="advicebefore_fk")
//@DiscriminatorValue("AB")
public class AOJAdviceBefore extends AOJAdviceDeclaration {

	protected AOJAdviceBefore() {

	}
	
	public AOJAdviceBefore(ASTNode node, AOJProgramElement owner) {
		super(node, owner, AOJAdviceKindEnum.BEFORE);
	}

}
