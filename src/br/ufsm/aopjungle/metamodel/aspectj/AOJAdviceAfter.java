package br.ufsm.aopjungle.metamodel.aspectj;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;

import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
@Deprecated
//@Entity
//@DiscriminatorValue("AA")
public class AOJAdviceAfter extends AOJAdviceDeclaration {
	protected AOJAdviceAfter() {

	}
	
	public AOJAdviceAfter(ASTNode node, AOJProgramElement owner) {
		super(node, owner, AOJAdviceKindEnum.AFTER);
	}

}
