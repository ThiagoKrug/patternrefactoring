package br.ufsm.aopjungle.metamodel.aspectj;

public enum AOJDeclareParentKindEnum {
	ENTENDS, 
	IMPLEMENTS; 

	private String name;
	
	private AOJDeclareParentKindEnum() {
		this.setName(toString());
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
