package br.ufsm.aopjungle.metamodel.aspectj;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.aspectj.org.eclipse.jdt.core.dom.AndPointcut;
import org.aspectj.org.eclipse.jdt.core.dom.CflowPointcut;
import org.aspectj.org.eclipse.jdt.core.dom.DefaultPointcut;
import org.aspectj.org.eclipse.jdt.core.dom.NotPointcut;
import org.aspectj.org.eclipse.jdt.core.dom.OrPointcut;
import org.aspectj.org.eclipse.jdt.core.dom.PerCflow;
import org.aspectj.org.eclipse.jdt.core.dom.PerObject;
import org.aspectj.org.eclipse.jdt.core.dom.PerTypeWithin;
import org.aspectj.org.eclipse.jdt.core.dom.PointcutDesignator;
import org.aspectj.org.eclipse.jdt.core.dom.ReferencePointcut;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * Provides services to retrieve information
 * from a pointcut. This includes pointcutcode, params, modifiers , etc.
 * 
 * @author Cristiano de Faveri
 * 
 */
@Entity
public class AOJPointcutExpression {
	@Id 
	@GeneratedValue (strategy = GenerationType.AUTO)
	private long id;

	@XStreamOmitField
	@Transient 
	private PointcutDesignator pointcutDesignator;
	
	@XStreamOmitField
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="pcutexpression_fk")
	private List<AOJPointcutPrimitive> pointcutPrimitives;
	
	@XStreamOmitField
	@Lob
	private String code;

	@XStreamOmitField
	private boolean isAnonymous;

	@XStreamOmitField
    @Enumerated(EnumType.STRING) 
	private AOJPointcutTypeEnum type;
	
	public static final boolean FATSTRING = true;
	
	public AOJPointcutExpression(PointcutDesignator pointcutDesignator) {
		this.pointcutDesignator = pointcutDesignator;
		pointcutPrimitives = new ArrayList<AOJPointcutPrimitive>();	
		load();
	}

	public AOJPointcutExpression(PointcutDesignator pointcutDesignator, AOJPointcutDeclaration pointcutDeclaration) {
		this(pointcutDesignator);
	}

	protected AOJPointcutExpression() {

	}
	
	public PointcutDesignator getPointcutDesignator() {
		return pointcutDesignator;
	}
	
	public List<AOJPointcutPrimitive> getPrimitives() {
		return pointcutPrimitives;
	}
	
	/*
	 * @return "The enum related to the pointcutDesignator type. Some types are fake-named 
	 *          in order to return the properly enum.  
	 */
	
	public String getTypeAsString(boolean returnsFat) {
     	return returnsFat ? " " + getTypeAsEnum().toString().toLowerCase() + " " : getTypeAsEnum().toString().toLowerCase();	
	}
	
	private AOJPointcutTypeEnum getTypeAsEnum() {
		String className = "";
		if (pointcutDesignator instanceof PerCflow)
			className = ((PerCflow)pointcutDesignator).isBelow() ? "PerCflowBelow" : "PerCflow";
		else if (pointcutDesignator instanceof CflowPointcut)   
			className = ((CflowPointcut)pointcutDesignator).isCflowBelow() ? "CflowBelow" : "Cflow";
		else if (pointcutDesignator instanceof PerObject)       
		//  Perhaps a bug, isthis = true returns pertarget; Maybe I'm getting crazy...
		//  Double checking required	
			className = ((PerObject)pointcutDesignator).isThis() ? "PerObjectTarget" :  "PerObjectThis";
		else
			className = pointcutDesignator.getClass().getSimpleName();
		
		AOJPointcutTypeEnum result;
		try {
			result = AOJPointcutTypeEnum.getAsEnum(className);
		} catch (IllegalArgumentException e) {
			result = null;
		}
		
     	return result;	
	}
	
	private void load() {
		if ( pointcutDesignator != null ) 
			setCode(joinPointcut(pointcutDesignator));
		else
			setCode("none");	
		
		setType(getTypeAsEnum());
		setAnonymous(type != null);
	}
		
	private void setAnonymous(boolean isAnonymousPointcut) {
		this.isAnonymous = isAnonymousPointcut;
	}

	/**
	 * @return
	 */
	public boolean isAnonymous() {
		return isAnonymous;
	}

	/*
	 *  Receives PointcutDesignator due to recursive mechanism...
	 */
	private String joinPointcut(PointcutDesignator pointcutDesignator) {
		if (pointcutDesignator instanceof ReferencePointcut) 
			return ((ReferencePointcut)pointcutDesignator).getName().toString();
		
		if (pointcutDesignator instanceof AndPointcut) 
			return joinPointcut( ((AndPointcut)pointcutDesignator).getLeft() ) 
					+ getTypeAsString(FATSTRING)
					+ joinPointcut( ((AndPointcut)pointcutDesignator).getRight() );	
		 		
		if (pointcutDesignator instanceof OrPointcut) 
			return joinPointcut( ((OrPointcut)pointcutDesignator).getLeft() )
					+ getTypeAsString(FATSTRING) 
					+ joinPointcut( ((OrPointcut)pointcutDesignator).getRight() );	
				 
		if (pointcutDesignator instanceof NotPointcut)  
			return  getTypeAsString(FATSTRING) 
					+ joinPointcut( ((NotPointcut)pointcutDesignator).getBody() );
		
		//setAnonymousPointcut(true);
		if (pointcutDesignator instanceof DefaultPointcut) { 
			pointcutPrimitives.add(new AOJPointcutPrimitive((DefaultPointcut)pointcutDesignator));
			return ((DefaultPointcut)pointcutDesignator).getDetail();
		}
		
		if (pointcutDesignator instanceof CflowPointcut)  
			return joinPointcut (((CflowPointcut)pointcutDesignator).getBody());	

		if (pointcutDesignator instanceof PerCflow)
			return joinPointcut ( ((PerCflow)pointcutDesignator).getBody() );	

		if (pointcutDesignator instanceof PerTypeWithin)
			// TODO : There's not getType yet according to the documentation..
			// http://www.jarvana.com/jarvana/view/org/aspectj/aspectjtools/1.6.10/aspectjtools-1.6.10-javadoc.jar!/org/aspectj/org/eclipse/jdt/core/dom/PerTypeWithin.html
			return ((PerTypeWithin)pointcutDesignator).toString();   
		
		if (pointcutDesignator instanceof PerObject)
			return joinPointcut ( ((PerObject)pointcutDesignator).getBody() );  
		
	    return "?";
	}

	/**
	 * @return
	 */
	public AOJPointcutTypeEnum getType() {
		return type;
	}

	/**
	 * @param type
	 */
	private void setType(AOJPointcutTypeEnum type) {
		this.type = type;
	}

	/**
	 * @return
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 */
	private void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
}
