package br.ufsm.aopjungle.metamodel.java;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.IfStatement;

import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJStatement;

public class AOJIfStatement extends AOJStatement {

	private String code;
	private IfStatement ifStatement;

	public AOJIfStatement(ASTNode node, AOJProgramElement owner) {
		super(node, owner);
		ifStatement = (IfStatement) node;
		setCode(ifStatement.toString());
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public IfStatement getIfStatement() {
		return ifStatement;
	}

	public void setIfStatement(IfStatement ifStatement) {
		this.ifStatement = ifStatement;
	}

}