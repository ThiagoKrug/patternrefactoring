package br.ufsm.aopjungle.metamodel.java;

import org.aspectj.org.eclipse.jdt.core.dom.Expression;

import br.ufsm.aopjungle.metamodel.commons.AOJExpression;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;

public class AOJStringLiteral extends AOJExpression {
	private Expression expression;

	public AOJStringLiteral(AOJProgramElement owner, Expression expression) {
		super(owner, expression.toString());
		this.expression = expression;
	}

	public Expression getExpression() {
		return expression;
	}

	public void setExpression(Expression expression) {
		this.expression = expression;
	}
}
