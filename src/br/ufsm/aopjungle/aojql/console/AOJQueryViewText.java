package br.ufsm.aopjungle.aojql.console;

public class AOJQueryViewText implements AOJResultSetView {
	private int ident;
	
	@Override
	public void print(AOJResultSet resultSet) {
		// Printing MetaData
		ident = 0;
		System.out.println("");
		printData(resultSet);
	}
	
	private void printData(AOJResultSet resultSet) {
		if (resultSet.getRoot().getChildren().size() == 0) 
			System.out.println("\nNo data to display !");
		else {
			printNode(resultSet.getRoot());
		}	
	}	

	private void printNode(AOJQLTreeNode node) {
		write(node);
		if (node instanceof AOJQLTreeParent)
			for (AOJQLTreeNode nodeChild : ((AOJQLTreeParent)node).getChildren()) {
				ident++;
				printNode(nodeChild);
			}
		if (ident > 0)
			ident--;
	}

	private void write(AOJQLTreeNode node) {
		for (int i = 1; i <= ident; i++)
			System.out.print(" ");
		System.out.print("+-");
		System.out.println(node.getLabel());
		
	}
	}
