package br.ufsm.aopjungle.aojql.console;

import org.hibernate.SessionFactory;

import br.ufsm.aopjungle.hql.AOJHQLEngine;
import br.ufsm.aopjungle.util.HibernateUtil;

public class AOJQLEngineFactory {
	public static AOJHQLEngine createHQLEngine(SessionFactory sessionFactory) {
		return new AOJHQLEngine(HibernateUtil.getSessionFactory());
	}
}
