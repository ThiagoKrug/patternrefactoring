package br.ufsm.aopjungle.aojql.console;

public interface AOJResultSetView {
	public void print (AOJResultSet resultSet);
}
