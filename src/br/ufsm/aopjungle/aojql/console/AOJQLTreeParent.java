package br.ufsm.aopjungle.aojql.console;

import java.util.ArrayList;
import java.util.List;

public class AOJQLTreeParent extends AOJQLTreeNode {
	private List<AOJQLTreeNode> children;
	
	public void addChild(AOJQLTreeNode child) {
		children.add(child);
		child.setParent(this);
	}
	public void removeChild(AOJQLTreeNode child) {
		children.remove(child);
		child.setParent(null);
	}

	public AOJQLTreeParent(String label) {
		super(label);
		children = new ArrayList<AOJQLTreeNode>();
	}
	
	public List<AOJQLTreeNode> getChildren() {
		return children;
	}
	
	public boolean hasChildren() {
		return children.size()>0;
	}
	
	public void clear() {
		children.clear();
	}
}
