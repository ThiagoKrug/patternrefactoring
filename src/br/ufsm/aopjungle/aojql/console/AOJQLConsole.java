package br.ufsm.aopjungle.aojql.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.exception.AOJQueryFailException;
import br.ufsm.aopjungle.log.AOJLogger;

/**
 * A singleton class to query AOP meta-model. It supports different kind of query engines
 * like HQL, JoSQL and any other that satisfies the Engine interface.
 * 
 * @author Cristiano De Faveri
 * 
 */
public class AOJQLConsole { 
	
	//TODO : Create a generic engine interface
	private AOJQLQuery queryEngine;
	private AOJResultSetView resultSetView;
	private static AOJQLConsole console;
			
	public static AOJQLConsole getInstance (AOJResultSetView view, AOJQLQuery queryEngine) {
		if (console == null)
			console = new AOJQLConsole();
		console.resultSetView = view;
		console.queryEngine = queryEngine;
		return console;
	}
	
	private AOJQLConsole() {
		
	}
	
	public void cursor()  {
		boolean loop = true;
		
		while (loop) {
			System.out.print(queryEngine.getCursor());
			InputStreamReader reader = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(reader);
			try {
				String query = br.readLine();
				loop = !(query.equalsIgnoreCase("quit"));
				if (loop) {
					if (query.length() > 0) {
						long start = System.currentTimeMillis();
						resultSetView.print(queryEngine.execute(query, AOJWorkspace.getInstance().getProjects()));
						long elapsedTimeMillis = System.currentTimeMillis()-start;
						printMessage(String.format("\nExecuted in %s ms", elapsedTimeMillis));
					}
				}	
			} catch (IOException e) {
				AOJLogger.getLogger().error(e);
				printMessage(String.format("Error : %s", e.getMessage()));
			} catch (AOJQueryFailException e) {
				AOJLogger.getLogger().error(e);
				printMessage(String.format("Error : %s", e.getMessage()));
			}
		}
		
		System.out.println("Bye !");
	}

	private void printMessage(String message) {
		System.out.println(message);
	}

	public void showBanner() {
		System.out.println(queryEngine.getBanner());		
	}
	
	
//	private String compile(String query) throws RecognitionException {
//		CharStream stream = new ANTLRStringStream (query);
//		aojqlLexer lexer = new aojqlLexer(stream);
//		TokenStream tokenStream = new CommonTokenStream (lexer);
//		aojqlParser parser = new aojqlParser(tokenStream);
//		String hqlQuery="";
//		queryRule_return parsedQuery = parser.queryRule();
//		if (parsedQuery.getTree() != null) {
//			CommonTree tree = (CommonTree)parsedQuery.getTree();		
//			CommonTreeNodeStream nodes = new CommonTreeNodeStream(tree);
//			nodes.setTokenStream(tokenStream);
//			AOJQLHQLWalkerImpl walker = new AOJQLHQLWalkerImpl(nodes); 
//			br.ufsm.aopjungle.parser.antlr.aojqlwalker.queryRule_return rwalker = walker.queryRule();
//			CommonTree treeWalker = ((CommonTree)rwalker.getTree());			
//			HQLTranslator translator = new HQLTranslator(walker);
//			hqlQuery = translator.generate();
//		}	
//			return hqlQuery;
//	}
}
