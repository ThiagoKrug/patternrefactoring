package br.ufsm.aopjungle.astor;

import java.util.List;

public interface CodeSmellEngine {
	public void run();
	public List<CodeSmellReport> getReport();
	public String getLabel();
}
