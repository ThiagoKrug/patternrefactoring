package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.astor.util.AstorProperties;
import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;

public class LargeAspect extends CodeSmellImpl {
	@Override
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJTypeDeclaration aspect : project.getAspects())
				largeAspect(aspect);
	}

	private void largeAspect(AOJTypeDeclaration aspect) {
		if (aspect.getMetrics().getNumberOfLines() > AstorProperties.getLargeAspectMax()) {
			String message = String.format("Number of lines in aspect %s is large (%d)", aspect.getName(), aspect.getMetrics().getNumberOfLines());
			List<String> list = new ArrayList<String>();
			list.add(message);
			registerReport(list, aspect);
		}	
	}

	@Override
	public String getLabel() {
		return "Large Aspect";
	}	
}
