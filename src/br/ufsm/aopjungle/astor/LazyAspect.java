package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJModifierEnum;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class LazyAspect extends CodeSmellImpl {
	@XStreamOmitField
	private static final int CROSSCUTTING_THRESHOLD = 3;

	@Override
	public String getLabel() {
		return "Lazy Aspect";
	}
	
	@Override
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJAspectDeclaration aspect : project.getAspects())
				findLazyAspects(aspect);
	}

	private void findLazyAspects(AOJAspectDeclaration aspect) {
		if (  (aspect.getMetrics().getNumberOfCrossCuttingMembers() < CROSSCUTTING_THRESHOLD) && (! aspect.getModifiers().exists(AOJModifierEnum.ABSTRACT) )
		   || (aspect.getMetrics().getNumberOfCrossCuttingMembers() == 0 ) && (aspect.getModifiers().exists(AOJModifierEnum.ABSTRACT)) ) {
			String message = String.format("Some indications of Lazy Aspect was found. Aspect : %s", aspect.getFullQualifiedName());
			List<String> list = new ArrayList<String>();
			list.add(message);
			registerReport(list, aspect);
		}
	}
}
