package br.ufsm.aopjungle.astor;

import java.util.List;

import br.ufsm.aopjungle.patternrefactoring.RefOppToEncapsulateClassesWithFactory;
import br.ufsm.aopjungle.patternrefactoring.RefactoringOpportunity;

public class CodeSmellReport {
	private String projectName;
	private String packageName;
	private String compilationUnitName;
	private String containerName;
	private String containerType;
	private List<String> elements;
	private RefactoringOpportunity refactoringOpportunity;

	public CodeSmellReport(String projectName, String packageName, String compilationUnitName, String containerName, String containerType, List<String> elements) {
		this.projectName = projectName;
		this.packageName = packageName;
		this.compilationUnitName = compilationUnitName;
		this.containerName = containerName;
		this.containerType = containerType;
		this.elements = elements;
	}

	public CodeSmellReport(String projectName, String packageName, String compilationUnitName, String containerName, String containerType,
			RefactoringOpportunity refactoringOpportunity) {
		this.projectName = projectName;
		this.packageName = packageName;
		this.compilationUnitName = compilationUnitName;
		this.containerName = containerName;
		this.containerType = containerType;
		this.refactoringOpportunity = refactoringOpportunity;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getCompilationUnitName() {
		return compilationUnitName;
	}

	public void setCompilationUnitName(String compilationUnitName) {
		this.compilationUnitName = compilationUnitName;
	}

	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public String getContainerType() {
		return containerType;
	}

	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}

	public List<String> getElements() {
		return elements;
	}

	public void setElements(List<String> elements) {
		this.elements = elements;
	}

	public RefactoringOpportunity getRefactoringOpportunity() {
		return refactoringOpportunity;
	}

	public void setRefactoringOpportunity(RefOppToEncapsulateClassesWithFactory refactoringOpportunity) {
		this.refactoringOpportunity = refactoringOpportunity;
	}
}
