package br.ufsm.aopjungle.astor.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import br.ufsm.aopjungle.log.AOJLogger;
import br.ufsm.aopjungle.util.IconProvider;

public class AstorProperties {
	private static Properties properties;
	private static final String LARGEASPECT_MAXLINES = "LargeAspect.MaxLines";
	private static final String METHODLIKE_MAXLINES = "MethodLike.MaxLines";
	private static final String DUPLICATEDCODE_CODESIMILARITY = "DuplicatedCode.CodeSimilarity";
	private static final String DIVERGENTCHANGES_CODESIMILARITY = "DivergentChange.CodeSimilarity";
	private static final String FEATUREENVY_MINASPECTS = "FeatureEnvy.MinAspects";
	private static final String LARGEPOINTCUTDEFINITION_MAXPRIMITIVES = "LargePointcutDefinition.MaxPrimitives";
	private static final String EXPORTFILEPATHNAME = "ExportFilePathName";
	private static final String appPath = IconProvider.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	private static final String DEFAULTPATH = appPath + "conf/";
	private static final String fileNameProperties = DEFAULTPATH+"astorrules.properties";
	private static final String DOUBLEPERSONALITY_INTERFACES = "DoublePersonality.MaxInterfaces";
	private static final String LAZYASPECT_MINCROSSCUTTING = "LazyAspect.MinCrosscutting";

	private static void loadProperties()  {
		properties = new Properties();
		try {
			properties.load(new FileInputStream(fileNameProperties));
			AOJLogger.getLogger().info("ASTOR Configuration file was loaded from " + fileNameProperties);
		} catch (FileNotFoundException e) {
			AOJLogger.getLogger().info("ASTOR Configuration file not found. Default file was loaded.");
			loadDefaultProperties();
		} catch (IOException e) {
			AOJLogger.getLogger().info("ASTOR Configuration file not found. Default file was loaded.");
			loadDefaultProperties();
		}
	}
	
	private static Properties getProperties() {
		if (properties == null)
			loadProperties();
		return properties;
	}
	
	private static void loadDefaultProperties() {
		properties = new Properties();
		properties.setProperty(EXPORTFILEPATHNAME, "/astor.xml");
		properties.setProperty(LARGEASPECT_MAXLINES, "200");
		properties.setProperty(METHODLIKE_MAXLINES, "15");
		properties.setProperty(DUPLICATEDCODE_CODESIMILARITY, "90");
		properties.setProperty(DIVERGENTCHANGES_CODESIMILARITY, "90");
		properties.setProperty(FEATUREENVY_MINASPECTS, "2");
		properties.setProperty(LARGEPOINTCUTDEFINITION_MAXPRIMITIVES, "200");
		properties.setProperty(DOUBLEPERSONALITY_INTERFACES, "10");
		properties.setProperty(LAZYASPECT_MINCROSSCUTTING, "3");
	}

	public static String getExportFilePathName() {
		return getProperties().getProperty(EXPORTFILEPATHNAME);
	}
	
	public static int getLargeAspectMax() {
		return Integer.parseInt(getProperties().getProperty(LARGEASPECT_MAXLINES, "200"));
	}
	
	public static int getMethodLikeMax() {
		return Integer.parseInt(getProperties().getProperty(METHODLIKE_MAXLINES, "15"));
	}

	public float getCodeSimilarityuplicatedCode() {
		return Float.parseFloat(getProperties().getProperty(DUPLICATEDCODE_CODESIMILARITY, "90"));
	}
	
	public static float getCodeSimilarityDivergentChange() {
		return Float.parseFloat(getProperties().getProperty(DIVERGENTCHANGES_CODESIMILARITY, "90"));
	}

	public static int getFeatureEnvyMinAspect() {
		return Integer.parseInt(getProperties().getProperty(FEATUREENVY_MINASPECTS, "2"));
	}

	public static int getLargePointcutDefinitionMax() {
		return Integer.parseInt(getProperties().getProperty(LARGEPOINTCUTDEFINITION_MAXPRIMITIVES, "200"));
	}

	public static int getDoublePersonalityInterfaces() {
		return Integer.parseInt(getProperties().getProperty(DOUBLEPERSONALITY_INTERFACES, "10"));
	}

	public static int getCrosscuttingMaxElements() {
		return Integer.parseInt(getProperties().getProperty(LAZYASPECT_MINCROSSCUTTING, "3"));
	}
	
}
