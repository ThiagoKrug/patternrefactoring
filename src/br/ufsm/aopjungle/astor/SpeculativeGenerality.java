package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.aspectj.AOJAspectDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJModifierEnum;
import br.ufsm.aopjungle.metamodel.commons.AOJPackageDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class SpeculativeGenerality extends CodeSmellImpl {
	@XStreamOmitField
	private List<CandidateCodeSmell> candidateCodeSmell = new ArrayList<CandidateCodeSmell>();

	class CandidateCodeSmell {
		@XStreamOmitField
		private boolean hasDescendant = false;
		@XStreamOmitField
		private boolean hasInterTypeDeclarations = false;
		@XStreamOmitField
		private boolean hasAssociations = false;
		@XStreamOmitField
		private long numberOfAffectedElements = 0;
		@XStreamOmitField
		private AOJProject project;
		@XStreamOmitField
		private AOJPackageDeclaration pack;
		@XStreamOmitField
		private AOJProgramElement cunit;
		@XStreamOmitField
		private AOJAspectDeclaration aspect;
		
		CandidateCodeSmell (AOJProject project, AOJPackageDeclaration pack,
					AOJProgramElement cunit, AOJAspectDeclaration aspect) {
			this.setProject(project);
			this.pack = pack;
			this.cunit = cunit;
			this.aspect = aspect;
			calcMetrics();
		}
		
		private void calcMetrics() {
			hasDescendant = aspect.getMetrics().getNumberOfDescendents() > 0;
			hasInterTypeDeclarations = aspect.getMetrics().getNumberOfInterTypeDeclarations() > 0;
			//hasAssociations = 
			numberOfAffectedElements = aspect.getMetrics().getNumberOfOutAffects();
		}
		
		public boolean hasDescendants() {
			return hasDescendant;
		}

		public boolean hasInterTypeDeclarations() {
			return hasInterTypeDeclarations;
		}

		public boolean hasAssociations() {
			return hasAssociations;
		}

		public long getNumberOfAffectedElements() {
			return numberOfAffectedElements;
		}

		public AOJPackageDeclaration getPack() {
			return pack;
		}

		public void setPack(AOJPackageDeclaration pack) {
			this.pack = pack;
		}

		public AOJProgramElement getCunit() {
			return cunit;
		}

		public void setCunit(AOJProgramElement cunit) {
			this.cunit = cunit;
		}

		public AOJAspectDeclaration getAspect() {
			return aspect;
		}

		public void setAspect(AOJAspectDeclaration aspect) {
			this.aspect = aspect;
		}

		public AOJProject getProject() {
			return project;
		}

		public void setProject(AOJProject project) {
			this.project = project;
		}
	}
	
	@Override
	public void run() {
		loadAbstractAspects();
		for (CandidateCodeSmell candidate : candidateCodeSmell)
			processSpeculativeGenerality(candidate);
	}
	
	private void processSpeculativeGenerality(CandidateCodeSmell candidateCodeSmell) {
		if ((!candidateCodeSmell.hasAssociations()) 
				&& (!candidateCodeSmell.hasDescendants())
				&& (!candidateCodeSmell.hasInterTypeDeclarations()) 
				&& (candidateCodeSmell.getNumberOfAffectedElements() == 0)) {
			String message = String.format("Some indication was found about Speculative Generality at aspect %s", candidateCodeSmell.getAspect().getName());
			List<String> list = new ArrayList<String>();
			list.add(message);
			registerReport(list, candidateCodeSmell.getAspect());
		}
	}
	
	private void loadAbstractAspects() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) 
			for (AOJPackageDeclaration pack: project.getPackages()) 
				for (AOJCompilationUnit cunit : pack.getCompilationUnits()) 
					for (AOJAspectDeclaration aspect : cunit.getAspects()) 
						if (aspect.getModifiers().exists(AOJModifierEnum.ABSTRACT)) 
							candidateCodeSmell.add(new CandidateCodeSmell(project,pack, cunit, aspect));
	}

	@Override
	public String getLabel() {
		return "Speculative Generality";
	}
}
