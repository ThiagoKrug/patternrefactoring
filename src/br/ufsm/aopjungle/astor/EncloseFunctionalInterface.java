package br.ufsm.aopjungle.astor;

import java.util.ArrayList;
import java.util.List;

import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJExpressionStatement;
import br.ufsm.aopjungle.metamodel.commons.AOJMethodDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJStatement;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJAnonymousClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;

public class EncloseFunctionalInterface extends CodeSmellImpl {
	
	@Override
	public void run() {
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()){
//			for (AOJTypeDeclaration aspect : project.getAspects())
//				largeAspect(aspect);
			for (AOJTypeDeclaration clazz : project.getClasses())
				findRefactorings(clazz);
		}

	}
	
	int cont = 1;
	
	private void printFunctionalInterface(List<AOJContainer> listContainer, AOJTypeDeclaration _class, String type){
		for (AOJContainer container : listContainer){
			System.out.println(container.getName()+" - "+container.getOwner().getName());
			if (((AOJAnonymousClassDeclaration)container).isFunctionalInterface()){
				String message = String.format( "%s: %s - %s: %s",type, ((AOJAnonymousClassDeclaration)container).getName(), ((AOJAnonymousClassDeclaration)container).getInstanceTypeName(), ((AOJAnonymousClassDeclaration)container).getCode() );
				List<String> list = new ArrayList<String>();
				list.add(message);
				
				registerReport(list, _class);
			}
			
			for ( AOJMethodDeclaration method : ((AOJAnonymousClassDeclaration)container).getMembers().getMethods()) 
				printFunctionalInterface(filterAnonymous(method.getAnonymousClasses()), _class, type);
			
		}
	}

	private void findRefactorings(AOJTypeDeclaration _class) {
		
		// constructors
		for (AOJConstructorDeclaration constructor : ((AOJClassDeclaration)_class).getMembers().getConstructors()){
			printFunctionalInterface(filterAnonymous(constructor.getAnonymousClasses()), _class, "Constructor");
			
			for(AOJStatement statement : constructor.getStatements()) 
				if (statement instanceof AOJExpressionStatement)
					printFunctionalInterface(filterAnonymous(((AOJExpressionStatement) statement).getAnonymousClasses()), _class, "Constructor");
//					for(AOJContainer container :  ((AOJExpressionStatement) statement).getAnonymousClasses())
//						if (((AOJAnonymousClassDeclaration)container).isFunctionalInterface()) {
//							String message = String.format( "Constructor %s - %s",constructor.getName(), ((AOJAnonymousClassDeclaration)container).getInstanceTypeName(), ((AOJAnonymousClassDeclaration)container).getCode() );
//							//String message = String.format( "Anonymous Class - %s",((AOJAnonymousClassDeclaration)container).getInstanceTypeName() + " (Constructor  - "+constructor.getName()+")");
//							List<String> list = new ArrayList<String>();
//							list.add(message);
//							registerReport(list, _class);
//						}
		}
		
		// methods
		for (AOJMethodDeclaration method : ((AOJClassDeclaration)_class).getMembers().getMethods()) {
			if (method.getNode().toString().contains("03"))
				System.out.println("blah");
			printFunctionalInterface(filterAnonymous(method.getAnonymousClasses()), _class, "Method");

			for(AOJStatement statement : method.getStatements()) 
				if (statement instanceof AOJExpressionStatement)
					printFunctionalInterface(filterAnonymous( ((AOJExpressionStatement) statement).getAnonymousClasses()), _class, "Method");

//					for(AOJContainer container :  ((AOJExpressionStatement) statement).getAnonymousClasses())
//						if (((AOJAnonymousClassDeclaration)container).isFunctionalInterface()) {
//							String message = String.format( "Method: %s - %s: %s ",method.getName(), ((AOJAnonymousClassDeclaration)container).getInstanceTypeName(), ((AOJAnonymousClassDeclaration)container).getCode() );
//							//String message = String.format( "Anonymous Class - %s",((AOJAnonymousClassDeclaration)container).getInstanceTypeName() + " (Method - "+method.getName()+")");
//							List<String> list = new ArrayList<String>();
//							list.add(message);
//							registerReport(list, _class);
//						}
		}

	}
	
	private List<AOJContainer> filterAnonymous(List<AOJContainer> list) {
		List<AOJContainer> result = new ArrayList<AOJContainer>();
		for (AOJContainer container : list)
			if (container instanceof AOJAnonymousClassDeclaration)
				result.add(container);
		return result;
	}

	@Override
	public String getLabel() {
		return "Convert Functional Interface to Lambda Expression";
		//return "Enclose Functional Interface";
	}	
}
