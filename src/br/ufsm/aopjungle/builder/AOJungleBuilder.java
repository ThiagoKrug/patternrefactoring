package br.ufsm.aopjungle.builder;

public class AOJungleBuilder {
	private AOJWorkspace workspace;
	
	public AOJungleBuilder() {
		workspace = AOJWorkspace.getInstance();
	}

	public AOJWorkspace getWorkspace() {
		return workspace;
	}
	
}	
