package br.ufsm.aopjungle.builder;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.JavaModelException;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.ufsm.aopjungle.aspectj.AOJCompiler;
import br.ufsm.aopjungle.exception.AOJCompilerException;
import br.ufsm.aopjungle.labs.AOJBindingOptionEnum;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.resource.ProjectResources;
import br.ufsm.aopjungle.views.AOPJungleMonitor;
import br.ufsm.aopjungle.views.AOPJungleView;

/**
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 *         Federal University of Santa Maria <br>
 *         Programming Languages and Database Research Group <br>
 */
public class AOJWorkspace {
	@XStreamOmitField
	private static AOJWorkspace workspace;
	private List<AOJProject> projects;
	private AOJProject currentProject;

	public static AOJWorkspace getInstance() {
		if (workspace == null)
			workspace = new AOJWorkspace();

		return workspace;
	}

	private AOJWorkspace() {
		// AOJungleASMVisitor visitor = new AOJungleASMVisitor();
		// AjBuildManager.setAsmHierarchyBuilder(visitor);
	}

	/*
	 * public void build() { try { AOJProgressMonitor monitor = new
	 * AOJProgressMonitor();
	 * ResourcesPlugin.getWorkspace().build(IncrementalProjectBuilder.FULL_BUILD,
	 * monitor); } catch (CoreException e) { e.printStackTrace(); } }
	 */

	public void build(AOJProject project) {
		System.out.println("Starting compilation project : " + project.getName());
		AOJCompiler builder = new AOJCompiler();
		try {
			builder.build(project);
		} catch (AOJCompilerException e) {
			e.printStackTrace();
		}

		/*
		 * try { AOJProgressMonitor monitor = new AOJProgressMonitor();
		 * ResourcesPlugin.getWorkspace().getRoot().getProject(project.getName()).build(
		 * IncrementalProjectBuilder.FULL_BUILD, monitor); } catch (CoreException e) {
		 * e.printStackTrace(); }
		 */
		System.out.println("Ends compilaton project : " + project.getName());
	}

	public void setProjects(List<AOJProject> projects) {
		this.projects = projects;
	}

	public List<AOJProject> getProjects() {
		if (projects == null)
			projects = new ArrayList<AOJProject>();
		return projects;
	}
	
	public AOJProject getProjectByName(String projectName) {
		for (AOJProject aojProject : projects) {
			if (aojProject.getName().compareTo(projectName) == 0)
				return aojProject;
		}
		return null;
	}

	public void loadProjects(AOJBindingOptionEnum setBindings) throws JavaModelException, CoreException {
		// if (setBindings == AOJBindingOptionEnum.BINDING_ON)
		// AJBuilder.addAdviceListener(new AOJAdviceListener());

		List<IProject> workspaceProjects = new ArrayList<IProject>();
		workspaceProjects = ProjectResources.getInstance().getProjects();
		AOPJungleMonitor.projectsMonitor = AOPJungleMonitor.mainMonitor.split(100).setWorkRemaining(AOPJungleView.selectedProjects.length);
		for (String projectName : AOPJungleView.selectedProjects) {
			AOPJungleMonitor.projectMonitor = AOPJungleMonitor.projectsMonitor.split(1).setWorkRemaining(100);
			for (IProject project : workspaceProjects) {
				if (project.getName().compareTo(projectName) == 0) {
					loadProject(project);
				}
			}
		}

		// build();
	}

	public void loadProject(IProject project) throws CoreException {
		// if (project.getName().contains("EclipseLink")) {
		// if (project.getName().contains("aAPICartas")){
		if (project.isNatureEnabled("org.eclipse.jdt.core.javanature")) {
			project.open(AOPJungleMonitor.projectMonitor.split(1));

			AOPJungleMonitor.projectMonitor.setTaskName("Parsing project " + project.getName());
			AOJProject projectNode = new AOJProject(project);

			currentProject = projectNode;
			getProjects().add(projectNode);
			build(projectNode); // Compile project to traverse nodes
		}
	}

	public AOJProject getCurrentProject() {
		return currentProject;
	}

}
