package br.ufsm.aopjungle.factories;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;

import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJPackageDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

public class AOJCommonFactoryImpl implements AOJCommonFactory {
	private static AOJCommonFactoryImpl myself;
	
	@Override
	public AOJProject createProject(IProject resourceProject) {
		return new AOJProject(resourceProject);
	}

	@Override
	public AOJPackageDeclaration createPackage(IPackageFragment pack, AOJProgramElement owner) {
		return new AOJPackageDeclaration(pack, owner);
	}

	@Override
	public AOJCompilationUnit createCompilationUnit(ICompilationUnit compilationUnit, AOJProgramElement owner) {
		return new AOJCompilationUnit(compilationUnit, owner);
	}

	public static AOJCommonFactory init() {
		if (null == myself)
			myself = new AOJCommonFactoryImpl();
		return myself;
	}

}
