package br.ufsm.aopjungle.factories;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;

import br.ufsm.aopjungle.metamodel.commons.AOJCompilationUnit;
import br.ufsm.aopjungle.metamodel.commons.AOJPackageDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProgramElement;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;

/**
 * A Factory for common elements of the model
 * 
 * @author Cristiano De Faveri (cristiano@defaveri.com.br) <br>
 * Federal University of Santa Maria <br>
 * Programming Languages and Database Research Group <br>
 */
public interface AOJCommonFactory {
	AOJCommonFactory eINSTANCE = AOJCommonFactoryImpl.init();
	public AOJProject createProject(IProject resourceProject);
	public AOJPackageDeclaration createPackage(IPackageFragment pack, AOJProgramElement owner);
	public AOJCompilationUnit createCompilationUnit(ICompilationUnit compilationUnit, AOJProgramElement owner);
}
