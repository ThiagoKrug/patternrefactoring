package br.ufsm.aopjungle.patternrefactoring;

import java.util.List;

import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;

public class RefOppClassCandidate {
	private AOJClassDeclaration clazz;
	private List<RefOppMethodCandidate> methods;

	public RefOppClassCandidate(AOJClassDeclaration clazz, List<RefOppMethodCandidate> methods) {
		super();
		this.clazz = clazz;
		this.methods = methods;
	}

	public AOJClassDeclaration getClazz() {
		return clazz;
	}

	public void setClazz(AOJClassDeclaration clazz) {
		this.clazz = clazz;
	}

	public List<RefOppMethodCandidate> getMethods() {
		return methods;
	}

	public void setMethods(List<RefOppMethodCandidate> methods) {
		this.methods = methods;
	}

}
