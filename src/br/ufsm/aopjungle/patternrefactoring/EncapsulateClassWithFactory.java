package br.ufsm.aopjungle.patternrefactoring;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.aspectj.org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.aspectj.org.eclipse.jdt.core.dom.Type;
import org.eclipse.core.resources.IFile;

import br.ufsm.aopjungle.astor.CodeSmellImpl;
import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.commons.AOJAssignment;
import br.ufsm.aopjungle.metamodel.commons.AOJBehaviourKind;
import br.ufsm.aopjungle.metamodel.commons.AOJClassInstanceCreation;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJExpression;
import br.ufsm.aopjungle.metamodel.commons.AOJExpressionStatement;
import br.ufsm.aopjungle.metamodel.commons.AOJImportDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJMethodDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJMethodInvocation;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJStatement;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJTypeLiteral;
import br.ufsm.aopjungle.metamodel.commons.AOJVariableDeclarationStatement;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;
import br.ufsm.aopjungle.patternrefactoring.util.Normalizador;
import br.ufsm.aopjungle.patternrefactoring.util.PRProp;
import br.ufsm.aopjungle.patternrefactoring.util.ProjectUtils;
import br.ufsm.aopjungle.views.AOPJungleMonitor;

public class EncapsulateClassWithFactory extends CodeSmellImpl {

	private List<AOJClassDeclaration> requiredClazzes;
	private List<AOJClassDeclaration> projectClazzes;
	private List<AOJClassDeclaration> typeLiteralClazzes;
	private AOJClassDeclaration currentClazz;
	private AOJProject currentProject;
	private List<RefOppToEncapsulateClassesWithFactory> refactoringOpportunities;
	
	@Override
	public String getLabel() {
		return "Encapsulate Classes with Factory";
	}

	@Override
	public void run() {
		AOPJungleMonitor.smellEngineMonitor = AOPJungleMonitor.smellEnginesMonitor.split(1).setWorkRemaining(AOJWorkspace.getInstance().getProjects().size());
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) {
			AOPJungleMonitor.smellEngineMonitor.subTask("Finding opportunities in project " + project.getName());
			currentProject = project;

			this.loadElementsFromAST(project);

			// List<AOJClassDeclaration> candidateClasses = this.getRootSuperTypes();
			// List<AOJClassDeclaration> candidateClasses = projectClazzes;
			List<AOJClassDeclaration> candidateClasses = this.getParentClasses();

			this.findRefactoringOpportunities(candidateClasses);
		}
	}

	private void findRefactoringOpportunities(List<AOJClassDeclaration> candidateClazzes) {
		refactoringOpportunities = new ArrayList<RefOppToEncapsulateClassesWithFactory>();
		for (AOJClassDeclaration candidateClazz : candidateClazzes) {
			double numberOfSubTypes = this.countSubTypes(candidateClazz);
			if (numberOfSubTypes >= 2) {
				// verifica se é um visitor
				double visitor = this.verifyVisitor(candidateClazz);
				double numberOfClassesInSamePackage = this.countSubTypesInSamePackage(candidateClazz, candidateClazz); // método recursivo
				double numberOfPublicModificators = this.countPublicModificators(candidateClazz);
				double numberOfSubTypesCalls = this.countSubTypesCalls(candidateClazz);

				// FUNÇÃO HEURÍSTICA
				double weight1 = PRProp.getECWFWeight1(), weight2 = PRProp.getECWFWeight2(), weight3 = PRProp.getECWFWeight3(), weight4 = PRProp.getECWFWeight4(), weight5 = PRProp.getECWFWeight5();

				double resultNumberOfSubTypes = Normalizador.normalizarReflexao(numberOfSubTypes, PRProp.getECWFSubtypes());
				double resultNumberOfClassesInSamePackage = Normalizador.normalizarReflexao(numberOfClassesInSamePackage, PRProp.getECWFClassesInTheSamePackage());
				double resultNumberOfPublicModificators = Normalizador.normalizarRetilineo(numberOfPublicModificators, PRProp.getECWFPublicModificators());
				double resultVisitor = Normalizador.normalizarRetilineo(visitor, PRProp.getECWFVisitor());
				double resultNumberOfSubTypesCalls = Normalizador.normalizarRetilineo(numberOfSubTypesCalls, PRProp.getECWFSubtypesCalls());

				double resultado = resultNumberOfSubTypes * weight1 + resultNumberOfClassesInSamePackage * weight2 - resultNumberOfPublicModificators * weight3 - resultVisitor * weight4
						- resultNumberOfSubTypesCalls * weight5;

				refactoringOpportunities.add(new RefOppToEncapsulateClassesWithFactory(resultado, candidateClazz, numberOfSubTypes, resultNumberOfSubTypes, numberOfClassesInSamePackage,
						resultNumberOfClassesInSamePackage, numberOfPublicModificators, resultNumberOfPublicModificators, visitor, resultVisitor, numberOfSubTypesCalls, resultNumberOfSubTypesCalls));
			}
		}

		// ordena as oportunidades de refatoração
		Collections.sort(refactoringOpportunities);
		// deixa em ordem descendente
		Collections.reverse(refactoringOpportunities);

		int i = 1;
		for (RefOppToEncapsulateClassesWithFactory ro : refactoringOpportunities) {
			ro.setPosition(i++);

			String message = "Encapsulate this subtypes into " + ro.getClazz().getName() + ": ";
			message += this.createMessage(ro.getClazz());
			String ms = message.substring(0, message.length() - 2) + ".";
			ro.setMessage(ms);

			registerReport(ro, ro.getClazz());
		}

		/*
		 * System.out.println("\n");
		 * System.out.println("Total de oportunidades candidatas encontradas: " +
		 * refactoringOpportunities.size()); for (RefactoringOpportunity
		 * refactoringOpportunity : refactoringOpportunities) {
		 * System.out.println(refactoringOpportunity.toStringComplete()); }
		 */

	}

	private double countSubTypesCalls(AOJClassDeclaration candidateClazz) {
		int count = 0;
		for (AOJContainer st : candidateClazz.getSubTypes()) {
			AOJClassDeclaration subtype = (AOJClassDeclaration) st;

			for (AOJClassDeclaration typeLiteral : typeLiteralClazzes) {
				if (typeLiteral.equals(subtype)) {
					count++;
				}
			}

			count += this.countSubTypesCalls(subtype);
		}
		return count;
	}

	private double verifyVisitor(AOJClassDeclaration root) {
		double visits = 0;

		if (root.getName().contains("Visitor")) {
			visits += 2;
		}
		// obtenho todos os métodos públicos da classe root
		for (AOJMethodDeclaration method : root.getMembers().getMethods()) {
			if (method.getName().startsWith("visit")) {
				visits += 1;
			}
		}
		return visits;
	}

	/*
	 * public static void main(String args[]) { EncapsulateClassWithFactory e = new
	 * EncapsulateClassWithFactory(); double numberOfSubTypes = 18; double
	 * numberOfClassesInSamePackage = 18; double numberOfPublicModficators = 2;
	 * 
	 * // FUNÇÃO HEURÍSTICA double weight1 = 1, weight2 = 1, weight3 = 0.7;
	 * 
	 * double resultNumberOfSubTypes = e.aplicaNormalizacao(numberOfSubTypes, 7);
	 * double resultNumberOfClassesInSamePackage =
	 * e.aplicaNormalizacao(numberOfClassesInSamePackage, 17); double
	 * resultNumberOfPublicModficators =
	 * e.aplicaRetilineo(numberOfPublicModficators, 6);
	 * 
	 * double resultado = resultNumberOfSubTypes * weight1 +
	 * resultNumberOfClassesInSamePackage * weight2 -
	 * resultNumberOfPublicModficators * weight3;
	 * 
	 * String s = "\nNumber Of SubTypes: " + resultNumberOfSubTypes; s +=
	 * "\nNumber Of Classes in the Same Package: " +
	 * resultNumberOfClassesInSamePackage; s += "\nNumber of Public Modificators: "
	 * + resultNumberOfPublicModficators; s += "\nResult: " + resultado + "\n";
	 * System.out.println(s);
	 * 
	 * }
	 */

	private int countPublicModificators(AOJClassDeclaration root) {
		List<String> rootMethods = new ArrayList<>();
		// obtenho todos os métodos públicos da classe root
		for (AOJMethodDeclaration method : root.getMembers().getMethods()) {
			for (String modifier : method.getModifiers().getNames()) {
				if (modifier.equalsIgnoreCase("public")) {
					rootMethods.add(method.getName());
				}
			}
		}

		return this.countSubtypesPublicModficators(root, rootMethods);
	}

	private int countSubtypesPublicModficators(AOJClassDeclaration type, List<String> rootMethods) {
		int count = 0;
		for (AOJContainer st : type.getSubTypes()) {
			AOJClassDeclaration subtype = (AOJClassDeclaration) st;

			// verifico os métodos públicos
			for (AOJMethodDeclaration method : subtype.getMembers().getMethods()) {
				for (String modifier : method.getModifiers().getNames()) {
					if (modifier.equalsIgnoreCase("public")) {
						if (rootMethods.contains(method.getName()) == false) {
							count++;
						}
					}
				}
			}

			count += this.countSubtypesPublicModficators(subtype, rootMethods);
		}
		return count;
	}

	private int countSubTypesInSamePackage(AOJClassDeclaration rootClazz, AOJClassDeclaration type) {
		int count = 0;
		for (AOJContainer st : type.getSubTypes()) {
			AOJClassDeclaration subtype = (AOJClassDeclaration) st;
			boolean samePackage = this.isInsideTheSamePackage(rootClazz, subtype);
			if (samePackage) {
				count++;
			}

			count += this.countSubTypesInSamePackage(rootClazz, subtype);
		}
		return count;
	}

	/**
	 * Cria a mensagem para ser impressa
	 * 
	 * @param rootClazz
	 * @return
	 */
	private String createMessage(AOJTypeDeclaration rootClazz) {
		String message = "";
		for (AOJContainer st : rootClazz.getSubTypes()) {
			AOJClassDeclaration subtype = (AOJClassDeclaration) st;
			message += subtype.getName() + ", ";
			message += this.createMessage((AOJClassDeclaration) subtype);
		}
		return message;
	}

	/**
	 * Conta a quantidade total (contando recursivamente todos os nós) de SubTypes
	 * que uma classe root possui que fazem parte das classes requeridas.
	 * 
	 * @param rootClazz
	 * @return
	 */
	private int countSubTypes(AOJClassDeclaration rootClazz) {
		int count = rootClazz.getSubTypes().size();
		for (AOJContainer st : rootClazz.getSubTypes()) {
			AOJClassDeclaration subtype = (AOJClassDeclaration) st;
			count += this.countSubTypes(subtype);
		}
		return count;
	}

	/**
	 * Carrega os elementos da AST necessários para encontrar as oportunidades de
	 * refatoração
	 * 
	 * @param project
	 */
	private void loadElementsFromAST(AOJProject project) {
		requiredClazzes = new ArrayList<>();
		typeLiteralClazzes = new ArrayList<>();

		// para cada classe
		projectClazzes = project.getClasses();

		for (AOJClassDeclaration clazz : projectClazzes) {
			currentClazz = clazz;

			// para cada método
			for (AOJMethodDeclaration method : ((AOJClassDeclaration) clazz).getMembers().getMethods()) {
				searchInStatements(method);
				searchInExpressions(method);
			}

			// para cada construtor
			for (AOJConstructorDeclaration constructor : ((AOJClassDeclaration) clazz).getMembers().getConstructors()) {
				searchInStatements(constructor);
				searchInExpressions(constructor);
			}
		}
	}

	private void searchInExpressions(AOJBehaviourKind method) {
		for (AOJExpression aojExpression : method.getExpressions()) {
			// pega as Expressions de TypeLiteral, ou seja, os Type.class
			if (aojExpression instanceof AOJTypeLiteral) {
				this.add(((AOJTypeLiteral) aojExpression).getType(), typeLiteralClazzes);
			}
		}
	}

	/**
	 * Encontra todos os "new" no código fonte adiciona os Types dos "new"
	 * 
	 * @param clazz
	 * @param method
	 */
	private void searchInStatements(AOJBehaviourKind method) {
		// para cada statement
		for (AOJStatement statement : method.getStatements()) {

			// para cada ExpressionStatement
			if (statement instanceof AOJExpressionStatement) {
				AOJExpressionStatement expressionStatement = ((AOJExpressionStatement) statement);

				// pega os statements de invocação de métodos e ve se tem um new
				if (expressionStatement.getAojExpression() instanceof AOJMethodInvocation) {
					AOJMethodInvocation methodInvocation = (AOJMethodInvocation) expressionStatement.getAojExpression();
					for (Object obj : methodInvocation.getMethodInvocation().arguments()) {
						if (obj instanceof ClassInstanceCreation) {
							this.addIfNotContains(((ClassInstanceCreation) obj).getType(), requiredClazzes);
						}
					}

					// pega os statements de instanciação
				} else if (expressionStatement.getAojExpression() instanceof AOJClassInstanceCreation) {
					AOJClassInstanceCreation cic = (AOJClassInstanceCreation) expressionStatement.getAojExpression();
					this.addIfNotContains(cic.getType(), requiredClazzes);

					// pega os statements de atribuição com invocação
				} else if (expressionStatement.getAojExpression() instanceof AOJAssignment) {
					AOJAssignment assignment = (AOJAssignment) expressionStatement.getAojExpression();
					if (assignment.getRightHandSide() instanceof AOJClassInstanceCreation) {
						AOJClassInstanceCreation cic = (AOJClassInstanceCreation) assignment.getRightHandSide();
						this.addIfNotContains(cic.getType(), requiredClazzes);
					}
				}

				// para cada AOJVariableDeclarationStatement
			} else if (statement instanceof AOJVariableDeclarationStatement) {
				AOJVariableDeclarationStatement variableDeclarationStatement = (AOJVariableDeclarationStatement) statement;

				// pega o método de inicializacao
				if (variableDeclarationStatement.getAojInitializer() instanceof AOJClassInstanceCreation) {
					AOJClassInstanceCreation cic = (AOJClassInstanceCreation) variableDeclarationStatement.getAojInitializer();
					this.addIfNotContains(cic.getType(), requiredClazzes);
				}
			}
		}
	}

	/**
	 * Adiciona um Type caso à lista.
	 * 
	 * @param obj
	 * @param list
	 */
	private void add(Type obj, List<AOJClassDeclaration> list) {
		// pega o Type de acordo com o import
		AOJClassDeclaration declaredClazz = this.getAOJTypeDeclarationFromImport(obj);

		if (declaredClazz != null) {
			// if (isInsideTheSamePackage(currentClazz, declaredClazz) == false) {
			AOJClassDeclaration aojClassDeclaration = ProjectUtils.getAOJClassByFullQualifiedName(declaredClazz.getFullQualifiedName(), projectClazzes);
			if (aojClassDeclaration != null)
				list.add(aojClassDeclaration);
			// }
		}
	}

	/**
	 * Adiciona um Type caso ele já não exista na lista.
	 * 
	 * @param obj
	 * @param list
	 */
	private void addIfNotContains(Type obj, List<AOJClassDeclaration> list) {
		// pega o Type de acordo com o import
		AOJClassDeclaration declaredClazz = this.getAOJTypeDeclarationFromImport(obj);

		if (declaredClazz != null) {
			if (isInsideTheExactSamePackage(currentClazz, declaredClazz) == false) {
				if (isInList(declaredClazz, list) == false) {
					AOJClassDeclaration aojClassDeclaration = ProjectUtils.getAOJClassByFullQualifiedName(declaredClazz.getFullQualifiedName(), projectClazzes);
					if (aojClassDeclaration != null)
						list.add(aojClassDeclaration);
				}
			}
		}
	}

	/**
	 * Verifica se a classe está na lista
	 * 
	 * @param declaredClazz
	 * @param list
	 * @return
	 */
	private boolean isInList(AOJClassDeclaration declaredClazz, List<AOJClassDeclaration> list) {
		if (declaredClazz != null) {
			// para cada classe
			for (AOJClassDeclaration clazz : list) {
				// verifica se existe a classe nas classes
				if (declaredClazz.equals(clazz)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Retorna o AOJClassDeclaration correto do Type informado. Necessário porque o
	 * Type por si só não contém todas as informações necessárias.
	 * 
	 * @param type
	 * @return
	 */
	private AOJClassDeclaration getAOJTypeDeclarationFromImport(Type type) {
		List<AOJImportDeclaration> imports = currentClazz.getCompilationUnit().getImportsDeclaration();

		// pega os imports do arquivo java
		for (AOJImportDeclaration aojImportDeclaration : imports) {
			// se o type da criação estiver no import
			if (aojImportDeclaration.getClassName().equals(type.toString())) {
				// pega as classes do projeto
				for (AOJClassDeclaration aojTypeDeclaration : projectClazzes) {
					// pega a classe correspondente ao type
					if (aojImportDeclaration.qualifiedName().equals(aojTypeDeclaration.getFullQualifiedName())) {
						return aojTypeDeclaration;
					}
				}
			} else {
				// caso esteja em um "*"
				String fullQualifiedName = this.findByWildcard(aojImportDeclaration, type.toString());
				if (fullQualifiedName.isEmpty() == false) {
					for (AOJClassDeclaration aojTypeDeclaration : projectClazzes) {
						// pega a classe correspondente ao type
						if (fullQualifiedName.equals(aojTypeDeclaration.getFullQualifiedName())) {
							return aojTypeDeclaration;
						}
					}
				}
			}
		}

		// caso o Type não está no import, ou seja, está no mesmo nível do package
		// pega as classes do package
		for (AOJContainer aojContainer : currentClazz.getCompilationUnit().getPackage().getTypes()) {
			// se for uma classe
			if (aojContainer instanceof AOJClassDeclaration) {
				AOJClassDeclaration aojTypeDeclaration = (AOJClassDeclaration) aojContainer;
				if (aojTypeDeclaration.getName().equals(type.toString())) {
					return aojTypeDeclaration;
				}
			}
		}

		// se não faz parte do projeto (Object por exemplo)
		return null;
	}

	private String findByWildcard(AOJImportDeclaration imp, String name) {
		if (imp.getCode().indexOf("*") != -1) { // there's not * on import declaration
			String fullQualifiedName = imp.qualifiedName().replace("*", name); // replace * by type name
			// if (forClass (fullQualifiedName)) { // try to find by classloader
			try {
				if (fileExists(fullQualifiedName)) {
					return fullQualifiedName;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "";
	}

	private boolean fileExists(String fullQualifiedName) throws Exception {
		if (currentProject.getResourceProject().isNatureEnabled("org.eclipse.m2e.core.maven2Nature")) {
			IFile file = currentProject.getResourceProject().getFile("/src/main/java/" + fullQualifiedName.replace(".", "/") + ".java");
			return file.exists();
		} else if (currentProject.getResourceProject().isNatureEnabled("org.eclipse.jdt.core.javanature")) {
			return forClass(fullQualifiedName);
		}

		throw new Exception("Natureza não disponível");
	}

	private boolean forClass(String fullQualifiedName) {
		boolean result = true;
		try {
			Class.forName(fullQualifiedName);
		} catch (ClassNotFoundException e) {
			result = false;
		}
		return result;
	}

	/**
	 * Verifica se duas classes estão dentro de um mesmo package. Dentro significa
	 * qualquer package dentro do package superior a partir da superclasse mais alta
	 * da família.
	 * 
	 * @return
	 */
	private boolean isInsideTheSamePackage(AOJClassDeclaration type, AOJClassDeclaration subtype) {
		String subtypeName = subtype.getCompilationUnit().getPackage().getName();
		String typeName = type.getCompilationUnit().getPackage().getName();

		return subtypeName.startsWith(typeName);
	}

	/**
	 * Mesma coisa que o isInsideTheSamePackage, mas não em toda a hierarquia, mas
	 * sim no exato pacote
	 * 
	 * @param type
	 * @param subtype
	 * @return
	 */
	private boolean isInsideTheExactSamePackage(AOJClassDeclaration type, AOJClassDeclaration subtype) {
		String subtypeName = subtype.getCompilationUnit().getPackage().getName();
		String typeName = type.getCompilationUnit().getPackage().getName();

		return subtypeName.equalsIgnoreCase(typeName);
	}

	/**
	 * Obtém uma lista das super classes das classes requeridas
	 * 
	 * @param clazzes
	 * @return
	 */
	private List<AOJClassDeclaration> getParentClasses() {
		List<AOJClassDeclaration> result = new ArrayList<>();

		for (AOJClassDeclaration clazz : requiredClazzes) {
			AOJClassDeclaration superType = ProjectUtils.getAOJClassByFullQualifiedName(clazz.getSuperType().getFullQualifiedName(), projectClazzes);

			if (superType != null) {
				if (result.contains(superType) == false) {
					result.add(superType);
				}
			}
		}

		return result;
	}

	/**
	 * Obtém uma lista das dos supertypes raiz
	 * 
	 * @param clazzes
	 * @return
	 */
	private List<AOJClassDeclaration> getRootSuperTypes() {
		List<AOJClassDeclaration> result = new ArrayList<>();

		for (AOJClassDeclaration clazz : requiredClazzes) {
			AOJClassDeclaration rootClass = extractRootClass(clazz);

			if (result.contains(rootClass) == false) {
				result.add(rootClass);
			}
		}

		return result;
	}

	private AOJClassDeclaration extractRootClass(AOJClassDeclaration aojClassDeclaration) {
		AOJClassDeclaration superType = ProjectUtils.getAOJClassByFullQualifiedName(aojClassDeclaration.getSuperType().getFullQualifiedName(), projectClazzes);
		if (superType != null) {
			return extractRootClass(superType);
		}
		return aojClassDeclaration;
	}
}