package br.ufsm.aopjungle.patternrefactoring;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.ufsm.aopjungle.metamodel.commons.AOJBehaviourKind;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;

public class RefOppToReplaceImplicitTreeWithComposite extends RefactoringOpportunity {

	@XStreamOmitField
	private AOJBehaviourKind method;
	private List<String> tags;
	private double quantityOfTags;
	private double resultQuantityOfTags;
	private double quantityOfClosedTags;
	private double resultQuantityOfClosedTags;
	private double sumOfRelevanceTags;
	private double resultRelevanceTags;

	public RefOppToReplaceImplicitTreeWithComposite(double value,
			AOJClassDeclaration clazz,
			AOJBehaviourKind method,
			List<String> tags,
			double quantityOfTags,
			double resultQuantityOfTags,
			double quantityOfClosedTags,
			double resultQuantityOfClosedTags) {
		super(value, clazz);
		this.method = method;
		this.tags = tags;
		this.quantityOfTags = quantityOfTags;
		this.resultQuantityOfTags = resultQuantityOfTags;
		this.quantityOfClosedTags = quantityOfClosedTags;
		this.resultQuantityOfClosedTags = resultQuantityOfClosedTags;
	}

	public AOJBehaviourKind getMethod() {
		return method;
	}

	public void setMethod(AOJBehaviourKind method) {
		this.method = method;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public double getQuantityOfTags() {
		return quantityOfTags;
	}

	public void setQuantityOfTags(double quantityOfTags) {
		this.quantityOfTags = quantityOfTags;
	}

	public double getQuantityOfClosedTags() {
		return quantityOfClosedTags;
	}

	public void setQuantityOfClosedTags(double quantityOfClosedTags) {
		this.quantityOfClosedTags = quantityOfClosedTags;
	}

	public double getResultQuantityOfTags() {
		return resultQuantityOfTags;
	}

	public void setResultQuantityOfTags(double resultQuantityOfTags) {
		this.resultQuantityOfTags = resultQuantityOfTags;
	}

	public double getResultQuantityOfClosedTags() {
		return resultQuantityOfClosedTags;
	}

	public void setResultQuantityOfClosedTags(double resultQuantityOfClosedTags) {
		this.resultQuantityOfClosedTags = resultQuantityOfClosedTags;
	}

	public double getSumOfRelevanceTags() {
		return sumOfRelevanceTags;
	}

	public void setSumOfRelevanceTags(double sumOfRelevanceTags) {
		this.sumOfRelevanceTags = sumOfRelevanceTags;
	}

	public double getResultRelevanceTags() {
		return resultRelevanceTags;
	}

	public void setResultRelevanceTags(double resultRelevanceTags) {
		this.resultRelevanceTags = resultRelevanceTags;
	}

}
