package br.ufsm.aopjungle.patternrefactoring;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.Assignment;
import org.aspectj.org.eclipse.jdt.core.dom.CharacterLiteral;
import org.aspectj.org.eclipse.jdt.core.dom.Expression;
import org.aspectj.org.eclipse.jdt.core.dom.IfStatement;
import org.aspectj.org.eclipse.jdt.core.dom.InfixExpression;
import org.aspectj.org.eclipse.jdt.core.dom.MethodInvocation;
import org.aspectj.org.eclipse.jdt.core.dom.Name;
import org.aspectj.org.eclipse.jdt.core.dom.Statement;
import org.aspectj.org.eclipse.jdt.core.dom.StringLiteral;

import br.ufsm.aopjungle.astor.CodeSmellImpl;
import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.commons.AOJBehaviourKind;
import br.ufsm.aopjungle.metamodel.commons.AOJExpression;
import br.ufsm.aopjungle.metamodel.commons.AOJMethodDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJIfStatement;
import br.ufsm.aopjungle.metamodel.java.AOJStringLiteral;
import br.ufsm.aopjungle.patternrefactoring.util.Normalizador;
import br.ufsm.aopjungle.patternrefactoring.util.PRProp;
import br.ufsm.aopjungle.views.AOPJungleMonitor;

public class ReplaceImplicitTreeWithComposite extends CodeSmellImpl {

	private AOJProject currentProject;
	private AOJClassDeclaration currentClazz;
	private AOJBehaviourKind currentMethod;
	private List<String> currentMethodTags;
	private Map<String, Tag> projectTags;
	private Map<String, List<Tag>> projectsTags;
	private Map<String, List<RefOppToReplaceImplicitTreeWithComposite>> refactoringOpportunities;
	private List<RefOppClassCandidate> clazzCandidates;
	private boolean print;

	public ReplaceImplicitTreeWithComposite() {
		refactoringOpportunities = new HashMap<>();
		projectsTags = new HashMap<>();
	}

	public void reRun(String projectName) {
		// n�o funciona
		//AOPJungleMonitor.smellEngineMonitor = AOPJungleMonitor.smellEnginesMonitor.split(1).setWorkRemaining(1);
		//AOPJungleMonitor.smellEngineMonitor.subTask("Finding opportunities in project " + projectName);
		
		List<RefOppToReplaceImplicitTreeWithComposite> projectRefactoringOpp = refactoringOpportunities.get(projectName);
		List<Tag> projectTags = projectsTags.get(projectName);
		double weight1 = PRProp.getRITWCWeight1(), weight2 = PRProp.getRITWCWeight2(), weight3 = PRProp.getRITWCWeight3();

		for (RefOppToReplaceImplicitTreeWithComposite ritwc : projectRefactoringOpp) {
			/*int quantityOfTags = 0;
			for (String methodTag : ritwc.getTags()) {
				for (Tag projectTag : projectTags) {
					if (projectTag.getTag().compareTo(methodTag) == 0 && projectTag.isSelected()) {
						quantityOfTags++;
						break;
					}
				}
			}
			double resultQuantityOfTags = Normalizador.normalizarRetilineo(quantityOfTags, PRProp.getRITWCQuantityOfTags());
			ritwc.setQuantityOfTags(quantityOfTags);
			ritwc.setResultQuantityOfTags(resultQuantityOfTags);*/
			
			double quantityOfClosedTags = countClosedTags2(new ArrayList<>(ritwc.getTags()), projectTags);
			double resultQuantityOfClosedTags = Normalizador.normalizarRetilineo(quantityOfClosedTags, PRProp.getRITWCQuantityOfClosedTags());
			ritwc.setQuantityOfClosedTags(quantityOfClosedTags);
			ritwc.setResultQuantityOfClosedTags(resultQuantityOfClosedTags);
			
			ritwc.setValue(ritwc.getValue() - ritwc.getResultRelevanceTags() * weight3);
			int sumOfRelevanceTags = 0;
			for (String roTag : ritwc.getTags()) {
				if (roTag.charAt(0) != '/')
					for (Tag projectTag : projectTags) {
						if (roTag.compareTo(projectTag.getTag()) == 0 && projectTag.isSelected())
							sumOfRelevanceTags += projectTag.getQuantidade();
					}
			}
			ritwc.setSumOfRelevanceTags(sumOfRelevanceTags);
			double resultRelevanceTags = Normalizador.normalizarRetilineo(sumOfRelevanceTags, PRProp.getRITWCRelevanceTags());
			ritwc.setResultRelevanceTags(resultRelevanceTags);
			
			double result = ritwc.getResultQuantityOfTags() * weight1 + resultQuantityOfClosedTags * weight2 + resultRelevanceTags * weight3;
			ritwc.setValue(result);
		}

		// ordena as oportunidades de refatora��o
		Collections.sort(projectRefactoringOpp);
		// deixa em ordem descendente
		Collections.reverse(projectRefactoringOpp);

		clearReportOfProject(projectName);
		int i = 1;
		for (RefOppToReplaceImplicitTreeWithComposite ro : projectRefactoringOpp) {
			ro.setPosition(i++);

			String message = "Refactor to Composite: " + ro.getMethod().getName() + "...";
			ro.setMessage(message);

			registerReport(ro, ro.getClazz());
		}
	}

	@Override
	public void run() {
		print = true;
		AOPJungleMonitor.smellEngineMonitor = AOPJungleMonitor.smellEnginesMonitor.split(1).setWorkRemaining(AOJWorkspace.getInstance().getProjects().size());
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) {
			AOPJungleMonitor.smellEngineMonitor.subTask("Finding opportunities in project " + project.getName());
			currentProject = project;

			clazzCandidates = new ArrayList<>();
			List<RefOppToReplaceImplicitTreeWithComposite> projectRefactoringOpp = new ArrayList<RefOppToReplaceImplicitTreeWithComposite>();

			this.loadElementsFromAST(project);

			this.findRefactoringOpportunities(projectRefactoringOpp);

			// ordena as oportunidades de refatora��o
			Collections.sort(projectRefactoringOpp);
			// deixa em ordem descendente
			Collections.reverse(projectRefactoringOpp);

			int i = 1;
			for (RefOppToReplaceImplicitTreeWithComposite ro : projectRefactoringOpp) {
				ro.setPosition(i++);

				String message = "Refactor to Composite: " + ro.getMethod().getName() + "...";
				ro.setMessage(message);

				registerReport(ro, ro.getClazz());
			}

			projectsTags.put(project.getName(), new ArrayList<>(projectTags.values()));

			refactoringOpportunities.put(project.getName(), projectRefactoringOpp);
		}
	}

	private void findRefactoringOpportunities(List<RefOppToReplaceImplicitTreeWithComposite> projectRefactoringOpp) {
		double weight1 = PRProp.getRITWCWeight1(), weight2 = PRProp.getRITWCWeight2(), weight3 = PRProp.getRITWCWeight3();
		projectTags = new HashMap<String, Tag>();
		for (RefOppClassCandidate classCandidate : clazzCandidates) {
			currentClazz = classCandidate.getClazz();
			for (RefOppMethodCandidate methodCandidate : classCandidate.getMethods()) {
				currentMethod = methodCandidate.getMethod();

				currentMethodTags = getTags(methodCandidate.getMethod());
				double quantityOfTags = currentMethodTags.size();
				if (quantityOfTags > 1) {
					double quantityOfClosedTags = countClosedTags(new ArrayList<>(currentMethodTags));
					if (quantityOfClosedTags > 1) {
						this.countTags();

						double resultQuantityOfTags = Normalizador.normalizarRetilineo(quantityOfTags, PRProp.getRITWCQuantityOfTags());
						double resultQuantityOfClosedTags = Normalizador.normalizarRetilineo(quantityOfClosedTags, PRProp.getRITWCQuantityOfClosedTags());

						double result = resultQuantityOfTags * weight1 + resultQuantityOfClosedTags * weight2;

						projectRefactoringOpp.add(new RefOppToReplaceImplicitTreeWithComposite(result, classCandidate.getClazz(), methodCandidate.getMethod(), currentMethodTags, quantityOfTags,
								resultQuantityOfTags, quantityOfClosedTags, resultQuantityOfClosedTags));
					}
				}
			}
		}

		for (RefOppToReplaceImplicitTreeWithComposite ritwc : projectRefactoringOpp) {
			int sumOfRelevanceTags = 0;
			for (String tag : ritwc.getTags()) {
				if (!(tag.charAt(0) == '/'))
					sumOfRelevanceTags += projectTags.get(tag).getQuantidade();
			}
			ritwc.setSumOfRelevanceTags(sumOfRelevanceTags);
			double resultRelevanceTags = Normalizador.normalizarRetilineo(sumOfRelevanceTags, PRProp.getRITWCRelevanceTags());
			ritwc.setResultRelevanceTags(resultRelevanceTags);
			ritwc.setValue(ritwc.getValue() + resultRelevanceTags * weight3);
		}
	}

	/*
	 * private void sortTags() { // ordena as tags List<Entry<String, Tag>> tags =
	 * new ArrayList<>(projectTags.entrySet()); Collections.sort(tags, new
	 * Comparator<Entry<String, Tag>>() { public int compare(Entry<String, Tag> o1,
	 * Entry<String, Tag> o2) { return
	 * Integer.compare(o2.getValue().getQuantidade(),
	 * o1.getValue().getQuantidade()); }; }); }
	 */

	private void countTags() {
		for (String tag : currentMethodTags) {
			if (tag.charAt(0) != '/') {
				if (projectTags.containsKey(tag))
					projectTags.get(tag).setQuantidade(projectTags.get(tag).getQuantidade() + 1);
				else
					projectTags.put(tag, new Tag(true, tag, 1));
			}
		}
	}

	private List<String> getTags(AOJBehaviourKind method) {
		List<String> tags = new ArrayList<>();
		for (AOJExpression expression : method.getExpressions()) {
			if (expression instanceof AOJStringLiteral) {
				AOJStringLiteral aojStringLiteral = (AOJStringLiteral) expression;
				if (aojStringLiteral.getExpression() instanceof StringLiteral) {
					StringLiteral stringLiteral = (StringLiteral) aojStringLiteral.getExpression();
					tags.addAll(extractTags(stringLiteral.getEscapedValue(), stringLiteral));
				} else if (aojStringLiteral.getExpression() instanceof CharacterLiteral) {
					CharacterLiteral charLiteral = (CharacterLiteral) aojStringLiteral.getExpression();
					if (charLiteral.getParent() instanceof InfixExpression && charLiteral.charValue() == '<') {
						InfixExpression infixExpression = (InfixExpression) charLiteral.getParent();
						Expression exp = nextExpression(charLiteral, infixExpression);
						if (exp != null) {
							if (exp instanceof StringLiteral)
								tags.add(extractTag(((StringLiteral) exp).getEscapedValue()));
							else if (exp instanceof Name)
								tags.add(((Name) exp).getFullyQualifiedName());
						}
					}
				}
			}
		}
		return tags;
	}

	private int countClosedTags(List<String> tags) {
		int count = 0, remove1 = -1, remove2 = -1;
		boolean brek = false;
		for (int i = 0; i < tags.size(); i++) {
			if (tags.get(i).charAt(0) == '/') {
				if (i > 0) {
					for (int j = i - 1; j >= 0; j--) {
						if (tags.get(i).substring(1).compareTo(tags.get(j)) == 0) {
							count++;
							brek = true;
							remove1 = i;
							remove2 = j;
							break;
						}
					}
				}
			}
			if (brek)
				break;
		}
		if (remove1 != -1) {
			tags.remove(remove1);
			tags.remove(remove2);
			return count + countClosedTags(tags);
		} else
			return count;
	}
	
	private int countClosedTags2(List<String> tags, List<Tag> projectTags) {
		int count = 0, remove1 = -1, remove2 = -1;
		boolean brek = false;
		for (int i = 0; i < tags.size(); i++) {
			if (tags.get(i).charAt(0) == '/') {
				if (i > 0) {
					for (int j = i - 1; j >= 0; j--) {
						if (tags.get(i).substring(1).compareTo(tags.get(j)) == 0) {
							if (isSelected(tags.get(j), projectTags))
								count++;
							brek = true;
							remove1 = i;
							remove2 = j;
							break;
						}
					}
				}
			}
			if (brek)
				break;
		}
		if (remove1 != -1) {
			tags.remove(remove1);
			tags.remove(remove2);
			return count + countClosedTags2(tags, projectTags);
		} else
			return count;
	}
	
	private boolean isSelected(String tag, List<Tag> projectTags) {
		for (Tag projectTag : projectTags) {
			if (projectTag.getTag().compareTo(tag) == 0)
				return projectTag.isSelected();
		}
		return false;
	}

	private List<String> extractTags(String extendedTags, StringLiteral stringLiteral) {
		List<String> tags = new ArrayList<>();
		int inicio = extendedTags.indexOf("<");
		if (inicio != -1) {
			inicio++;
			String tag = extractTag(extendedTags.substring(inicio));
			if (tag.compareTo("") == 0) {
				if (stringLiteral.getParent() instanceof InfixExpression) {
					Expression expression = nextExpression(stringLiteral, (InfixExpression) stringLiteral.getParent());
					if (expression != null) {
						if (expression instanceof StringLiteral) {
							String t = extractTag(((StringLiteral) expression).getEscapedValue());
							if (!(t.compareTo("\"") == 0))
								tags.add(t);
						} else if (expression instanceof Name) {
							tags.add(((Name) expression).getFullyQualifiedName());
						}
					}
				}
			} else if (tag.compareTo("\"") == 0 || tag.compareTo("/\"") == 0) {
				String closeChar = tag.compareTo("/\"") == 0 ? "/" : "";
				if (stringLiteral.getParent() instanceof InfixExpression) {
					Expression expression = nextExpression(stringLiteral, (InfixExpression) stringLiteral.getParent());
					if (expression != null) {
						if (expression instanceof StringLiteral) {
							String t = extractTag(closeChar + ((StringLiteral) expression).getEscapedValue());
							tags.add(t);
						} else if (expression instanceof Name) {
							tags.add(closeChar + ((Name) expression).getFullyQualifiedName());
						}
					}
				}
			} else if (tag.charAt(0) == '=') {
				// IGNORA as tags com '='
			} else
				tags.add(tag);

			tags.addAll(extractTags(extendedTags.substring(inicio + tag.length()), stringLiteral));
		}
		return tags;
	}

	private String extractTag(String extendedTag) {
		String tag = "";
		for (int i = 0; i < extendedTag.length(); i++) {
			if (extendedTag.charAt(i) == ' ' || extendedTag.charAt(i) == '>')
				break;

			tag += extendedTag.charAt(i);
		}
		return tag;
	}

	private Expression nextExpression(Expression tag, InfixExpression infixExpression) {
		if (infixExpression.getLeftOperand().equals(tag))
			return infixExpression.getRightOperand();
		else if (infixExpression.getRightOperand().equals(tag) && infixExpression.extendedOperands().size() > 0)
			return (Expression) infixExpression.extendedOperands().get(0);
		else
			for (int i = 0; i < infixExpression.extendedOperands().size(); i++)
				if (infixExpression.extendedOperands().get(i).equals(tag) && i < infixExpression.extendedOperands().size() - 1)
					return (Expression) infixExpression.extendedOperands().get(i + 1);

		return null;
	}

	private double countSpecialChar(AOJBehaviourKind method) {
		int count = 0;
		for (AOJExpression expression : method.getExpressions()) {
			if (expression instanceof AOJStringLiteral)
				count++;
		}
		return count;
	}

	private void loadElementsFromAST(AOJProject project) {
		for (AOJClassDeclaration clazz : project.getClasses()) {
			List<RefOppMethodCandidate> methods = new ArrayList<>();
			currentClazz = clazz;

			// para cada m�todo
			for (AOJMethodDeclaration method : ((AOJClassDeclaration) clazz).getMembers().getMethods()) {
				if (searchInExpressions(method))
					methods.add(new RefOppMethodCandidate(method));
			}

			// para cada construtor
			for (AOJConstructorDeclaration constructor : ((AOJClassDeclaration) clazz).getMembers().getConstructors()) {
				if (searchInExpressions(constructor))
					methods.add(new RefOppMethodCandidate(constructor));
			}

			this.clazzCandidates.add(new RefOppClassCandidate(clazz, methods));
		}
	}

	private boolean searchInExpressions(AOJBehaviourKind method) {
		// para cada statement
		for (AOJExpression expression : method.getExpressions()) {
			// para cada ExpressionStatement
			if (expression instanceof AOJStringLiteral)
				return true;
		}
		return false;
	}

	private ASTNode extractStatement(ASTNode node) {
		if (node.getParent() instanceof Statement) {
			return node;
		} else {
			return extractStatement(node.getParent());
		}
	}

	private String extractVariable(ASTNode node) {
		try {
			if (node instanceof Assignment) {
				String variable = ((Name) ((Assignment) node).getLeftHandSide()).getFullyQualifiedName();
				// print(variable);
				return variable;
			} else if (node instanceof MethodInvocation) {
				if (((MethodInvocation) node).getExpression() instanceof MethodInvocation) {
					extractVariable(((MethodInvocation) node).getExpression());
				} else {
					String variable = ((Name) ((MethodInvocation) node).getExpression()).getFullyQualifiedName();
					// print(variable);
					return variable;
				}
			}

			// nem sempre h� vari�vel
		} catch (ClassCastException e) {
			/*
			 * print("Exception message: " + e.getMessage()); print("Cast Exception: " +
			 * node.toString()); print(node.getClass().toString());
			 */
		} catch (Exception e) {
			/*
			 * print("Exception message: " + e.getMessage()); print("Exception: " +
			 * node.toString()); print(node.getClass().toString());
			 */
		}
		return null;
	}

	private void addIfNotContains(AOJIfStatement ifStatementCandidate, List<AOJIfStatement> aojIfStatements) {
		boolean exists = false;
		for (AOJIfStatement aojIfStatement : aojIfStatements) {
			Statement statement = aojIfStatement.getIfStatement().getElseStatement();
			if (statement != null && statement instanceof IfStatement) {
				exists = statement.toString().contains(ifStatementCandidate.getIfStatement().toString());
			} else
				exists = true;
		}
		if (exists == false) {
			aojIfStatements.add(ifStatementCandidate);
		}
	}

	public Map<String, List<Tag>> getProjectsTags() {
		return projectsTags;
	}

	@Override
	public String getLabel() {
		return "Replace Implicit Tree With Composite";
	}

	private void print(String message) {
		if (print)
			System.out.println(message);
	}
}
