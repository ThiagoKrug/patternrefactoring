package br.ufsm.aopjungle.patternrefactoring;

public class Tag implements Comparable<Tag> {

	private boolean selected;
	private String tag;
	private int quantidade;

	public Tag(boolean selected, String tag, int quantidade) {
		this.selected = selected;
		this.tag = tag;
		this.quantidade = quantidade;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public int compareTo(Tag otherTag) {
		return this.getTag().compareTo(otherTag.getTag());
	}

}
