package br.ufsm.aopjungle.patternrefactoring.util;

import org.aspectj.org.eclipse.jdt.core.dom.AssertStatement;
import org.aspectj.org.eclipse.jdt.core.dom.Block;
import org.aspectj.org.eclipse.jdt.core.dom.BreakStatement;
import org.aspectj.org.eclipse.jdt.core.dom.CatchClause;
import org.aspectj.org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.aspectj.org.eclipse.jdt.core.dom.ContinueStatement;
import org.aspectj.org.eclipse.jdt.core.dom.DoStatement;
import org.aspectj.org.eclipse.jdt.core.dom.EmptyStatement;
import org.aspectj.org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.aspectj.org.eclipse.jdt.core.dom.ExpressionStatement;
import org.aspectj.org.eclipse.jdt.core.dom.ForStatement;
import org.aspectj.org.eclipse.jdt.core.dom.IfStatement;
import org.aspectj.org.eclipse.jdt.core.dom.LabeledStatement;
import org.aspectj.org.eclipse.jdt.core.dom.ReturnStatement;
import org.aspectj.org.eclipse.jdt.core.dom.Statement;
import org.aspectj.org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.aspectj.org.eclipse.jdt.core.dom.SwitchCase;
import org.aspectj.org.eclipse.jdt.core.dom.SwitchStatement;
import org.aspectj.org.eclipse.jdt.core.dom.SynchronizedStatement;
import org.aspectj.org.eclipse.jdt.core.dom.ThrowStatement;
import org.aspectj.org.eclipse.jdt.core.dom.TryStatement;
import org.aspectj.org.eclipse.jdt.core.dom.TypeDeclarationStatement;
import org.aspectj.org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.aspectj.org.eclipse.jdt.core.dom.WhileStatement;

public class StatementsCounter {

	public static int countStatements(Statement statement) throws Exception {
		/*
		 * AssertStatement Block BreakStatement ConstructorInvocation ContinueStatement
		 * DoStatement EmptyStatement EnhancedForStatement ExpressionStatement
		 * ForStatement IfStatement LabeledStatement ReturnStatement
		 * SuperConstructorInvocation SwitchCase SwitchStatement SynchronizedStatement
		 * ThrowStatement TryStatement TypeDeclarationStatement
		 * VariableDeclarationStatement WhileStatement 22 statements
		 */
		if (statement == null)
			return 0;

		if (statement instanceof EmptyStatement)
			return 0;

		if (statement instanceof AssertStatement 
				|| statement instanceof BreakStatement 
				|| statement instanceof ConstructorInvocation 
				|| statement instanceof ContinueStatement
				|| statement instanceof ExpressionStatement 
				|| statement instanceof ReturnStatement 
				|| statement instanceof SuperConstructorInvocation 
				|| statement instanceof SwitchCase
				|| statement instanceof VariableDeclarationStatement
				|| statement instanceof SynchronizedStatement 
				|| statement instanceof ThrowStatement)
			return 1;

		if (statement instanceof Block) {
			int counter = 0;
			for (Object s : ((Block) statement).statements())
				counter += countStatements((Statement) s);
			return counter;
		}

		if (statement instanceof DoStatement)
			// ' 2 + ' se refere ao 'do {' e ao 'while (Expression);'
			return 2 + countStatements(((DoStatement) statement).getBody());

		if (statement instanceof EnhancedForStatement) {
			Statement bodyStatement = ((EnhancedForStatement) statement).getBody();
			// '1 + ' se refere ao for
			int count = 1 + countStatements(bodyStatement);
			if (bodyStatement instanceof Block)
				if (((Block) bodyStatement).statements().size() > 1)
					// '1 + ' caso tenha mais de uma linha, precisa do fecha chave
					return 1 + count;
			return count;
		}

		if (statement instanceof ForStatement) {
			Statement bodyStatement2 = ((ForStatement) statement).getBody();
			// '1 + ' se refere ao for
			int count2 = 1 + countStatements(bodyStatement2);
			if (bodyStatement2 instanceof Block)
				if (((Block) bodyStatement2).statements().size() > 1)
					// '1 + ' caso tenha mais de uma linha, precisa do fecha chave
					return 1 + count2;
			return count2;
		}

		if (statement instanceof IfStatement) {
			// se refere a linha do if
			int linhaDoIf = 1;
			int linhaDoElse = 0;
			int countThen = countStatements(((IfStatement) statement).getThenStatement());
			int countElse = countStatements(((IfStatement) statement).getElseStatement());

			boolean isBlock = ((IfStatement) statement).getElseStatement() instanceof Block;
			boolean isIf = ((IfStatement) statement).getElseStatement() instanceof IfStatement;

			if (countElse >= 1 && !isIf)
				linhaDoElse = 1;

			int fechaChaveAdicional = 0;
			if (countThen <= 1 && linhaDoElse == 0)
				fechaChaveAdicional = 0;
			else if (countThen <= 1 && isBlock && countElse > 1)
				fechaChaveAdicional = 1;
			else if (countThen <= 1 && isBlock && countElse <= 1)
				fechaChaveAdicional = 0;
			else if (countThen > 1 && countElse == 0)
				fechaChaveAdicional = 1;

			int retorno = linhaDoIf + countThen + linhaDoElse + countElse + fechaChaveAdicional;
			return retorno;
		}

		if (statement instanceof LabeledStatement) {
			/*
			 * exemplo de LabeledStatement start: { System.out.println("asdf"); }
			 */
			int count3 = countStatements(((LabeledStatement) statement).getBody());
			if (count3 > 1)
				// '2 + ' se refere ao inicio e ao fecha chave
				return 2 + count3;
			// '1 + ' se refere somente ao inicio do LabeledStatement
			return 1 + count3;
		}

		if (statement instanceof SwitchStatement) {
			int counter = 0;
			for (Object s : ((SwitchStatement) statement).statements())
				// '2 + ' se refere ao inicio e o fecha chave
				counter = 2 + countStatements((Statement) s);
			return counter;
		}

		if (statement instanceof TryStatement) {
			int linhaDoTry = 1;
			int countBody = countStatements(((TryStatement) statement).getBody());

			int countCatches = 0;
			for (Object obj : ((TryStatement) statement).catchClauses())
				// '1 + ' se refere a linha do catch
				countCatches += 1 + countStatements(((CatchClause) obj).getBody());

			int linhaDoFinally = 0;
			int countFinally = countStatements(((TryStatement) statement).getFinally());
			if (countFinally > 0)
				linhaDoFinally = 1;

			int fechaChaveFinal = 1;

			return linhaDoTry + countBody + countCatches + linhaDoFinally + countFinally + fechaChaveFinal;
		}

		if (statement instanceof TypeDeclarationStatement) {
			TypeDeclarationStatement tds = ((TypeDeclarationStatement) statement);
			System.out.println("TypeDeclarationStatement\n" + tds);
		}

		if (statement instanceof WhileStatement) {
			int countWhile = countStatements(((WhileStatement) statement).getBody());
			if (countWhile > 1)
				// '2 + ' se refere ao while e o fecha chave
				return 2 + countWhile;
			// '1 + ' se refere ao while
			return 1 + countWhile;
		}

		throw new Exception("Statement desconhecido!");
	}

}
