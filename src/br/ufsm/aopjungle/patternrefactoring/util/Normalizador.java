package br.ufsm.aopjungle.patternrefactoring.util;

public class Normalizador {
	public static double normalizarRetilineo(double indicio, double threshold) {
		double fator = 1 / threshold;
		return indicio * fator;
	}

	public static double normalizarReflexao(double indicio, double threshold) {
		double fator = 1 / threshold;
		double result = indicio * fator;
		if (result > 1) {
			result = 1 - (result - 1);
		}
		return result;
	}
}
