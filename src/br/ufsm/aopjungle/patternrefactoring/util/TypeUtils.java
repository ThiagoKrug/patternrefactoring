package br.ufsm.aopjungle.patternrefactoring.util;

public class TypeUtils {

	private static final String INTEGER = "java.lang.Integer";

	public static boolean isPrimitiveFromJavaLang(String className) {
		if (className.compareTo(INTEGER) == 0) {
			return true;
		}
		return false;
	}
}
