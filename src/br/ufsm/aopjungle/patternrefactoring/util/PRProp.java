package br.ufsm.aopjungle.patternrefactoring.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import br.ufsm.aopjungle.util.IconProvider;

public class PRProp {
	static class SortedProperties extends Properties {
		private static final long serialVersionUID = 1L;

		@Override
		public Enumeration<Object> keys() {
			Enumeration<Object> keysEnum = super.keys();
			List<String> keyList = new ArrayList<>();
			while (keysEnum.hasMoreElements()) {
				keyList.add((String) keysEnum.nextElement());
			}
			Collections.sort(keyList);
			List<Object> list = new ArrayList<>();
			for (String string : keyList) {
				list.add(string);
			}
			return Collections.enumeration(list);
		}

	}

	private static SortedProperties properties;
	private static final String appPath = IconProvider.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	private static final String defaultPath = appPath + "conf/";
	private static final String fileNameProperties = defaultPath + "pattern-refactoring.properties";

	private static String ECWF = "encapsulateClassWithFactory";
	public static String ECWF_Execute = ECWF + ".execute";
	public static String ECWF_Weight1 = ECWF + ".weight1";
	public static String ECWF_Weight2 = ECWF + ".weight2";
	public static String ECWF_Weight3 = ECWF + ".weight3";
	public static String ECWF_Weight4 = ECWF + ".weight4";
	public static String ECWF_Weight5 = ECWF + ".weight5";
	public static String ECWF_Subtypes = ECWF + ".subtypes";
	public static String ECWF_ClassesInTheSamePackage = ECWF + ".classesInTheSamePackage";
	public static String ECWF_PublicModificators = ECWF + ".publicModificators";
	public static String ECWF_Visitor = ECWF + ".visitor";
	public static String ECWF_SubtypesCalls = ECWF + ".subtypeCalls";

	private static String RCDWC = "replaceConditionalDispatcherWithCommand";
	public static String RCDWC_Execute = RCDWC + ".execute";
	public static String RCDWC_Weight1 = RCDWC + ".weight1";
	public static String RCDWC_Weight2 = RCDWC + ".weight2";
	public static String RCDWC_Weight3 = RCDWC + ".weight3";
	public static String RCDWC_Weight4 = RCDWC + ".weight4";
	public static String RCDWC_Weight5 = RCDWC + ".weight5";
	public static String RCDWC_QuantityOfConditionals = RCDWC + ".quantityOfConditionals";
	public static String RCDWC_QuantityOfLOC = RCDWC + ".quantityOfLOC";
	public static String RCDWC_QuantityOfSimpleExpressions = RCDWC + ".quantityOfSimpleExpressions";
	public static String RCDWC_AverageLOCPerConditional = RCDWC + ".averageLOCPerConditional";
	public static String RCDWC_SimpleConditions = RCDWC + ".simpleConditions";

	private static String RITWC = "replaceImplicitTreeWithComposite";
	public static String RITWC_Execute = RITWC + ".execute";
	public static String RITWC_Weight1 = RITWC + ".weight1";
	public static String RITWC_Weight2 = RITWC + ".weight2";
	public static String RITWC_Weight3 = RITWC + ".weight3";
	public static String RITWC_QuantityOfTags = RITWC + ".quantityOfTags";
	public static String RITWC_QuantityOfClosedTags = RITWC + ".quantityOfClosedTags";
	public static String RITWC_RelevanceTags = RITWC + ".relevanceTags";
	
	private PRProp() {
	}

	public static Properties getProperties() {
		if (properties == null) {
			loadProperties();
		}
		return properties;
	}

	private static void loadProperties() {
		properties = new SortedProperties();
		try {
			properties.load(new FileInputStream(fileNameProperties));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void loadDefaultProperties() {
		properties = new SortedProperties();
		properties.setProperty(ECWF_Execute, "true");
		properties.setProperty(ECWF_Weight1, "1");
		properties.setProperty(ECWF_Weight2, "1");
		properties.setProperty(ECWF_Weight3, "0.7");
		properties.setProperty(ECWF_Weight4, "2");
		properties.setProperty(ECWF_Weight5, "1");
		properties.setProperty(ECWF_Subtypes, "7");
		properties.setProperty(ECWF_ClassesInTheSamePackage, "7");
		properties.setProperty(ECWF_PublicModificators, "6");
		properties.setProperty(ECWF_Visitor, "3");
		properties.setProperty(ECWF_SubtypesCalls, "2");

		properties.setProperty(RCDWC_Execute, "true");
		properties.setProperty(RCDWC_Weight1, "0.1");
		properties.setProperty(RCDWC_Weight2, "0.25");
		properties.setProperty(RCDWC_Weight3, "0.9");
		properties.setProperty(RCDWC_Weight4, "0.5");
		properties.setProperty(RCDWC_Weight5, "1");
		properties.setProperty(RCDWC_QuantityOfConditionals, "5");
		properties.setProperty(RCDWC_QuantityOfLOC, "30");
		properties.setProperty(RCDWC_QuantityOfSimpleExpressions, "5");
		properties.setProperty(RCDWC_AverageLOCPerConditional, "4");
		properties.setProperty(RCDWC_SimpleConditions, "1");
		
		properties.setProperty(RITWC_Execute, "true");
		properties.setProperty(RITWC_Weight1, "1");
		properties.setProperty(RITWC_Weight2, "1");
		properties.setProperty(RITWC_Weight3, "1");
		properties.setProperty(RITWC_QuantityOfTags, "6");
		properties.setProperty(RITWC_QuantityOfClosedTags, "2");
		properties.setProperty(RITWC_RelevanceTags, "1");
	}

	public static void saveProperties() {
		try {
			properties.store(new FileOutputStream(fileNameProperties), "");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static boolean getECWFExecute() {
		return Boolean.parseBoolean(getProperties().getProperty(ECWF_Execute, "true"));
	}

	public static double getECWFWeight1() {
		return Double.parseDouble(getProperties().getProperty(ECWF_Weight1, "1"));
	}

	public static double getECWFWeight2() {
		return Double.parseDouble(getProperties().getProperty(ECWF_Weight2, "1"));
	}

	public static double getECWFWeight3() {
		return Double.parseDouble(getProperties().getProperty(ECWF_Weight3, "0.7"));
	}

	public static double getECWFWeight4() {
		return Double.parseDouble(getProperties().getProperty(ECWF_Weight4, "2"));
	}

	public static double getECWFWeight5() {
		return Double.parseDouble(getProperties().getProperty(ECWF_Weight5, "1"));
	}

	public static double getECWFSubtypes() {
		return Double.parseDouble(getProperties().getProperty(ECWF_Subtypes, "7"));
	}

	public static double getECWFClassesInTheSamePackage() {
		return Double.parseDouble(getProperties().getProperty(ECWF_ClassesInTheSamePackage, "7"));
	}

	public static double getECWFPublicModificators() {
		return Double.parseDouble(getProperties().getProperty(ECWF_PublicModificators, "6"));
	}

	public static double getECWFVisitor() {
		return Double.parseDouble(getProperties().getProperty(ECWF_Visitor, "3"));
	}

	public static double getECWFSubtypesCalls() {
		return Double.parseDouble(getProperties().getProperty(ECWF_SubtypesCalls, "2"));
	}

	public static boolean getRCDWCExecute() {
		return Boolean.parseBoolean(getProperties().getProperty(RCDWC_Execute, "true"));
	}

	public static double getRCDWCWeight1() {
		return Double.parseDouble(getProperties().getProperty(RCDWC_Weight1, "0.1"));
	}

	public static double getRCDWCWeight2() {
		return Double.parseDouble(getProperties().getProperty(RCDWC_Weight2, "0.25"));
	}

	public static double getRCDWCWeight3() {
		return Double.parseDouble(getProperties().getProperty(RCDWC_Weight3, "0.9"));
	}

	public static double getRCDWCWeight4() {
		return Double.parseDouble(getProperties().getProperty(RCDWC_Weight4, "0.5"));
	}

	public static double getRCDWCWeight5() {
		return Double.parseDouble(getProperties().getProperty(RCDWC_Weight5, "1"));
	}

	public static double getRCDWCQuantityOfConditionals() {
		return Double.parseDouble(getProperties().getProperty(RCDWC_QuantityOfConditionals, "5"));
	}

	public static double getRCDWCQuantityOfLOC() {
		return Double.parseDouble(getProperties().getProperty(RCDWC_QuantityOfLOC, "30"));
	}

	public static double getRCDWCQuantityOfSimpleExpressions() {
		return Double.parseDouble(getProperties().getProperty(RCDWC_QuantityOfSimpleExpressions, "5"));
	}

	public static double getRCDWCAverageLOCPerConditional() {
		return Double.parseDouble(getProperties().getProperty(RCDWC_AverageLOCPerConditional, "4"));
	}

	public static double getRCDWCSimpleConditions() {
		return Double.parseDouble(getProperties().getProperty(RCDWC_SimpleConditions, "1"));
	}
	
	public static boolean getRITWCExecute() {
		return Boolean.parseBoolean(getProperties().getProperty(RITWC_Execute, "true"));
	}
	
	public static double getRITWCWeight1() {
		return Double.parseDouble(getProperties().getProperty(RITWC_Weight1, "1"));
	}

	public static double getRITWCWeight2() {
		return Double.parseDouble(getProperties().getProperty(RITWC_Weight2, "1"));
	}

	public static double getRITWCWeight3() {
		return Double.parseDouble(getProperties().getProperty(RITWC_Weight3, "1"));
	}
	
	public static double getRITWCQuantityOfTags() {
		return Double.parseDouble(getProperties().getProperty(RITWC_QuantityOfTags, "6"));
	}
	
	public static double getRITWCQuantityOfClosedTags() {
		return Double.parseDouble(getProperties().getProperty(RITWC_QuantityOfClosedTags, "2"));
	}
	
	public static double getRITWCRelevanceTags() {
		return Double.parseDouble(getProperties().getProperty(RITWC_RelevanceTags, "1"));
	}
}
