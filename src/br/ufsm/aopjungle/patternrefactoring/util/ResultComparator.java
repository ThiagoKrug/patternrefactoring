package br.ufsm.aopjungle.patternrefactoring.util;

import java.util.Comparator;

import br.ufsm.aopjungle.patternrefactoring.RefOppToEncapsulateClassesWithFactory;

public class ResultComparator implements Comparator<RefOppToEncapsulateClassesWithFactory> {
	@Override
	public int compare(RefOppToEncapsulateClassesWithFactory o1, RefOppToEncapsulateClassesWithFactory o2) {
		if (o1.getValue() == o2.getValue()) {
			return 0;
		} else if (o1.getValue() > o2.getValue()) {
			return 1;
		} else {
			return -1;
		}
	}
}
