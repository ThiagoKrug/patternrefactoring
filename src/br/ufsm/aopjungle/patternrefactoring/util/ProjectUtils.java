package br.ufsm.aopjungle.patternrefactoring.util;

import java.util.List;

import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;

public class ProjectUtils {

	/**
	 * Retorna a AOJClassDeclaration de acordo com o nome da Classe. Busca nas
	 * classes do projeto.
	 * 
	 * @param clazzName
	 * @return
	 */
	public static AOJClassDeclaration getAOJClassByFullQualifiedName(String clazzName, List<AOJClassDeclaration> projectClazzes) {
		for (AOJClassDeclaration aojTypeDeclaration : projectClazzes) {
			if (aojTypeDeclaration.getFullQualifiedName().compareTo(clazzName) == 0) {
				return aojTypeDeclaration;
			}
		}
		return null;
	}
}
