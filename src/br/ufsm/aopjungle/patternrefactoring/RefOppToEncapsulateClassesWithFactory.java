package br.ufsm.aopjungle.patternrefactoring;

import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;

public class RefOppToEncapsulateClassesWithFactory extends RefactoringOpportunity {

	private double numberOfSubTypes;
	private double resultNumberOfSubTypes;
	private double numberOfClassesInSamePackage;
	private double resultNumberOfClassesInSamePackage;
	private double numberOfPublicModificators;
	private double resultNumberOfPublicModificators;
	private double visitor;
	private double resultVisitor;
	private double numberOfSubTypesCalls;
	private double resultNumberOfSubTypesCalls;

	public RefOppToEncapsulateClassesWithFactory(double value, AOJClassDeclaration rootClazz, double numberOfSubTypes, double resultNumberOfSubTypes, double numberOfClassesInSamePackage,
			double resultNumberOfClassesInSamePackage, double numberOfPublicModificators, double resultNumberOfPublicModificators, double visitor, double resultVisitor, double numberOfSubTypesCalls,
			double resultNumberOfSubTypesCalls) {
		super(value, rootClazz);
		this.numberOfSubTypes = numberOfSubTypes;
		this.resultNumberOfSubTypes = resultNumberOfSubTypes;
		this.numberOfClassesInSamePackage = numberOfClassesInSamePackage;
		this.resultNumberOfClassesInSamePackage = resultNumberOfClassesInSamePackage;
		this.numberOfPublicModificators = numberOfPublicModificators;
		this.resultNumberOfPublicModificators = resultNumberOfPublicModificators;
		this.visitor = visitor;
		this.resultVisitor = resultVisitor;
		this.numberOfSubTypesCalls = numberOfSubTypesCalls;
		this.resultNumberOfSubTypesCalls = resultNumberOfSubTypesCalls;
	}

	public double getResultNumberOfSubTypes() {
		return resultNumberOfSubTypes;
	}

	public void setResultNumberOfSubTypes(double resultNumberOfSubTypes) {
		this.resultNumberOfSubTypes = resultNumberOfSubTypes;
	}

	public double getResultNumberOfClassesInSamePackage() {
		return resultNumberOfClassesInSamePackage;
	}

	public void setResultNumberOfClassesInSamePackage(double resultNumberOfClassesInSamePackage) {
		this.resultNumberOfClassesInSamePackage = resultNumberOfClassesInSamePackage;
	}

	public double getResultNumberOfPublicModificators() {
		return resultNumberOfPublicModificators;
	}

	public void setResultNumberOfPublicModificators(double resultNumberOfPublicModficators) {
		this.resultNumberOfPublicModificators = resultNumberOfPublicModficators;
	}

	public double getResultNumberOfSubTypesCalls() {
		return resultNumberOfSubTypesCalls;
	}

	public void setResultNumberOfSubTypesCalls(double resultNumberOfSubTypesCalls) {
		this.resultNumberOfSubTypesCalls = resultNumberOfSubTypesCalls;
	}

	public double getNumberOfSubTypes() {
		return numberOfSubTypes;
	}

	public void setNumberOfSubTypes(double numberOfSubTypes) {
		this.numberOfSubTypes = numberOfSubTypes;
	}

	public double getNumberOfClassesInSamePackage() {
		return numberOfClassesInSamePackage;
	}

	public void setNumberOfClassesInSamePackage(double numberOfClassesInSamePackage) {
		this.numberOfClassesInSamePackage = numberOfClassesInSamePackage;
	}

	public double getNumberOfPublicModificators() {
		return numberOfPublicModificators;
	}

	public void setNumberOfPublicModificators(double numberOfPublicModificators) {
		this.numberOfPublicModificators = numberOfPublicModificators;
	}

	public double getVisitor() {
		return visitor;
	}

	public void setVisitor(double visitor) {
		this.visitor = visitor;
	}

	public double getResultVisitor() {
		return resultVisitor;
	}

	public void setResultVisitor(double resultVisitor) {
		this.resultVisitor = resultVisitor;
	}

	public double getNumberOfSubTypesCalls() {
		return numberOfSubTypesCalls;
	}

	public void setNumberOfSubTypesCalls(double numberOfSubTypesCalls) {
		this.numberOfSubTypesCalls = numberOfSubTypesCalls;
	}

	/*
	 * @Override public String toString() { NumberFormat formatter = new
	 * DecimalFormat("#.###"); String s = formatter.format(value); return s + " : "
	 * + rootClazz.getFullQualifiedName(); }
	 */

	public String toStringComplete() {
		String s = "\n" + this.getClazz().getFullQualifiedName();
		s += "\nPosition #: " + this.getPosition();
		s += "\nNumber Of Subtypes: " + this.numberOfSubTypes;
		s += "\nResult Of Subtypes: " + this.resultNumberOfSubTypes;
		s += "\nNumber Of Classes in the Same Package: " + this.numberOfClassesInSamePackage;
		s += "\nResult Of Classes in the Same Package: " + this.resultNumberOfClassesInSamePackage;
		s += "\nNumber of Public Modificators: " + this.numberOfPublicModificators;
		s += "\nResult of Public Modificators: " + this.resultNumberOfPublicModificators;
		s += "\nVisitor: " + this.visitor;
		s += "\nResult of Visitor: " + this.resultVisitor;
		s += "\nNumber of Subtypes Calls: " + this.numberOfSubTypesCalls;
		s += "\nResult of Subtypes Calls: " + this.resultNumberOfSubTypesCalls;
		s += "\nFinal result: " + this.getValue() + "\n";
		return s;
	}

}
