package br.ufsm.aopjungle.patternrefactoring;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.aspectj.org.eclipse.jdt.core.dom.ASTNode;
import org.aspectj.org.eclipse.jdt.core.dom.ArrayAccess;
import org.aspectj.org.eclipse.jdt.core.dom.BooleanLiteral;
import org.aspectj.org.eclipse.jdt.core.dom.CharacterLiteral;
import org.aspectj.org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.aspectj.org.eclipse.jdt.core.dom.Expression;
import org.aspectj.org.eclipse.jdt.core.dom.FieldAccess;
import org.aspectj.org.eclipse.jdt.core.dom.ForStatement;
import org.aspectj.org.eclipse.jdt.core.dom.IfStatement;
import org.aspectj.org.eclipse.jdt.core.dom.InfixExpression;
import org.aspectj.org.eclipse.jdt.core.dom.InstanceofExpression;
import org.aspectj.org.eclipse.jdt.core.dom.MethodDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.MethodInvocation;
import org.aspectj.org.eclipse.jdt.core.dom.NullLiteral;
import org.aspectj.org.eclipse.jdt.core.dom.NumberLiteral;
import org.aspectj.org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.aspectj.org.eclipse.jdt.core.dom.PrefixExpression;
import org.aspectj.org.eclipse.jdt.core.dom.QualifiedName;
import org.aspectj.org.eclipse.jdt.core.dom.SimpleName;
import org.aspectj.org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.aspectj.org.eclipse.jdt.core.dom.Statement;
import org.aspectj.org.eclipse.jdt.core.dom.StringLiteral;
import org.aspectj.org.eclipse.jdt.core.dom.SuperMethodInvocation;
import org.aspectj.org.eclipse.jdt.core.dom.ThisExpression;
import org.aspectj.org.eclipse.jdt.core.dom.Type;
import org.aspectj.org.eclipse.jdt.core.dom.TypeLiteral;
import org.aspectj.org.eclipse.jdt.core.dom.VariableDeclarationExpression;
import org.aspectj.org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.aspectj.org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.core.IImportDeclaration;
import org.eclipse.jdt.core.JavaModelException;

import br.ufsm.aopjungle.astor.CodeSmellImpl;
import br.ufsm.aopjungle.builder.AOJWorkspace;
import br.ufsm.aopjungle.metamodel.commons.AOJAttributeDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJBehaviourKind;
import br.ufsm.aopjungle.metamodel.commons.AOJContainer;
import br.ufsm.aopjungle.metamodel.commons.AOJImportDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJInterfaceble;
import br.ufsm.aopjungle.metamodel.commons.AOJMethodDeclaration;
import br.ufsm.aopjungle.metamodel.commons.AOJParameter;
import br.ufsm.aopjungle.metamodel.commons.AOJProject;
import br.ufsm.aopjungle.metamodel.commons.AOJReferencedType;
import br.ufsm.aopjungle.metamodel.commons.AOJStatement;
import br.ufsm.aopjungle.metamodel.commons.AOJVariableDeclarationStatement;
import br.ufsm.aopjungle.metamodel.commons.ASTElement;
import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJConstructorDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJEnumDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJIfStatement;
import br.ufsm.aopjungle.patternrefactoring.util.Normalizador;
import br.ufsm.aopjungle.patternrefactoring.util.PRProp;
import br.ufsm.aopjungle.patternrefactoring.util.ProjectUtils;
import br.ufsm.aopjungle.patternrefactoring.util.StatementsCounter;
import br.ufsm.aopjungle.patternrefactoring.util.TypeUtils;
import br.ufsm.aopjungle.views.AOPJungleMonitor;

public class ReplaceConditionalDispatcherWithCommand extends CodeSmellImpl {

	private AOJProject currentProject;
	private AOJClassDeclaration currentClazz;
	private AOJBehaviourKind currentMethod;
	private List<RefOppToReplaceConditionalDispatcherWithCommand> refactoringOpportunities;
	private List<RefOppClassCandidate> clazzCandidates;
	private boolean print;

	@Override
	public void run() {
		AOPJungleMonitor.smellEngineMonitor = AOPJungleMonitor.smellEnginesMonitor.split(1).setWorkRemaining(AOJWorkspace.getInstance().getProjects().size());
		for (AOJProject project : AOJWorkspace.getInstance().getProjects()) {
			AOPJungleMonitor.smellEngineMonitor.subTask("Finding opportunities in project " + project.getName());
			currentProject = project;

			clazzCandidates = new ArrayList<>();

			this.loadElementsFromAST(project);

			this.findRefactoringOpportunities();

			// ordena as oportunidades de refatora��o
			Collections.sort(refactoringOpportunities);
			// deixa em ordem descendente
			Collections.reverse(refactoringOpportunities);

			int i = 1;
			for (RefOppToReplaceConditionalDispatcherWithCommand ro : refactoringOpportunities) {
				ro.setPosition(i++);

				String message = "Refactor to Command: " + ro.getIfFirstLine() + "...";
				ro.setMessage(message);

				registerReport(ro, ro.getClazz());
			}
		}
	}

	private void findRefactoringOpportunities() {
		refactoringOpportunities = new ArrayList<RefOppToReplaceConditionalDispatcherWithCommand>();
		for (RefOppClassCandidate classCandidate : clazzCandidates) {
			currentClazz = classCandidate.getClazz();
			for (RefOppMethodCandidate methodCandidate : classCandidate.getMethods()) {
				currentMethod = methodCandidate.getMethod();
				for (AOJIfStatement ifStatement : methodCandidate.getIfStatements()) {
					// '1 +' significa o primeiro if
					double quantOfConditionals = 1 + this.countElseAndElseIfs(ifStatement.getIfStatement().getElseStatement());

					if (quantOfConditionals >= 3) {
						double quantOfLOC = this.countStatements(ifStatement);
						double averageLOCPerConditional = quantOfLOC / quantOfConditionals;

						if (averageLOCPerConditional >= 3) {
							// double quantSameVariable =
							// this.countSameVariable(ifStatement.getIfStatement());
							double quantOfSimpleExpressions = this.countSimpleExpressions(ifStatement.getIfStatement());
							double averageSimpleConditions = quantOfSimpleExpressions / quantOfConditionals;

							double resultQuantOfConditionals = Normalizador.normalizarRetilineo(quantOfConditionals, PRProp.getRCDWCQuantityOfConditionals());
							double resultQuantOfLOC = Normalizador.normalizarRetilineo(quantOfLOC, PRProp.getRCDWCQuantityOfLOC());
							double resultAverageLOCPerConditional = Normalizador.normalizarRetilineo(averageLOCPerConditional, PRProp.getRCDWCAverageLOCPerConditional());
							double resultQuantOfSimpleExpressions = Normalizador.normalizarRetilineo(quantOfSimpleExpressions, PRProp.getRCDWCQuantityOfSimpleExpressions());
							double resultAverageSimpleConditions = Normalizador.normalizarRetilineo(averageSimpleConditions, PRProp.getRCDWCSimpleConditions());

							double weight1 = PRProp.getRCDWCWeight1(), weight2 = PRProp.getRCDWCWeight2(), weight3 = PRProp.getRCDWCWeight3(), weight4 = PRProp.getRCDWCWeight4(),
									weight5 = PRProp.getRCDWCWeight5();

							double resultado = resultQuantOfConditionals * weight1 + resultQuantOfLOC * weight2 + resultAverageLOCPerConditional * weight3 + resultQuantOfSimpleExpressions * weight4
									+ resultAverageSimpleConditions * weight5;

							// ADempiere
							/*
							 * double resultado = resultQuantOfConditionals * 0.1 + resultQuantOfLOC * 0.25
							 * + resultAverageLOCPerConditional * 0.9 + resultQuantOfSimpleExpressions * 0.5
							 * + resultAverageSimpleConditions * 1;
							 */

							// Apache-Ant
							/*
							 * double resultado = resultQuantOfConditionals * 1.8 + resultQuantOfLOC * 0.9 +
							 * resultAverageLOCPerConditional * 0.4 + resultQuantOfSimpleExpressions * 0.3 +
							 * resultAverageSimpleConditions * 0.5;
							 */

							refactoringOpportunities.add(new RefOppToReplaceConditionalDispatcherWithCommand(resultado, classCandidate.getClazz(), ifStatement, ifStatement.getCode(),
									quantOfConditionals, resultQuantOfConditionals, quantOfLOC, resultQuantOfLOC, quantOfSimpleExpressions, resultQuantOfSimpleExpressions, averageLOCPerConditional,
									resultAverageLOCPerConditional, averageSimpleConditions, resultAverageSimpleConditions));
						}
					}
				}
			}
		}
	}

	/*
	 * private double countSameVariable(IfStatement ifStatement) { if (ifStatement
	 * instanceof IfStatement) { Expression expression = ((IfStatement)
	 * ifStatement).getExpression();
	 * 
	 * if (isSimpleExpression(expression)) { return 1 +
	 * countSimpleExpressions(((IfStatement) ifStatement).getElseStatement()); }
	 * else { return 0 + countSimpleExpressions(((IfStatement)
	 * ifStatement).getElseStatement()); }
	 * 
	 * } return -1; }
	 */

	private double countSimpleExpressions(Statement ifStatement) {
		if (ifStatement instanceof IfStatement) {
			Expression expression = ((IfStatement) ifStatement).getExpression();

			if (isSimpleExpression(expression)) {
				return 1 + countSimpleExpressions(((IfStatement) ifStatement).getElseStatement());
			} else {
				return 0 + countSimpleExpressions(((IfStatement) ifStatement).getElseStatement());
			}

		}
		return 0;
	}

	private boolean isSimpleExpression(Expression expression) {
		try {
			if (expression instanceof SimpleName) {
				Object type = identifyType(((SimpleName) expression).getIdentifier(), expression.getParent(), expression);
				return isTypePrimitive(type);

			} else if (expression instanceof InfixExpression) {
				Expression leftExpression = ((InfixExpression) expression).getLeftOperand();
				Expression rightExpression = ((InfixExpression) expression).getRightOperand();

				boolean left = isSimpleInfixExpression(leftExpression);
				boolean right = isSimpleInfixExpression(rightExpression);

				return left || right;

			} else if (expression instanceof MethodInvocation) {
				MethodInvocation mi = (MethodInvocation) expression;
				if (mi.arguments().size() == 1) {
					return isSimpleInfixExpression((Expression) mi.arguments().get(0));
				} else if (mi.arguments().size() < 1) {
					// muito simples pra ser uma express�o �til na refatora��o
					return false;
				}
			} else if (expression instanceof SuperMethodInvocation) {
				// n�o achei onde pode ser usado
				return false;

			} else if (expression instanceof InstanceofExpression) {
				return true;
			} else if (expression instanceof PrefixExpression) {
				PrefixExpression pe = (PrefixExpression) expression;
				if (pe.getOperator().toString().compareTo("!") == 0) {
					return isSimpleExpression(pe.getOperand());
				}
			} else if (expression instanceof ParenthesizedExpression) {
				return isSimpleExpression(((ParenthesizedExpression) expression).getExpression());

			} else if (expression instanceof FieldAccess) {
				// muito simples, vai ser um boolean, n�o faz sentido para a refatora��o
				return false;

			} else {
				// System.out.println("\n" + expression.getClass());
				// System.out.println(expression);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean isSimpleInfixExpression(Expression expression) {
		if (expression instanceof SimpleName) {
			try {
				Object type = identifyType(((SimpleName) expression).getIdentifier(), expression.getParent(), expression);
				return isTypePrimitive(type);
			} catch (Exception e) {
				// e.printStackTrace();
			}
		} else if (expression instanceof NumberLiteral || expression instanceof NullLiteral || expression instanceof CharacterLiteral || expression instanceof BooleanLiteral
				|| expression instanceof StringLiteral || expression instanceof TypeLiteral) {
			return true;
		} else if (expression instanceof ParenthesizedExpression) {
			return isSimpleInfixExpression(((ParenthesizedExpression) expression).getExpression());
		} else if (expression instanceof QualifiedName) {
			QualifiedName qn = (QualifiedName) expression;
			if (qn.getQualifier() instanceof SimpleName) {
				Object type = null;
				try {
					// se for um atributo de um objeto, identifica o tipo do objeto
					type = identifyType(((SimpleName) qn.getQualifier()).getIdentifier(), qn.getParent(), qn);
					return isTypePrimitive(type);
				} catch (Exception e) {
					// e.printStackTrace();
					// n�o achou o tipo da vari�vel, ent�o deve ser uma classe
					type = getAOJTypeDeclarationFromImport(((SimpleName) qn.getQualifier()).getIdentifier(), currentClazz);

					if (type != null) {
						// verifica se a vari�vel � um atributo de classe
						Type t = getAttributeTypeFromClass(qn.getName().getIdentifier(), (AOJContainer) type);
						if (t != null) {
							return isTypePrimitive(t);
						}

						// verifica se a vari�vel � um atributo de interface
						Type t2 = getAttributeTypeFromInterface(qn.getName().getIdentifier(), (AOJInterfaceble) type);
						if (t2 != null) {
							return isTypePrimitive(t2);
						}

						// verifica se � um atributo constante importado
						Object t3 = getAttributeTypeFromImport(qn.getName().getIdentifier(), (ASTElement) type);
						if (t3 != null) {
							return isTypePrimitive(t3);
						}
					}
				}
			}
		} else if (expression instanceof FieldAccess) {
			FieldAccess fa = (FieldAccess) expression;
			if (fa.getExpression() instanceof ThisExpression) {
				Object type;
				try {
					type = identifyType(fa.getName().getIdentifier(), expression.getParent(), expression);
					return isTypePrimitive(type);
				} catch (Exception e) {
					// e.printStackTrace();
				}
			}
		} else if (expression instanceof ArrayAccess) {
			ArrayAccess aa = (ArrayAccess) expression;
			Object type;
			try {
				type = identifyType(((SimpleName) aa.getArray()).getIdentifier(), expression.getParent(), expression);
				return isTypePrimitive(type);
			} catch (Exception e) {
				// e.printStackTrace();
			}

		} else if (expression instanceof PrefixExpression) {
			PrefixExpression pe = (PrefixExpression) expression;
			if (pe.getOperator().toString().compareTo("!") == 0) {
				return isSimpleInfixExpression(pe.getOperand());
			}

		} else if (expression instanceof MethodInvocation) {
			// deixa de ser simples
			return false;
		} else if (expression instanceof InstanceofExpression) {
			// deixa de ser simples
			return false;
		} else if (expression instanceof InfixExpression) {
			// n�o precisa fazer
			return false;
		} else if (expression instanceof ThisExpression) {
			return true;
		} else {
			// System.out.println("\n" + expression.getClass());
			// System.out.println(expression);
		}
		return false;
	}

	private boolean isTypePrimitive(Object type) {
		if (type != null) {
			print(type.toString());
			if (type instanceof Type) {
				Type t = (Type) type;
				if (t.isPrimitiveType() || (t.toString().compareTo("String") == 0) || (t.toString().compareTo("Class") == 0)) {
					return true;
				}

				// verifica se � um ENUM
			} else if (type instanceof AOJEnumDeclaration) {
				return true;
			} else if (type instanceof TypeUtils) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Identifica o tipo de uma vari�vel.
	 * 
	 * @param variable
	 *            a vari�vel a ser procurada
	 * @param node
	 *            o n� que come�ar� a ser buscada a vari�vel
	 * @param expression
	 *            exibe essa express�o caso n�o ache o tipo
	 * @return o tipo da vari�vel Type ou TypeUtils
	 * @throws Exception
	 *             quando o tipo est� fora do projeto, em classes referenciadas.
	 */
	private Object identifyType(String variable, ASTNode node, Expression expression) throws Exception {
		// para debug
		// print =
		// currentClazz.getFullQualifiedName().compareTo("org.eclipse.persistence.internal.helper.NonSynchronizedVector")
		// == 0;

		// verifica se a vari�vel foi criada em algum for statement
		if (node instanceof MethodDeclaration) {
			// parar de pesquisar o tipo da vari�vel
			return null;

			// se foi criado em um EnhancedForStatement
		} else if (node instanceof EnhancedForStatement) {
			SingleVariableDeclaration svd = ((EnhancedForStatement) node).getParameter();
			if (svd.getName().toString().compareTo(variable) == 0) {
				return svd.getType();
			}

			// se foi criado em um ForStatement
		} else if (node instanceof ForStatement) {
			for (Object o : ((ForStatement) node).initializers()) {
				if (o instanceof VariableDeclarationExpression) {
					for (Object vdf : ((VariableDeclarationExpression) o).fragments()) {
						if (((VariableDeclarationFragment) vdf).getName().toString().compareTo(variable) == 0) {
							return ((VariableDeclarationExpression) o).getType();
						}
					}
				}
			}

			// sen�o procura no node acima pra ver se a variavel n�o foi criado antes
		} else {
			Object t = identifyType(variable, node.getParent(), expression);
			if (t != null) {
				return t;
			}
		}

		// verifica se a vari�vel foi criada dentro do m�todo
		for (AOJStatement aojStatement : currentMethod.getStatements()) {
			if (aojStatement instanceof AOJVariableDeclarationStatement) {
				VariableDeclarationFragment vdf = (VariableDeclarationFragment) aojStatement.getNode();
				if (vdf.getName().getIdentifier().compareTo(variable) == 0) {
					VariableDeclarationStatement vds = (VariableDeclarationStatement) vdf.getParent();
					return vds.getType();
				}
			}
		}

		// verifica se a vari�vel foi passada por par�metro
		for (AOJParameter aojParameter : currentMethod.getParameters()) {
			if (aojParameter.getName().compareTo(variable) == 0) {
				return (Type) aojParameter.getType().getNode();
			}
		}

		// verifica se a vari�vel � um atributo de classe
		// print("Obtendo o tipo da variavel nos atributos da classe " +
		// currentClazz.getFullQualifiedName());
		Type t = getAttributeTypeFromClass(variable, currentClazz);
		if (t != null) {
			return t;
		}

		// verifica se a vari�vel � um atributo de interface
		Type t2 = getAttributeTypeFromInterface(variable, currentClazz);
		if (t2 != null) {
			return t2;
		}

		// verifica se � um atributo constante importado
		Object t3 = getAttributeTypeFromImport(variable, currentClazz);
		if (t3 != null) {
			return t3;
		}

		// n�o achei!
		// if (print)
		throw new Exception("Tipo n�o encontrado! Expression: " + expression + ", of class " + currentClazz.getFullQualifiedName());
		// return null;
	}

	private Object getAttributeTypeFromImport(String variable, ASTElement clazz) {
		try {
			for (IImportDeclaration importDeclaration : clazz.getCompilationUnit().getCompilationUnit().getImports()) {
				// pega somente os import static
				if (importDeclaration.getFlags() == 8) {
					String importName = importDeclaration.getElementName();
					String attributeName = importName.substring(importName.lastIndexOf(".") + 1, importName.length());
					if ((attributeName.compareTo(variable) == 0) || (attributeName.compareTo("*") == 0)) {
						String className = importName.substring(0, importName.lastIndexOf("."));
						AOJClassDeclaration aojClassDeclaration = ProjectUtils.getAOJClassByFullQualifiedName(className, currentProject.getClasses());
						if (aojClassDeclaration != null) {
							return getAttributeTypeFromClass(variable, aojClassDeclaration);
						} else if (TypeUtils.isPrimitiveFromJavaLang(className)) {
							return new TypeUtils();
						}
					}
				}
			}
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Type getAttributeTypeFromInterface(String variable, AOJInterfaceble clazz) {
		for (AOJReferencedType aojReferencedType : clazz.getImplementedInterfaces()) {
			// print("Buscando a variavel " + variable + " na interface " +
			// aojReferencedType.getName());
			AOJContainer implementedInterface = getAOJTypeDeclarationFromImport(aojReferencedType.getName(), (ASTElement) clazz);
			if (implementedInterface != null) {
				// print("Achou a interface " + implementedInterface);
				for (AOJContainer type : currentProject.getTypes()) {
					if (implementedInterface.getFullQualifiedName().compareTo(type.getFullQualifiedName()) == 0) {
						// print("Achou o type da interface " + implementedInterface);
						Type t = getAttributeTypeFromClass(variable, type);
						if (t != null) {
							return t;
						}
						break;
					}
				}
			}
		}
		return null;
	}

	private AOJContainer getAOJTypeDeclarationFromImport(String className, ASTElement clazz) {
		List<AOJImportDeclaration> imports = clazz.getCompilationUnit().getImportsDeclaration();

		// pega os imports do arquivo java
		for (AOJImportDeclaration aojImportDeclaration : imports) {
			// se o type da cria��o estiver no import
			if (aojImportDeclaration.getClassName().equals(className)) {
				// pega as classes do projeto
				for (AOJContainer type : currentProject.getTypes()) {
					// pega a classe correspondente ao type
					if (aojImportDeclaration.qualifiedName().equals(type.getFullQualifiedName())) {
						return type;
					}
				}
			} else {
				// caso esteja em um "*"
				String fullQualifiedName = this.findByWildcard(aojImportDeclaration, className);
				if (fullQualifiedName.isEmpty() == false) {
					for (AOJContainer type : currentProject.getTypes()) {
						// pega a classe correspondente ao type
						if (fullQualifiedName.equals(type.getFullQualifiedName())) {
							return type;
						}
					}
				}
			}
		}

		// caso o Type n�o est� no import, ou seja, est� no mesmo n�vel do package
		// pega as classes do package
		for (AOJContainer aojContainer : clazz.getCompilationUnit().getPackage().getTypes()) {
			// se for uma classe
			// if (aojContainer instanceof AOJClassDeclaration) {
			// AOJClassDeclaration aojTypeDeclaration = (AOJClassDeclaration) aojContainer;
			if (aojContainer.getName().equals(className)) {
				return aojContainer;
			}
			// }
		}

		// se n�o faz parte do projeto (Object por exemplo)
		return null;
	}

	private String findByWildcard(AOJImportDeclaration imp, String name) {
		if (imp.getCode().indexOf("*") != -1) { // there's not * on import declaration
			String fullQualifiedName = imp.qualifiedName().replace("*", name); // replace * by type name
			// if (forClass (fullQualifiedName)) { // try to find by classloader
			try {
				if (fileExists(fullQualifiedName)) {
					return fullQualifiedName;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "";
	}

	private boolean fileExists(String fullQualifiedName) throws Exception {
		if (currentProject.getResourceProject().isNatureEnabled("org.eclipse.m2e.core.maven2Nature")) {
			IFile file = currentProject.getResourceProject().getFile("/src/main/java/" + fullQualifiedName.replace(".", "/") + ".java");
			return file.exists();
		} else if (currentProject.getResourceProject().isNatureEnabled("org.eclipse.jdt.core.javanature")) {
			return forClass(fullQualifiedName);
		}

		throw new Exception("Natureza n�o dispon�vel");
	}

	private boolean forClass(String fullQualifiedName) {
		boolean result = true;
		try {
			Class.forName(fullQualifiedName);
		} catch (ClassNotFoundException e) {
			result = false;
		}
		return result;
	}

	private Type getAttributeTypeFromClass(String variable, AOJContainer clazz) {
		if (clazz != null) {
			// print("Buscando a variavel " + variable + " nos atributos da classe " +
			// clazz.getFullQualifiedName());
			if (clazz.getMembers() != null) {
				for (AOJAttributeDeclaration aojAttributeDeclaration : clazz.getMembers().getAttributes()) {
					if (aojAttributeDeclaration.getName().compareTo(variable) == 0) {
						// print("Achou a vari�vel " + variable + " na classe " +
						// clazz.getFullQualifiedName());
						return aojAttributeDeclaration.getAttributeDeclaration().getType();
					}
				}
			}
			// print("Buscando a variavel " + variable + " nas interfaces da classe " +
			// clazz.getFullQualifiedName());
			if (clazz instanceof AOJInterfaceble) {
				Type t = getAttributeTypeFromInterface(variable, (AOJInterfaceble) clazz);
				if (t != null) {
					// print("Achou a vari�vel " + variable + " na interface " +
					// clazz.getFullQualifiedName());
					return t;
				}
			}
			/*
			 * if (clazz.getSuperType() != null) print("Chamando superclasse " +
			 * clazz.getSuperType().getFullQualifiedName()); else print("Supertype nulo");
			 */

			return getAttributeTypeFromClass(variable, clazz.getSuperType());
		}
		// print("Retornando nulo");
		return null;
	}

	private double countStatements(AOJIfStatement aojIfStatement) {
		try {
			return StatementsCounter.countStatements(aojIfStatement.getIfStatement());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	private double countElseAndElseIfs(Statement ifStatement) {
		if (ifStatement instanceof IfStatement) {
			return 1 + countElseAndElseIfs(((IfStatement) ifStatement).getElseStatement());
		} else if (ifStatement != null) {
			return 1;
		}
		return 0;
	}

	private void loadElementsFromAST(AOJProject project) {
		for (AOJClassDeclaration clazz : project.getClasses()) {
			List<RefOppMethodCandidate> methods = new ArrayList<>();
			currentClazz = clazz;

			// para cada m�todo
			for (AOJMethodDeclaration method : ((AOJClassDeclaration) clazz).getMembers().getMethods()) {
				List<AOJIfStatement> ifStatements = new ArrayList<>();
				ifStatements.addAll(searchInStatements(method));
				if (ifStatements.size() > 0)
					methods.add(new RefOppMethodCandidate(method, ifStatements));
			}

			// para cada construtor
			for (AOJConstructorDeclaration constructor : ((AOJClassDeclaration) clazz).getMembers().getConstructors()) {
				List<AOJIfStatement> ifStatements = new ArrayList<>();
				ifStatements.addAll(searchInStatements(constructor));
				if (ifStatements.size() > 0)
					methods.add(new RefOppMethodCandidate(constructor, ifStatements));
			}

			// this.ifStatements.put(clazz, ifStatements);
			this.clazzCandidates.add(new RefOppClassCandidate(clazz, methods));
		}
	}

	private List<AOJIfStatement> searchInStatements(AOJBehaviourKind method) {
		List<AOJIfStatement> ifStatements = new ArrayList<>();
		// para cada statement
		for (AOJStatement statement : method.getStatements()) {
			// para cada ExpressionStatement
			if (statement instanceof AOJIfStatement) {
				addIfNotContains((AOJIfStatement) statement, ifStatements);
			}
		}
		return ifStatements;
	}

	private void addIfNotContains(AOJIfStatement ifStatementCandidate, List<AOJIfStatement> aojIfStatements) {
		boolean exists = false;
		for (AOJIfStatement aojIfStatement : aojIfStatements) {
			Statement statement = aojIfStatement.getIfStatement().getElseStatement();
			if (statement != null && statement instanceof IfStatement) {
				exists = statement.toString().contains(ifStatementCandidate.getIfStatement().toString());
			} else
				exists = true;
		}
		if (exists == false) {
			aojIfStatements.add(ifStatementCandidate);
		}
	}

	@Override
	public String getLabel() {
		return "Replace Conditional Dispatcher With Command";
	}

	private void print(String message) {
		if (print)
			System.out.println(message);
	}

}
