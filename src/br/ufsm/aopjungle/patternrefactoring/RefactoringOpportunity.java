package br.ufsm.aopjungle.patternrefactoring;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;

public class RefactoringOpportunity implements Comparable<RefactoringOpportunity> {
	private int position;
	private double value;
	private String message;

	@XStreamOmitField
	private AOJClassDeclaration clazz;
	private String className;

	public RefactoringOpportunity(double value, AOJClassDeclaration clazz) {
		this.value = value;
		this.clazz = clazz;
		this.className = clazz.getName();
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public AOJClassDeclaration getClazz() {
		return clazz;
	}

	public void setClazz(AOJClassDeclaration rootClazz) {
		this.clazz = rootClazz;
	}

	@Override
	public int compareTo(RefactoringOpportunity o) {
		if (this.value == o.getValue()) {
			return 0;
		} else if (this.value > o.getValue()) {
			return 1;
		} else {
			return -1;
		}
	}

	@Override
	public String toString() {
		NumberFormat formatter = new DecimalFormat("#.###");
		String s = formatter.format(value);
		return "#" + position + " - " + s + " : " + message;
	}
}
