package br.ufsm.aopjungle.patternrefactoring;

import java.util.List;

import br.ufsm.aopjungle.metamodel.commons.AOJBehaviourKind;
import br.ufsm.aopjungle.metamodel.java.AOJIfStatement;

public class RefOppMethodCandidate {

	private AOJBehaviourKind method;
	private List<AOJIfStatement> ifStatements;

	public RefOppMethodCandidate(AOJBehaviourKind method, List<AOJIfStatement> ifStatements) {
		this.method = method;
		this.ifStatements = ifStatements;
	}
	
	public RefOppMethodCandidate(AOJBehaviourKind method) {
		this.method = method;
	}

	public AOJBehaviourKind getMethod() {
		return method;
	}

	public void setMethod(AOJBehaviourKind	 method) {
		this.method = method;
	}

	public List<AOJIfStatement> getIfStatements() {
		return ifStatements;
	}

	public void setIfStatements(List<AOJIfStatement> ifStatements) {
		this.ifStatements = ifStatements;
	}

}
