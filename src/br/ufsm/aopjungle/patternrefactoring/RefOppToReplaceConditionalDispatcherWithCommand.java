package br.ufsm.aopjungle.patternrefactoring;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

import br.ufsm.aopjungle.metamodel.java.AOJClassDeclaration;
import br.ufsm.aopjungle.metamodel.java.AOJIfStatement;

public class RefOppToReplaceConditionalDispatcherWithCommand extends RefactoringOpportunity {

	@XStreamOmitField
	private AOJIfStatement ifStatement;
	private String ifFirstLine;
	private double quantOfConditionals;
	private double resultQuantOfConditionals;
	private double quantOfLOC;
	private double resultQuantOfLOC;
	private double quantOfSimpleExpressions;
	private double resultQuantOfSimpleExpressions;
	private double averageLOCPerConditional;
	private double resultAverageLOCPerConditional;
	private double simpleConditions;
	private double resultSimpleConditions;

	public RefOppToReplaceConditionalDispatcherWithCommand(double value, AOJClassDeclaration clazz, AOJIfStatement ifStatement, String ifFirstLine, double quantOfConditionals,
			double resultQuantOfConditionals, double quantOfLOC, double resultQuantOfLOC, double quantOfSimpleExpressions, double resultQuantOfSimpleExpressions, double averageLOCPerConditional,
			double resultAverageLOCPerConditional, double simpleConditions, double resultSimpleConditions) {
		super(value, clazz);
		this.ifStatement = ifStatement;
		this.ifFirstLine = ifFirstLine;
		this.quantOfConditionals = quantOfConditionals;
		this.resultQuantOfConditionals = resultQuantOfConditionals;
		this.quantOfLOC = quantOfLOC;
		this.resultQuantOfLOC = resultQuantOfLOC;
		this.quantOfSimpleExpressions = quantOfSimpleExpressions;
		this.resultQuantOfSimpleExpressions = resultQuantOfSimpleExpressions;
		this.averageLOCPerConditional = averageLOCPerConditional;
		this.resultAverageLOCPerConditional = resultAverageLOCPerConditional;
		this.simpleConditions = simpleConditions;
		this.resultSimpleConditions = resultSimpleConditions;
	}

	public AOJIfStatement getIfStatement() {
		return ifStatement;
	}

	public void setIfStatement(AOJIfStatement ifStatement) {
		this.ifStatement = ifStatement;
	}

	public String getIfFirstLine() {
		return ifFirstLine;
	}

	public void setIfFirstLine(String ifFirstLine) {
		this.ifFirstLine = ifFirstLine;
	}

	public double getQuantOfConditionals() {
		return quantOfConditionals;
	}

	public void setQuantOfConditionals(double quantOfConditionals) {
		this.quantOfConditionals = quantOfConditionals;
	}

	public double getQuantOfLOC() {
		return quantOfLOC;
	}

	public void setQuantOfLOC(double quantOfLOC) {
		this.quantOfLOC = quantOfLOC;
	}

	public double getResultQuantOfConditionals() {
		return resultQuantOfConditionals;
	}

	public void setResultQuantOfConditionals(double resultQuantOfConditionals) {
		this.resultQuantOfConditionals = resultQuantOfConditionals;
	}

	public double getResultQuantOfLOC() {
		return resultQuantOfLOC;
	}

	public void setResultQuantOfLOC(double resultQuantOfLOC) {
		this.resultQuantOfLOC = resultQuantOfLOC;
	}

	public double getQuantOfSimpleExpressions() {
		return quantOfSimpleExpressions;
	}

	public void setQuantOfSimpleExpressions(double quantOfSimpleExpressions) {
		this.quantOfSimpleExpressions = quantOfSimpleExpressions;
	}

	public double getResultQuantOfSimpleExpressions() {
		return resultQuantOfSimpleExpressions;
	}

	public void setResultQuantOfSimpleExpressions(double resultQuantOfSimpleExpressions) {
		this.resultQuantOfSimpleExpressions = resultQuantOfSimpleExpressions;
	}

	public double getAverageLOCPerConditional() {
		return averageLOCPerConditional;
	}

	public void setAverageLOCPerConditional(double averageLOCPerConditional) {
		this.averageLOCPerConditional = averageLOCPerConditional;
	}

	public double getResultAverageLOCPerConditional() {
		return resultAverageLOCPerConditional;
	}

	public void setResultAverageLOCPerConditional(double resultAverageLOCPerConditional) {
		this.resultAverageLOCPerConditional = resultAverageLOCPerConditional;
	}

	public double getSimpleConditions() {
		return simpleConditions;
	}

	public void setSimpleConditions(double simpleConditions) {
		this.simpleConditions = simpleConditions;
	}

	public double getResultSimpleConditions() {
		return resultSimpleConditions;
	}

	public void setResultSimpleConditions(double resultSimpleConditions) {
		this.resultSimpleConditions = resultSimpleConditions;
	}

}
