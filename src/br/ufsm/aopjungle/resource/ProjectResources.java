package br.ufsm.aopjungle.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;

import com.thoughtworks.xstream.annotations.XStreamOmitField;

/**
 * Wrapper for main resource of the Eclipse workbench
 * Singleton
 * @author Cristiano De Faveri
 */

public class ProjectResources {
	@XStreamOmitField
	private static ProjectResources instance;
	/**
	 * @return List<IProject>
	 */	
	public List<IProject> getProjects() {
		List<IProject> result = new ArrayList<IProject>();
		result.addAll(Arrays.asList(getWorkspaceRoot().getProjects()));	
		return result;
	}
	
	public IWorkspaceRoot getWorkspaceRoot() {
		return ResourcesPlugin.getWorkspace().getRoot(); 
	}
	
	public static ProjectResources getInstance() {
		if (instance == null) 
			instance = new ProjectResources();
		return instance;
	}
	
	private ProjectResources() {
		// Singleton 
	}
}	
